.class public Lorg/scribe/utils/StreamUtils;
.super Ljava/lang/Object;
.source "StreamUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getStreamContents(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 9
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 24
    :try_start_0
    const-string v6, "Cannot get String from a null object"

    invoke-static {p0, v6}, Lorg/scribe/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    const/high16 v6, 0x10000

    :try_start_1
    new-array v0, v6, [C

    .line 35
    .local v0, "buffer":[C
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .local v4, "out":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/io/InputStreamReader;

    const-string v6, "UTF-8"

    invoke-direct {v2, p0, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 40
    .local v2, "in":Ljava/io/Reader;
    :cond_0
    const/4 v6, 0x0

    array-length v7, v0

    invoke-virtual {v2, v0, v6, v7}, Ljava/io/Reader;->read([CII)I

    move-result v5

    .line 41
    .local v5, "read":I
    if-lez v5, :cond_1

    .line 43
    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 38
    :cond_1
    if-gez v5, :cond_0

    .line 46
    invoke-virtual {v2}, Ljava/io/Reader;->close()V

    .line 47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .end local v0    # "buffer":[C
    .end local v2    # "in":Ljava/io/Reader;
    .end local v4    # "out":Ljava/lang/StringBuilder;
    .end local v5    # "read":I
    :goto_0
    return-object v6

    .line 26
    :catch_0
    move-exception v1

    .line 28
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v6, "Prime31-SU"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "InputStream is null when attempting to get it\'s contents: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    const-string v6, ""

    goto :goto_0

    .line 48
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v3

    .line 50
    .local v3, "ioe":Ljava/io/IOException;
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Error while reading response body"

    invoke-direct {v6, v7, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method
