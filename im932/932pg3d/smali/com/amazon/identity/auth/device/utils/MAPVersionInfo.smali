.class public interface abstract Lcom/amazon/identity/auth/device/utils/MAPVersionInfo;
.super Ljava/lang/Object;
.source "MAPVersionInfo.java"


# static fields
.field public static final LWA_VERSION:Ljava/lang/String; = "1.0.0"

.field public static final VERSION:Ljava/lang/String; = "3.3.0"

.field public static final VERSION_NAME:Ljava/lang/String; = "AmazonAuthenticationSDK"
