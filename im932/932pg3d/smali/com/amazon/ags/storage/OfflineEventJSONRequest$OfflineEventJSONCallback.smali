.class public interface abstract Lcom/amazon/ags/storage/OfflineEventJSONRequest$OfflineEventJSONCallback;
.super Ljava/lang/Object;
.source "OfflineEventJSONRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/ags/storage/OfflineEventJSONRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OfflineEventJSONCallback"
.end annotation


# virtual methods
.method public abstract onRecoverableError()V
.end method

.method public abstract onSuccess()V
.end method

.method public abstract onUnrecoverableError()V
.end method
