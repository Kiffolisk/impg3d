.class public Lcom/amazon/ags/constants/ClientConfigKeys;
.super Ljava/lang/Object;
.source "ClientConfigKeys.java"


# static fields
.field public static final AUTH_GET_TOKEN_TIMEOUT_MILLIS:Ljava/lang/String; = "AUTH_GET_TOKEN_TIMEOUT_MILLIS"

.field public static final HTTP_CONNECTION_POOL_TIMEOUT_MILLIS:Ljava/lang/String; = "HTTP_CONNECTION_POOL_TIMEOUT_MILLIS"

.field public static final HTTP_CONNECTION_TIMEOUT_MILLIS:Ljava/lang/String; = "HTTP_CONNECTION_TIMEOUT_MILLIS"

.field public static final HTTP_MAX_CONNECTIONS_PER_ROUTE:Ljava/lang/String; = "HTTP_MAX_CONNECTIONS_PER_ROUTE"

.field public static final HTTP_MAX_TOTAL_CONNECTIONS:Ljava/lang/String; = "HTTP_MAX_TOTAL_CONNECTIONS"

.field public static final HTTP_SOCKET_TIMEOUT_MILLIS:Ljava/lang/String; = "HTTP_SOCKET_TIMEOUT_MILLIS"

.field public static final SETTINGS_CACHE_KEY:Ljava/lang/String; = "clientConfig"

.field public static final THREAD_POOL_SIZE:Ljava/lang/String; = "THREAD_POOL_SIZE"

.field public static final THREAD_TIMEOUT:Ljava/lang/String; = "THREAD_TIMEOUT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
