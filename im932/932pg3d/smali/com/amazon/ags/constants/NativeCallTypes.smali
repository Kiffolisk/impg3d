.class public Lcom/amazon/ags/constants/NativeCallTypes;
.super Ljava/lang/Object;
.source "NativeCallTypes.java"


# static fields
.field public static final BACKGROUND_TASK:Ljava/lang/String; = "backgroundTask"

.field public static final BATCH_GET_GLOBAL_STATE:Ljava/lang/String; = "batchGetGlobalState"

.field public static final BATCH_PUT_GLOBAL_STATE:Ljava/lang/String; = "batchPutGlobalState"

.field public static final CAN_LAUNCH_KINDLE_SETTINGS:Ljava/lang/String; = "canDisplayKindleSettings"

.field public static final CLEAR_CACHE:Ljava/lang/String; = "clearCache"

.field public static final DOWNLOAD_IMAGES:Ljava/lang/String; = "downloadImages"

.field public static final DO_LOGIN:Ljava/lang/String; = "doLogin"

.field public static final DO_LOGOUT:Ljava/lang/String; = "doLogout"

.field public static final GET_CACHE_ITEM:Ljava/lang/String; = "getCacheItem"

.field public static final GET_LOCALE_INFO:Ljava/lang/String; = "getLocaleInfo"

.field public static final GET_LOGGED_IN_STATUS:Ljava/lang/String; = "getLoggedInStatus"

.field public static final GET_NETWORK_INFO:Ljava/lang/String; = "getNetworkInfo"

.field public static final GET_PACKAGE_NAME:Ljava/lang/String; = "getPackageName"

.field public static final GET_SETTING:Ljava/lang/String; = "getSetting"

.field public static final GET_VARIATION_VARIABLE:Ljava/lang/String; = "getVariationVariable"

.field public static final HAS_OPTED_IN:Ljava/lang/String; = "hasOptedIn"

.field public static final IS_GC_SERVICE_READY:Ljava/lang/String; = "isGCServiceReady"

.field public static final IS_KINDLE:Ljava/lang/String; = "isKindle"

.field public static final IS_KINDLE_REGISTERED:Ljava/lang/String; = "isKindleRegistered"

.field public static final IS_WHISERPSYNC_ENABLED:Ljava/lang/String; = "isWhispersyncEnabled"

.field public static final LAUNCH_KINDLE_SETTINGS:Ljava/lang/String; = "launchKindleSettings"

.field public static final MAKE_SERVICE_CALL:Ljava/lang/String; = "makeServiceCall"

.field public static final NOTIFY_NATIVE:Ljava/lang/String; = "notifyNative"

.field public static final NOTIFY_SIGN_IN_STATE_CHANGE:Ljava/lang/String; = "NOTIFY_SIGN_IN_STATE_CHANGE"

.field public static final OPEN_BROWSER:Ljava/lang/String; = "openBrowser"

.field public static final OPEN_EMAIL_EDITOR:Ljava/lang/String; = "openEmailEditor"

.field public static final PUT_SETTING:Ljava/lang/String; = "putSetting"

.field public static final QUERY_CACHE_ITEMS:Ljava/lang/String; = "queryCacheItems"

.field public static final QUEUE_OFFLINE_EVENT:Ljava/lang/String; = "queueOfflineEvent"

.field public static final REMOVE_CACHE_ITEMS:Ljava/lang/String; = "removeCachedItems"

.field public static final REPORT_EVENT:Ljava/lang/String; = "reportEvent"

.field public static final SET_CACHE_ITEM:Ljava/lang/String; = "setCacheItem"

.field public static final SET_CACHE_ITEMS:Ljava/lang/String; = "setCacheItems"

.field public static final SET_OPT_IN:Ljava/lang/String; = "setOptIn"

.field public static final SHOW_TOAST:Ljava/lang/String; = "showToast"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method
