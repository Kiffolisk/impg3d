.class public final Lcom/amazon/ags/constants/OverlayBindingKeys;
.super Ljava/lang/Object;
.source "OverlayBindingKeys.java"


# static fields
.field public static final OVERLAY_ACTION_CODE_KEY:Ljava/lang/String; = "OVERLAY_ACTION_CODE"

.field public static final OVERLAY_DATA_BUNDLE_KEY:Ljava/lang/String; = "OVERLAY_DATA_BUNDLE"

.field public static final OVERLAY_SESSION_APPLICATION_NAME_KEY:Ljava/lang/String; = "OVERLAY_SESSION_APPLICATION_NAME"

.field public static final OVERLAY_SESSION_CLIENT_VERSION_KEY:Ljava/lang/String; = "OVERLAY_SESSION_CLIENT_VERSION"

.field public static final OVERLAY_SESSION_CONTENT_VERSION_KEY:Ljava/lang/String; = "OVERLAY_SESSION_CONTENT_VERSION"

.field public static final OVERLAY_SESSION_COUNTRY_CODE_KEY:Ljava/lang/String; = "OVERLAY_SESSION_COUNTRY_CODE"

.field public static final OVERLAY_SESSION_DEVICE_IDENTIFIER_KEY:Ljava/lang/String; = "OVERLAY_SESSION_DEVICE_IDENTIFIER"

.field public static final OVERLAY_SESSION_DEVICE_MANUFACTURER_KEY:Ljava/lang/String; = "OVERLAY_SESSION_DEVICE_MANUFACTURER"

.field public static final OVERLAY_SESSION_DEVICE_MODEL_KEY:Ljava/lang/String; = "OVERLAY_SESSION_DEVICE_MODEL"

.field public static final OVERLAY_SESSION_DEVICE_TYPE_KEY:Ljava/lang/String; = "OVERLAY_SESSION_DEVICE_TYPE"

.field public static final OVERLAY_SESSION_GAMECIRCLE_VERSION_KEY:Ljava/lang/String; = "OVERLAY_SESSION_GAMECIRCLE_VERSION_KEY"

.field public static final OVERLAY_SESSION_LANGUAGE_CODE_KEY:Ljava/lang/String; = "OVERLAY_SESSION_LANGUAGE_CODE"

.field public static final OVERLAY_SESSION_VARIATION_CACHE:Ljava/lang/String; = "OVERLAY_SESSION_VARIATION_CACHE"


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
