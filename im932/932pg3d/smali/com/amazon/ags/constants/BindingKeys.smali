.class public final Lcom/amazon/ags/constants/BindingKeys;
.super Ljava/lang/Object;
.source "BindingKeys.java"


# static fields
.field public static final AUTHENTICATE_RESULT_BUNDLE_KEY:Ljava/lang/String; = "AUTHENTICATE_RESULT"

.field public static final AUTHORIZE_RESULT_BUNDLE_KEY:Ljava/lang/String; = "AUTH_RESULT"

.field public static final BIND_ASYNCHRONOUS:Ljava/lang/String; = "BindAsynchronous"

.field public static final BIND_SYNCHRONOUS:Ljava/lang/String; = "BindSynchronous"

.field public static final COUNTRY_OF_RESIDENCE:Ljava/lang/String; = "COR"

.field public static final FEATURE_LIST:Ljava/lang/String; = "FEATURE_LIST"

.field public static final GAME_ID_KEY:Ljava/lang/String; = "GAME_ID"

.field public static final OVERLAY_ACTION_CODE:Ljava/lang/String; = "OVERLAY_ACTION_CODE"

.field public static final PACKAGE_NAME_KEY:Ljava/lang/String; = "PACKAGE_NAME"

.field public static final POP_UP_LOCATION:Ljava/lang/String; = "POP_UP_LOCATION"

.field public static final REQUEST_ID_KEY:Ljava/lang/String; = "REQUEST_ID"

.field public static final REQUEST_TIMESTAMP:Ljava/lang/String; = "REQUEST_TIMESTAMP"

.field public static final SESSION_ID:Ljava/lang/String; = "SESSION_ID"

.field public static final WELCOME_BACK_TOAST:Ljava/lang/String; = "WELCOME_BACK_TOAST"

.field public static final WHISPERSYNC_ENABLED:Ljava/lang/String; = "WHISPERSYNC_ENABLED"


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
