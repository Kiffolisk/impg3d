.class public Lcom/amazon/ags/constants/OfflineCacheTypes;
.super Ljava/lang/Object;
.source "OfflineCacheTypes.java"


# static fields
.field public static final ACHIEVEMENTS_CACHE:Ljava/lang/String; = "AchievementsCache"

.field public static final ACHIEVEMENTS_EXIST_CACHE:Ljava/lang/String; = "AchievementsExistCache"

.field public static final LEADERBOARDS_CACHE:Ljava/lang/String; = "LeaderboardsCache"

.field public static final LEADERBOARDS_EXIST_CACHE:Ljava/lang/String; = "LeaderboardsExistCache"

.field public static final PLAYER_PROFILE_CACHE:Ljava/lang/String; = "PlayerProfileCache"

.field public static final SETTINGS_CACHE:Ljava/lang/String; = "SettingsCache"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method
