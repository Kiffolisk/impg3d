.class public final Lcom/amazon/ags/constants/ToastKeys;
.super Ljava/lang/Object;
.source "ToastKeys.java"


# static fields
.field public static final ACTION_MAPPINGS_KEY:Ljava/lang/String; = "ACTION_MAPPINGS"

.field public static final SHOW_TOASTS_FALSE:Ljava/lang/String; = "dontShowToasts"

.field public static final SHOW_TOASTS_TRUE:Ljava/lang/String; = "showToasts"

.field public static final SHOW_TOASTS_UNDEFINED:Ljava/lang/String; = "toastsUndefined"

.field public static final TOAST_BUTTON_TEXT:Ljava/lang/String; = "buttonText"

.field public static final TOAST_CLICK_DATA_KEY:Ljava/lang/String; = "TOAST_CLICK_DATA"

.field public static final TOAST_DEDUPE_KEY:Ljava/lang/String; = "dedupeTypes"

.field public static final TOAST_DESCRIPTION_KEY:Ljava/lang/String; = "description"

.field public static final TOAST_HEIGHT_KEY:Ljava/lang/String; = "TOAST_HEIGHT"

.field public static final TOAST_ICON_KEY:Ljava/lang/String; = "icon"

.field public static final TOAST_IS_GUEST_KEY:Ljava/lang/String; = "isGuest"

.field public static final TOAST_METHOD_KEY:Ljava/lang/String; = "method"

.field public static final TOAST_REQUEST_DISPLAYED:Ljava/lang/String; = "toastRequestDisplayed"

.field public static final TOAST_SHOW_BUTTON_KEY:Ljava/lang/String; = "showButton"

.field public static final TOAST_TARGET_KEY:Ljava/lang/String; = "target"

.field public static final TOAST_TITLE_KEY:Ljava/lang/String; = "title"

.field public static final TOAST_TOUCH_X_KEY:Ljava/lang/String; = "TOAST_TOUCH_X"

.field public static final TOAST_TOUCH_Y_KEY:Ljava/lang/String; = "TOAST_TOUCH_Y"

.field public static final TOAST_TYPE_KEY:Ljava/lang/String; = "type"

.field public static final TOAST_WIDTH_KEY:Ljava/lang/String; = "TOAST_WIDTH"


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
