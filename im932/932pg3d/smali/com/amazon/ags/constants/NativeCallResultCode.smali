.class public Lcom/amazon/ags/constants/NativeCallResultCode;
.super Ljava/lang/Object;
.source "NativeCallResultCode.java"


# static fields
.field public static final AUTHORIZATION_ERROR:Ljava/lang/String; = "AUTHORIZATION_ERROR"

.field public static final ERROR:Ljava/lang/String; = "ERROR"

.field public static final NETWORK_ERROR:Ljava/lang/String; = "NETWORK_ERROR"

.field public static final REQUEST_ERROR:Ljava/lang/String; = "REQUEST_ERROR"

.field public static final SUCCESS:Ljava/lang/String; = "SUCCESS"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method
