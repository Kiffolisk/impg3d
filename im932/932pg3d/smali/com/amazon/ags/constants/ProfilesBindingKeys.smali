.class public final Lcom/amazon/ags/constants/ProfilesBindingKeys;
.super Ljava/lang/Object;
.source "ProfilesBindingKeys.java"


# static fields
.field public static final ALIAS_KEY:Ljava/lang/String; = "ALIAS"

.field public static final AVATAR_URL_KEY:Ljava/lang/String; = "avatarUrl"

.field public static final FRIENDS_KEY:Ljava/lang/String; = "friends"

.field public static final FRIENDS_PLAYER_IDS_KEY:Ljava/lang/String; = "friendsPlayerIds"

.field public static final FRIENDS_PROFILES_KEY:Ljava/lang/String; = "friendsProfiles"

.field public static final IS_SIGNED_IN_KEY:Ljava/lang/String; = "isSignedIn"

.field public static final PLAYER_ID_KEY:Ljava/lang/String; = "playerId"


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
