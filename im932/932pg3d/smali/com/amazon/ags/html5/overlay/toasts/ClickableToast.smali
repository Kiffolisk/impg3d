.class public interface abstract Lcom/amazon/ags/html5/overlay/toasts/ClickableToast;
.super Ljava/lang/Object;
.source "ClickableToast.java"


# virtual methods
.method public abstract addClickableToastObserver(Lcom/amazon/ags/html5/overlay/toasts/ClickableToastObserver;)V
.end method

.method public abstract destroy()V
.end method

.method public abstract isShowing()Z
.end method

.method public abstract setToastData(Ljava/lang/String;)V
.end method

.method public abstract setToastOnTouchListener(Landroid/view/View$OnTouchListener;)V
.end method

.method public abstract show(Landroid/os/Handler;)V
.end method

.method public abstract update(Ljava/lang/String;I)V
.end method
