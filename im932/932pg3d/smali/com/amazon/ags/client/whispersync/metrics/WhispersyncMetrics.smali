.class public Lcom/amazon/ags/client/whispersync/metrics/WhispersyncMetrics;
.super Ljava/lang/Object;
.source "WhispersyncMetrics.java"


# static fields
.field public static final WHIPSERYNC_PARSE_EXCEPTION_METRIC:Ljava/lang/String; = "WHISPERSYNC_PARSE_EXCEPTION"

.field public static final WHISPERSYNC_CLIENT_NOT_INITIALIZED_METRIC:Ljava/lang/String; = "WHISPERSYNC_CLIENT_NOT_INITIALIZED"

.field public static final WHISPERSYNC_CLOUD_SYNC_CLIENT_EXCEPTION_METRIC:Ljava/lang/String; = "WHISPERSYNC_CLOUD_SYNC_CLIENT_EXCEPTION"

.field public static final WHISPERSYNC_CLOUD_SYNC_IO_EXCEPTION_METRIC:Ljava/lang/String; = "WHISPERSYNC_CLOUD_SYNC_IO_EXCEPTION"

.field public static final WHISPERSYNC_CLOUD_SYNC_NETWORK_EXCEPTION_METRIC:Ljava/lang/String; = "WHISPERSYNC_CLOUD_SYNC_NETWORK_EXCEPTION"

.field public static final WHISPERSYNC_CLOUD_SYNC_SERVER_EXCEPTION_METRIC:Ljava/lang/String; = "WHISPERSYNC_CLOUD_SYNC_SERVER_EXCEPTION"

.field public static final WHISPERSYNC_CLOUD_SYNC_THROTTLED_METRIC:Ljava/lang/String; = "WHISPERSYNC_CLOUD_SYNC_THROTTLED"

.field public static final WHISPERSYNC_DISK_TO_MEMORY_SYNC_METRIC:Ljava/lang/String; = "WHISPERSYNC_DISK_TO_MEMORY_SYNC"

.field public static final WHISPERSYNC_DOWNLOAD_FROM_CLOUD_METRIC:Ljava/lang/String; = "WHISPERSYNC_DOWNLOAD_FROM_CLOUD"

.field public static final WHISPERSYNC_FULL_SYNC_METRIC:Ljava/lang/String; = "WHISPERSYNC_FULL_SYNC"

.field public static final WHISPERSYNC_MANUAL_FLUSH_METRIC:Ljava/lang/String; = "WHISPERSYNC_MANUAL_FLUSH"

.field public static final WHISPERSYNC_MANUAL_SYNC_METRIC:Ljava/lang/String; = "WHISPERSYNC_MANUAL_SYNC"

.field public static final WHISPERSYNC_MEMORY_TO_DISK_SYNC_METRIC:Ljava/lang/String; = "WHISPERSYNC_MEMORY_TO_DISK_SYNC"

.field public static final WHISPERSYNC_MERGE_ON_UPLOAD_METRIC:Ljava/lang/String; = "WHISPERSYNC_MERGE_ON_UPLOAD"

.field public static final WHISPERSYNC_SYNCABLE_TYPE_FAILED_MERGE_METRIC:Ljava/lang/String; = "WHISPERSYNC_SYNCABLE_TYPE_FAILED_MERGE"

.field public static final WHISPERSYNC_SYNCABLE_TYPE_UPDATE_FAILURE_METRIC:Ljava/lang/String; = "WHISPERSYNC_SYNCABLE_TYPE_UPDATE_FAILURE"

.field public static final WHISPERSYNC_UPLOAD_TO_CLOUD_METRIC:Ljava/lang/String; = "WHISPERSYNC_UPLOAD_TO_CLOUD"

.field public static final WHISPERSYNC_V1_MIGRATION_FAILURE_METRIC:Ljava/lang/String; = "WHISPERSYNC_V1_MIGRATION_FAILURE"

.field public static final WHISPERSYNC_V1_MIGRATION_METRIC:Ljava/lang/String; = "WHISPERSYNC_V1_MIGRATION"

.field public static final WHISPERSYNC_V1_MIGRATION_NO_DATA_METRIC:Ljava/lang/String; = "WHISPERSYNC_V1_MIGRATION_NO_DATA"

.field public static final WHISPERSYNC_V1_MIGRATION_NO_NETWORK_METRIC:Ljava/lang/String; = "WHISPERSYNC_V1_MIGRATION_NO_NETWORK"

.field public static final WHISPERSYNC_V1_MIGRATION_WHISPERSYNC_OFF_METRIC:Ljava/lang/String; = "WHISPERSYNC_V1_MIGRATION_WHISPERSYNC_OFF"

.field public static final WHISPERSYNC_V1_UNPACK_MULTIFILE_METRIC:Ljava/lang/String; = "WHISPERSYNC_V1_UNPACK_MULTIFILE"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method
