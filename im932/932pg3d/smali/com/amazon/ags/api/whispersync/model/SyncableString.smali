.class public interface abstract Lcom/amazon/ags/api/whispersync/model/SyncableString;
.super Ljava/lang/Object;
.source "SyncableString.java"

# interfaces
.implements Lcom/amazon/ags/api/whispersync/model/SyncableStringElement;


# virtual methods
.method public abstract isSet()Z
.end method

.method public abstract set(Ljava/lang/String;)V
.end method

.method public abstract set(Ljava/lang/String;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
