.class public interface abstract Lcom/amazon/ags/api/whispersync/model/SyncableNumberElement;
.super Ljava/lang/Object;
.source "SyncableNumberElement.java"

# interfaces
.implements Lcom/amazon/ags/api/whispersync/model/SyncableElement;


# virtual methods
.method public abstract asDecimal()Ljava/math/BigDecimal;
.end method

.method public abstract asDouble()D
.end method

.method public abstract asInt()I
.end method

.method public abstract asLong()J
.end method

.method public abstract asString()Ljava/lang/String;
.end method
