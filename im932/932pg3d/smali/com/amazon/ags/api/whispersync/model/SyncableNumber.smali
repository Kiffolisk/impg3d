.class public interface abstract Lcom/amazon/ags/api/whispersync/model/SyncableNumber;
.super Ljava/lang/Object;
.source "SyncableNumber.java"

# interfaces
.implements Lcom/amazon/ags/api/whispersync/model/SyncableNumberElement;


# virtual methods
.method public abstract isSet()Z
.end method

.method public abstract set(D)V
.end method

.method public abstract set(DLjava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract set(I)V
.end method

.method public abstract set(ILjava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract set(J)V
.end method

.method public abstract set(JLjava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract set(Ljava/lang/String;)V
.end method

.method public abstract set(Ljava/lang/String;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
