.class public interface abstract Lcom/amazon/ags/api/whispersync/model/SyncableAccumulatingNumber;
.super Ljava/lang/Object;
.source "SyncableAccumulatingNumber.java"


# virtual methods
.method public abstract asDecimal()Ljava/math/BigDecimal;
.end method

.method public abstract asDouble()D
.end method

.method public abstract asInt()I
.end method

.method public abstract asLong()J
.end method

.method public abstract asString()Ljava/lang/String;
.end method

.method public abstract decrement(D)V
.end method

.method public abstract decrement(I)V
.end method

.method public abstract decrement(J)V
.end method

.method public abstract decrement(Ljava/lang/String;)V
.end method

.method public abstract increment(D)V
.end method

.method public abstract increment(I)V
.end method

.method public abstract increment(J)V
.end method

.method public abstract increment(Ljava/lang/String;)V
.end method
