.class public interface abstract Lcom/amazon/ags/api/whispersync/model/SyncableDeveloperString;
.super Ljava/lang/Object;
.source "SyncableDeveloperString.java"


# virtual methods
.method public abstract getCloudTimestamp()J
.end method

.method public abstract getCloudValue()Ljava/lang/String;
.end method

.method public abstract getTimestamp()J
.end method

.method public abstract getValue()Ljava/lang/String;
.end method

.method public abstract inConflict()Z
.end method

.method public abstract isSet()Z
.end method

.method public abstract markAsResolved()V
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method
