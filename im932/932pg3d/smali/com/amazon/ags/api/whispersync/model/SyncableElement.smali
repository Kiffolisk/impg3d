.class public interface abstract Lcom/amazon/ags/api/whispersync/model/SyncableElement;
.super Ljava/lang/Object;
.source "SyncableElement.java"


# virtual methods
.method public abstract getMetadata()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTimestamp()J
.end method
