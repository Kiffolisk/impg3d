.class public interface abstract Lcom/amazon/ags/api/unity/LeaderboardsClientProxy;
.super Ljava/lang/Object;
.source "LeaderboardsClientProxy.java"


# virtual methods
.method public abstract requestLeaderboards(I)V
.end method

.method public abstract requestLocalPlayerScore(Ljava/lang/String;II)V
.end method

.method public abstract requestPercentileRanks(Ljava/lang/String;II)V
.end method

.method public abstract requestPercentileRanksForPlayer(Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract requestScoreForPlayer(Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract requestScores(Ljava/lang/String;II)V
.end method

.method public abstract showLeaderboardsOverlay()V
.end method

.method public abstract submitScore(Ljava/lang/String;JI)V
.end method
