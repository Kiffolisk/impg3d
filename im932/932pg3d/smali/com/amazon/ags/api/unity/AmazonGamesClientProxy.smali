.class public interface abstract Lcom/amazon/ags/api/unity/AmazonGamesClientProxy;
.super Ljava/lang/Object;
.source "AmazonGamesClientProxy.java"


# virtual methods
.method public abstract init(ZZZ)V
.end method

.method public abstract isReady()Z
.end method

.method public abstract release()V
.end method

.method public abstract setPopUpLocation(Ljava/lang/String;)V
.end method

.method public abstract setPopupEnabled(Z)V
.end method

.method public abstract showGameCircleOverlay()V
.end method

.method public abstract showSignInPage()V
.end method
