.class public interface abstract Lcom/amazon/ags/api/unity/AchievementsClientProxy;
.super Ljava/lang/Object;
.source "AchievementsClientProxy.java"


# virtual methods
.method public abstract requestAchievements(I)V
.end method

.method public abstract requestAchievementsForPlayer(Ljava/lang/String;I)V
.end method

.method public abstract showAchievementsOverlay()V
.end method

.method public abstract updateAchievementProgress(Ljava/lang/String;FI)V
.end method
