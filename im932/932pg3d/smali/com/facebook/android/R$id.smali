.class public final Lcom/facebook/android/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Button01:I = 0x7f0b0027

.field public static final Button02:I = 0x7f0b0026

.field public static final LinearLayout1:I = 0x7f0b0034

.field public static final RelativeLayout01:I = 0x7f0b0037

.field public static final RelativeLayout2:I = 0x7f0b003b

.field public static final TextView01:I = 0x7f0b003c

.field public static final TextView02:I = 0x7f0b0038

.field public static final actionListTitle:I = 0x7f0b0024

.field public static final action_settings:I = 0x7f0b009f

.field public static final actionlistButtonContainer:I = 0x7f0b0025

.field public static final appIcon:I = 0x7f0b0097

.field public static final approveCellular:I = 0x7f0b008f

.field public static final authLayoutProgressBar:I = 0x7f0b004e

.field public static final button:I = 0x7f0b0009

.field public static final button1:I = 0x7f0b0008

.field public static final buttonRow:I = 0x7f0b0092

.field public static final cancelButton:I = 0x7f0b008e

.field public static final com_facebook_body_frame:I = 0x7f0b001c

.field public static final com_facebook_button_xout:I = 0x7f0b001e

.field public static final com_facebook_login_activity_progress_bar:I = 0x7f0b000c

.field public static final com_facebook_picker_activity_circle:I = 0x7f0b000b

.field public static final com_facebook_picker_checkbox:I = 0x7f0b000e

.field public static final com_facebook_picker_checkbox_stub:I = 0x7f0b0012

.field public static final com_facebook_picker_divider:I = 0x7f0b0016

.field public static final com_facebook_picker_done_button:I = 0x7f0b0015

.field public static final com_facebook_picker_image:I = 0x7f0b000f

.field public static final com_facebook_picker_list_section_header:I = 0x7f0b0013

.field public static final com_facebook_picker_list_view:I = 0x7f0b000a

.field public static final com_facebook_picker_profile_pic_stub:I = 0x7f0b0010

.field public static final com_facebook_picker_row_activity_circle:I = 0x7f0b000d

.field public static final com_facebook_picker_search_text:I = 0x7f0b001b

.field public static final com_facebook_picker_title:I = 0x7f0b0011

.field public static final com_facebook_picker_title_bar:I = 0x7f0b0018

.field public static final com_facebook_picker_title_bar_stub:I = 0x7f0b0017

.field public static final com_facebook_picker_top_bar:I = 0x7f0b0014

.field public static final com_facebook_search_bar_view:I = 0x7f0b001a

.field public static final com_facebook_tooltip_bubble_view_bottom_pointer:I = 0x7f0b0020

.field public static final com_facebook_tooltip_bubble_view_text_body:I = 0x7f0b001f

.field public static final com_facebook_tooltip_bubble_view_top_pointer:I = 0x7f0b001d

.field public static final com_facebook_usersettingsfragment_login_button:I = 0x7f0b0023

.field public static final com_facebook_usersettingsfragment_logo_image:I = 0x7f0b0021

.field public static final com_facebook_usersettingsfragment_profile_name:I = 0x7f0b0022

.field public static final commentButtonBarButton:I = 0x7f0b006a

.field public static final description:I = 0x7f0b009d

.field public static final downloaderDashboard:I = 0x7f0b0087

.field public static final editorBrowseButton:I = 0x7f0b0035

.field public static final editorProcessingProgressBar:I = 0x7f0b003e

.field public static final editorProcessingText:I = 0x7f0b003d

.field public static final editorRangeSlider:I = 0x7f0b0041

.field public static final editorShareButton:I = 0x7f0b0039

.field public static final editorTimeElapsedTextView:I = 0x7f0b0040

.field public static final editorTimeLeftTextView:I = 0x7f0b0042

.field public static final editorTrimButton:I = 0x7f0b0043

.field public static final editorTrimmerContainer:I = 0x7f0b003f

.field public static final editorUndoTrimButton:I = 0x7f0b0044

.field public static final endScreenScrollViewContent:I = 0x7f0b0071

.field public static final endscreenInstallButton:I = 0x7f0b0072

.field public static final endscreenInstallButtonImage:I = 0x7f0b0073

.field public static final endscreenInstallButtonText:I = 0x7f0b0074

.field public static final endscreenScrollView:I = 0x7f0b0070

.field public static final endscreenThumb:I = 0x7f0b0076

.field public static final endscreenVideoContainer:I = 0x7f0b0075

.field public static final everyplayAuthProgressBar:I = 0x7f0b002b

.field public static final everyplayAuthTopBar:I = 0x7f0b0029

.field public static final everyplayAuthWebView:I = 0x7f0b002a

.field public static final everyplayBrowserBottomBar:I = 0x7f0b0031

.field public static final everyplayBrowserButtonBack:I = 0x7f0b002c

.field public static final everyplayBrowserButtonForward:I = 0x7f0b002d

.field public static final everyplayBrowserButtonRefresh:I = 0x7f0b002e

.field public static final everyplayBrowserButtonShare:I = 0x7f0b002f

.field public static final everyplayBrowserProgressBar:I = 0x7f0b0033

.field public static final everyplayBrowserTopBar:I = 0x7f0b0030

.field public static final everyplayBrowserWebView:I = 0x7f0b0032

.field public static final everyplayButtonBarButton:I = 0x7f0b0069

.field public static final everyplayControlPauseButton:I = 0x7f0b006e

.field public static final everyplayControlPlayButton:I = 0x7f0b006d

.field public static final everyplayControlReplayButton:I = 0x7f0b006f

.field public static final everyplayFaceCamVideoPlayer:I = 0x7f0b0079

.field public static final everyplayFaceCamVideoPlayerContainer:I = 0x7f0b0078

.field public static final everyplayGroupLabelText:I = 0x7f0b004d

.field public static final everyplayMicrophoneAvatar:I = 0x7f0b007f

.field public static final everyplayNativeOverlaySideMenu:I = 0x7f0b0050

.field public static final everyplayNativeOverlayTopBar:I = 0x7f0b005f

.field public static final everyplayShowFaceCamButton:I = 0x7f0b007a

.field public static final everyplayShowMicrophoneAvatar:I = 0x7f0b0080

.field public static final everyplaySocialLayoutSideMenu:I = 0x7f0b004f

.field public static final everyplaySocialLayoutSplashScreen:I = 0x7f0b0054

.field public static final everyplaySocialLayoutTopBar:I = 0x7f0b0052

.field public static final everyplaySocialLayoutWebView:I = 0x7f0b0051

.field public static final everyplaySocialLayoutWebViewCover:I = 0x7f0b0053

.field public static final everyplayTextInputDone:I = 0x7f0b0067

.field public static final everyplayTextInputInput:I = 0x7f0b0066

.field public static final everyplay_topbar_notification:I = 0x7f0b009e

.field public static final hybrid:I = 0x7f0b0000

.field public static final iconAndTextButtonIcon:I = 0x7f0b0036

.field public static final imageView2:I = 0x7f0b003a

.field public static final installButton:I = 0x7f0b0028

.field public static final large:I = 0x7f0b0005

.field public static final likeButtonBarButton:I = 0x7f0b006b

.field public static final modal_overlay_container:I = 0x7f0b0095

.field public static final none:I = 0x7f0b0001

.field public static final normal:I = 0x7f0b0002

.field public static final notificationLayout:I = 0x7f0b0096

.field public static final pauseButton:I = 0x7f0b008d

.field public static final picker_subtitle:I = 0x7f0b0019

.field public static final progressAsFraction:I = 0x7f0b0088

.field public static final progressAsPercentage:I = 0x7f0b0089

.field public static final progressAverageSpeed:I = 0x7f0b008b

.field public static final progressBar:I = 0x7f0b008a

.field public static final progressTimeRemaining:I = 0x7f0b008c

.field public static final progress_bar:I = 0x7f0b009c

.field public static final progress_bar_frame:I = 0x7f0b009b

.field public static final progress_text:I = 0x7f0b0098

.field public static final resumeOverCellular:I = 0x7f0b0093

.field public static final satellite:I = 0x7f0b0003

.field public static final shareButtonBarButton:I = 0x7f0b006c

.field public static final sideMenuButtonBadge:I = 0x7f0b004b

.field public static final sideMenuButtonDescription:I = 0x7f0b004a

.field public static final sideMenuButtonHeader:I = 0x7f0b0049

.field public static final sideMenuButtonIcon:I = 0x7f0b0047

.field public static final sideMenuButtonSecondaryBadge:I = 0x7f0b004c

.field public static final sideMenuButtonTextContainer:I = 0x7f0b0048

.field public static final sideMenuItemsContainer:I = 0x7f0b0046

.field public static final sideMenuScrollView:I = 0x7f0b0045

.field public static final small:I = 0x7f0b0006

.field public static final socialTopBarActionButton:I = 0x7f0b005e

.field public static final socialTopBarCenterLabel:I = 0x7f0b0059

.field public static final socialTopBarCloseButton:I = 0x7f0b0058

.field public static final socialTopBarEveryplayLogo:I = 0x7f0b005a

.field public static final socialTopBarMenuButton:I = 0x7f0b0055

.field public static final socialTopBarModalIcon:I = 0x7f0b0056

.field public static final socialTopBarNotificationMarker:I = 0x7f0b0057

.field public static final socialTopBarUploadLayout:I = 0x7f0b005b

.field public static final socialTopBarUploadProgressBar:I = 0x7f0b005c

.field public static final socialTopBarUploadText:I = 0x7f0b005d

.field public static final splashImage:I = 0x7f0b0085

.field public static final splashScreenCloseButton:I = 0x7f0b0065

.field public static final splashScreenLinearLayout:I = 0x7f0b0060

.field public static final splashScreenLogo:I = 0x7f0b0061

.field public static final splashScreenProgressBar:I = 0x7f0b0062

.field public static final splashScreenRetryButton:I = 0x7f0b0064

.field public static final splashScreenStatusText:I = 0x7f0b0063

.field public static final statusText:I = 0x7f0b0086

.field public static final terrain:I = 0x7f0b0004

.field public static final textPausedParagraph1:I = 0x7f0b0090

.field public static final textPausedParagraph2:I = 0x7f0b0091

.field public static final textView1:I = 0x7f0b0077

.field public static final time_remaining:I = 0x7f0b009a

.field public static final title:I = 0x7f0b0099

.field public static final userAvatarButton:I = 0x7f0b007c

.field public static final videoDescription:I = 0x7f0b007d

.field public static final videoPlayerHeader:I = 0x7f0b007b

.field public static final videoQualityButtonBarButton:I = 0x7f0b0068

.field public static final videoRangeSlider:I = 0x7f0b0083

.field public static final videoTimeElapsedTextView:I = 0x7f0b0082

.field public static final videoTimeLeftTextView:I = 0x7f0b0084

.field public static final videoTimelineContainer:I = 0x7f0b0081

.field public static final videoUploaderUsername:I = 0x7f0b007e

.field public static final webView:I = 0x7f0b0007

.field public static final wifiSettingsButton:I = 0x7f0b0094


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
