.class public final Lcom/facebook/android/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final action_settings:I = 0x7f050077

.field public static final app_name:I = 0x7f050076

.field public static final auth_client_needs_enabling_title:I = 0x7f050037

.field public static final auth_client_needs_installation_title:I = 0x7f050038

.field public static final auth_client_needs_update_title:I = 0x7f050039

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f05003a

.field public static final auth_client_requested_by_msg:I = 0x7f05003b

.field public static final auth_client_using_bad_version_title:I = 0x7f050036

.field public static final com_facebook_choose_friends:I = 0x7f05004c

.field public static final com_facebook_dialogloginactivity_ok_button:I = 0x7f05003d

.field public static final com_facebook_internet_permission_error_message:I = 0x7f050050

.field public static final com_facebook_internet_permission_error_title:I = 0x7f05004f

.field public static final com_facebook_loading:I = 0x7f05004e

.field public static final com_facebook_loginview_cancel_action:I = 0x7f050043

.field public static final com_facebook_loginview_log_in_button:I = 0x7f05003f

.field public static final com_facebook_loginview_log_out_action:I = 0x7f050042

.field public static final com_facebook_loginview_log_out_button:I = 0x7f05003e

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f050040

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f050041

.field public static final com_facebook_logo_content_description:I = 0x7f050044

.field public static final com_facebook_nearby:I = 0x7f05004d

.field public static final com_facebook_picker_done_button_text:I = 0x7f05004b

.field public static final com_facebook_placepicker_subtitle_catetory_only_format:I = 0x7f050049

.field public static final com_facebook_placepicker_subtitle_format:I = 0x7f050048

.field public static final com_facebook_placepicker_subtitle_were_here_only_format:I = 0x7f05004a

.field public static final com_facebook_requesterror_password_changed:I = 0x7f050053

.field public static final com_facebook_requesterror_permissions:I = 0x7f050055

.field public static final com_facebook_requesterror_reconnect:I = 0x7f050054

.field public static final com_facebook_requesterror_relogin:I = 0x7f050052

.field public static final com_facebook_requesterror_web_login:I = 0x7f050051

.field public static final com_facebook_tooltip_default:I = 0x7f050056

.field public static final com_facebook_usersettingsfragment_log_in_button:I = 0x7f050045

.field public static final com_facebook_usersettingsfragment_logged_in:I = 0x7f050046

.field public static final com_facebook_usersettingsfragment_not_logged_in:I = 0x7f050047

.field public static final common_google_play_services_enable_button:I = 0x7f050028

.field public static final common_google_play_services_enable_text:I = 0x7f050027

.field public static final common_google_play_services_enable_title:I = 0x7f050026

.field public static final common_google_play_services_install_button:I = 0x7f050025

.field public static final common_google_play_services_install_text_phone:I = 0x7f050023

.field public static final common_google_play_services_install_text_tablet:I = 0x7f050024

.field public static final common_google_play_services_install_title:I = 0x7f050022

.field public static final common_google_play_services_invalid_account_text:I = 0x7f05002e

.field public static final common_google_play_services_invalid_account_title:I = 0x7f05002d

.field public static final common_google_play_services_network_error_text:I = 0x7f05002c

.field public static final common_google_play_services_network_error_title:I = 0x7f05002b

.field public static final common_google_play_services_unknown_issue:I = 0x7f05002f

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f050032

.field public static final common_google_play_services_unsupported_text:I = 0x7f050031

.field public static final common_google_play_services_unsupported_title:I = 0x7f050030

.field public static final common_google_play_services_update_button:I = 0x7f050033

.field public static final common_google_play_services_update_text:I = 0x7f05002a

.field public static final common_google_play_services_update_title:I = 0x7f050029

.field public static final common_signin_button_text:I = 0x7f050034

.field public static final common_signin_button_text_long:I = 0x7f050035

.field public static final everyplay_app_name:I = 0x7f050057

.field public static final everyplay_browse_text:I = 0x7f05005f

.field public static final everyplay_by_text:I = 0x7f05005d

.field public static final everyplay_by_username_text:I = 0x7f05006c

.field public static final everyplay_cancel_text:I = 0x7f050061

.field public static final everyplay_close:I = 0x7f05005e

.field public static final everyplay_close_confirmation_text:I = 0x7f050071

.field public static final everyplay_connection_failed:I = 0x7f05006e

.field public static final everyplay_connection_success:I = 0x7f050070

.field public static final everyplay_connection_timeout:I = 0x7f05006f

.field public static final everyplay_default_button_label:I = 0x7f050065

.field public static final everyplay_default_title:I = 0x7f050064

.field public static final everyplay_done_text:I = 0x7f050062

.field public static final everyplay_editor_processing_text:I = 0x7f05006d

.field public static final everyplay_empty_string:I = 0x7f050069

.field public static final everyplay_install_game_text:I = 0x7f05005b

.field public static final everyplay_launch_game_text:I = 0x7f05005c

.field public static final everyplay_loading_text:I = 0x7f050063

.field public static final everyplay_retry_text:I = 0x7f050066

.field public static final everyplay_share_text:I = 0x7f050060

.field public static final everyplay_text:I = 0x7f05005a

.field public static final everyplay_unable_to_play_video_text:I = 0x7f05006a

.field public static final everyplay_upload_failed_text:I = 0x7f050059

.field public static final everyplay_upload_text:I = 0x7f050058

.field public static final everyplay_video_description_text:I = 0x7f05006b

.field public static final everyplay_zero_text:I = 0x7f050068

.field public static final everyplay_zero_time_text:I = 0x7f050067

.field public static final gamehelper_app_misconfigured:I = 0x7f050073

.field public static final gamehelper_license_failed:I = 0x7f050074

.field public static final gamehelper_sign_in_failed:I = 0x7f050072

.field public static final gamehelper_unknown_error:I = 0x7f050075

.field public static final hello_world:I = 0x7f050078

.field public static final kilobytes_per_second:I = 0x7f05001f

.field public static final location_client_powered_by_google:I = 0x7f05003c

.field public static final notification_download_complete:I = 0x7f05000b

.field public static final notification_download_failed:I = 0x7f05000c

.field public static final state_completed:I = 0x7f050012

.field public static final state_connecting:I = 0x7f050010

.field public static final state_downloading:I = 0x7f050011

.field public static final state_failed:I = 0x7f05001e

.field public static final state_failed_cancelled:I = 0x7f05001d

.field public static final state_failed_fetching_url:I = 0x7f05001b

.field public static final state_failed_sdcard_full:I = 0x7f05001c

.field public static final state_failed_unlicensed:I = 0x7f05001a

.field public static final state_fetching_url:I = 0x7f05000f

.field public static final state_idle:I = 0x7f05000e

.field public static final state_paused_by_request:I = 0x7f050015

.field public static final state_paused_network_setup_failure:I = 0x7f050014

.field public static final state_paused_network_unavailable:I = 0x7f050013

.field public static final state_paused_roaming:I = 0x7f050018

.field public static final state_paused_sdcard_unavailable:I = 0x7f050019

.field public static final state_paused_wifi_disabled:I = 0x7f050017

.field public static final state_paused_wifi_unavailable:I = 0x7f050016

.field public static final state_unknown:I = 0x7f05000d

.field public static final text_button_cancel:I = 0x7f050009

.field public static final text_button_cancel_verify:I = 0x7f05000a

.field public static final text_button_pause:I = 0x7f050007

.field public static final text_button_resume:I = 0x7f050008

.field public static final text_button_resume_cellular:I = 0x7f050002

.field public static final text_button_wifi_settings:I = 0x7f050003

.field public static final text_paused_cellular:I = 0x7f050000

.field public static final text_paused_cellular_2:I = 0x7f050001

.field public static final text_validation_complete:I = 0x7f050005

.field public static final text_validation_failed:I = 0x7f050006

.field public static final text_verifying_download:I = 0x7f050004

.field public static final time_remaining:I = 0x7f050020

.field public static final time_remaining_notification:I = 0x7f050021

.field public static final title_activity_web_view:I = 0x7f050079


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
