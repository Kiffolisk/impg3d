.class Lcom/pixel/gun3d/GCMIntentService$1;
.super Ljava/lang/Object;
.source "GCMIntentService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/pixel/gun3d/GCMIntentService;->generateNotification(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/pixel/gun3d/GCMIntentService;


# direct methods
.method constructor <init>(Lcom/pixel/gun3d/GCMIntentService;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/pixel/gun3d/GCMIntentService$1;->this$0:Lcom/pixel/gun3d/GCMIntentService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 99
    iget-object v0, p0, Lcom/pixel/gun3d/GCMIntentService$1;->this$0:Lcom/pixel/gun3d/GCMIntentService;

    .line 100
    .local v0, "context":Landroid/content/Context;
    iget-object v8, p0, Lcom/pixel/gun3d/GCMIntentService$1;->this$0:Lcom/pixel/gun3d/GCMIntentService;

    invoke-virtual {v8}, Lcom/pixel/gun3d/GCMIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const-string v9, "ic_launcher"

    const-string v10, "drawable"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 101
    .local v1, "icon":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 102
    .local v6, "when":J
    const-string v8, "notification"

    invoke-virtual {v0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 104
    .local v5, "notificationManager":Landroid/app/NotificationManager;
    new-instance v3, Landroid/app/Notification;

    sget-object v8, Lcom/pixel/gun3d/GCMIntentService;->message:Ljava/lang/String;

    invoke-direct {v3, v1, v8, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 108
    .local v3, "notification":Landroid/app/Notification;
    new-instance v4, Landroid/content/Intent;

    const-class v8, Lcom/prime31/UnityPlayerNativeActivity;

    invoke-direct {v4, v0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .local v4, "notificationIntent":Landroid/content/Intent;
    const/high16 v8, 0x24000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 112
    invoke-static {v0, v12, v4, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 114
    .local v2, "intent":Landroid/app/PendingIntent;
    const-string v8, "Pixel Gun 3D"

    sget-object v9, Lcom/pixel/gun3d/GCMIntentService;->message:Ljava/lang/String;

    invoke-virtual {v3, v0, v8, v9, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 117
    iget v8, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v8, v8, 0x10

    iput v8, v3, Landroid/app/Notification;->flags:I

    .line 120
    invoke-virtual {v5, v12, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 121
    return-void
.end method
