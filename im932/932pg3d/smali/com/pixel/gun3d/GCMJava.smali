.class public Lcom/pixel/gun3d/GCMJava;
.super Landroid/app/IntentService;
.source "GCMJava.java"


# static fields
.field static isRegistration:Ljava/lang/Boolean;


# instance fields
.field DISPLAY_MESSAGE_ACTION:Ljava/lang/String;

.field EXTRA_MESSAGE:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "GCMJava"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/pixel/gun3d/GCMJava;->DISPLAY_MESSAGE_ACTION:Ljava/lang/String;

    .line 18
    const-string v0, "message"

    iput-object v0, p0, Lcom/pixel/gun3d/GCMJava;->EXTRA_MESSAGE:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 28
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "isRegistration"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 29
    .local v1, "isRegistration":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    .line 31
    invoke-static {p0}, Lcom/google/android/gcm/GCMRegistrar;->unregister(Landroid/content/Context;)V

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    const-string v3, "senderID"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "SENDER_ID":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/gcm/GCMRegistrar;->checkDevice(Landroid/content/Context;)V

    .line 38
    invoke-static {p0}, Lcom/google/android/gcm/GCMRegistrar;->checkManifest(Landroid/content/Context;)V

    .line 41
    invoke-static {p0}, Lcom/google/android/gcm/GCMRegistrar;->getRegistrationId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "regId":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 46
    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {p0, v3}, Lcom/google/android/gcm/GCMRegistrar;->register(Landroid/content/Context;[Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_2
    invoke-virtual {p0, v2}, Lcom/pixel/gun3d/GCMJava;->sendConfirmRegistration(Ljava/lang/String;)V

    .line 51
    invoke-static {p0}, Lcom/google/android/gcm/GCMRegistrar;->isRegisteredOnServer(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 52
    invoke-static {p0, v5}, Lcom/google/android/gcm/GCMRegistrar;->setRegisteredOnServer(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public sendConfirmRegistration(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 95
    const-string v0, "ECPNManager"

    const-string v1, "RegisterAndroidDevice"

    invoke-static {v0, v1, p1}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method
