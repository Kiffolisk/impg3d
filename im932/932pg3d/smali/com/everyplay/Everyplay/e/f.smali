.class public final Lcom/everyplay/Everyplay/e/f;
.super Ljava/lang/Object;


# static fields
.field public static a:Lcom/everyplay/Everyplay/e/h;

.field public static b:Lcom/everyplay/Everyplay/e/i;

.field public static c:Z

.field public static d:Lcom/everyplay/Everyplay/e/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/everyplay/Everyplay/e/h;->a:Lcom/everyplay/Everyplay/e/h;

    sput-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    sget-object v0, Lcom/everyplay/Everyplay/e/i;->a:Lcom/everyplay/Everyplay/e/i;

    sput-object v0, Lcom/everyplay/Everyplay/e/f;->b:Lcom/everyplay/Everyplay/e/i;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/everyplay/Everyplay/e/f;->c:Z

    sget-object v0, Lcom/everyplay/Everyplay/e/g;->a:Lcom/everyplay/Everyplay/e/g;

    sput-object v0, Lcom/everyplay/Everyplay/e/f;->d:Lcom/everyplay/Everyplay/e/g;

    return-void
.end method

.method public static a()Z
    .locals 2

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.everyplay.everyplayapp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    sget-object v0, Lcom/everyplay/Everyplay/e/f;->d:Lcom/everyplay/Everyplay/e/g;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/everyplay/Everyplay/e/f;->d:Lcom/everyplay/Everyplay/e/g;

    sget-object v1, Lcom/everyplay/Everyplay/e/g;->b:Lcom/everyplay/Everyplay/e/g;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->a()Z

    move-result v3

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-nez v3, :cond_0

    const-string v4, "Everyplay listener not set"

    invoke-static {v4}, Lcom/everyplay/Everyplay/d/e;->a(Ljava/lang/String;)V

    :cond_0
    if-nez v2, :cond_1

    const-string v4, "Activity not set"

    invoke-static {v4}, Lcom/everyplay/Everyplay/d/e;->a(Ljava/lang/String;)V

    :cond_1
    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    :goto_1
    return v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static d()Z
    .locals 1

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/everyplay/Everyplay/view/f;

    return v0
.end method

.method public static e()Landroid/app/Activity;
    .locals 1

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static f()Landroid/app/Activity;
    .locals 1

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lorg/json/JSONObject;
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/everyplay/Everyplay/c/d;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/everyplay/Everyplay/c/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/everyplay/Everyplay/c/d;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/everyplay/Everyplay/communication/g;->b()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/everyplay/Everyplay/c/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    :try_start_1
    const-string v7, "version"

    const-string v8, "1"

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "build"

    const-string v8, "1240"

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "platform"

    const-string v8, "android"

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "platformVersion"

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v7, "deviceType"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/everyplay/Everyplay/d/a;->h()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/everyplay/Everyplay/d/a;->i()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "tablet"

    invoke-static {}, Lcom/everyplay/Everyplay/d/a;->b()Z

    move-result v8

    if-eqz v8, :cond_5

    :goto_1
    invoke-virtual {v3, v7, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "webview"

    const/4 v2, 0x1

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "standaloneApp"

    invoke-static {}, Lcom/everyplay/Everyplay/e/f;->a()Z

    move-result v2

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "sandbox"

    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "jailbroken"

    invoke-static {}, Lcom/everyplay/Everyplay/d/a;->k()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "hasRespondsTo"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "hasNativeAuth"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "hasNativePlayer"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "hasNativeTopBar"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "hasNativeTopBarActionButton"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "nativeTopBarHeightLandscape"

    const/16 v1, 0x2d

    invoke-static {v1}, Lcom/everyplay/Everyplay/e/a;->a(I)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "nativeTopBarHeightPortrait"

    const/16 v1, 0x2d

    invoke-static {v1}, Lcom/everyplay/Everyplay/e/a;->a(I)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "hasNativeTopBarOverlay"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "hasOAuth2"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "hasSessionSupport"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "canUseAdvertisingTracking"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "deviceId"

    invoke-static {}, Lcom/everyplay/Everyplay/d/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "openUDID"

    invoke-static {}, Lcom/everyplay/Everyplay/d/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "odin1"

    invoke-static {}, Lcom/everyplay/Everyplay/d/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "unhashedAdvertisingIndentifier"

    invoke-static {}, Lcom/everyplay/Everyplay/d/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "advertisingIdentifier"

    invoke-static {}, Lcom/everyplay/Everyplay/d/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "macAddress"

    invoke-static {}, Lcom/everyplay/Everyplay/d/d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "clientId"

    sget-object v1, Lcom/everyplay/Everyplay/e/b;->a:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "hasFacebookSDK"

    invoke-static {}, Lcom/everyplay/Everyplay/communication/socialnetworks/EveryplayFacebook;->a()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    invoke-static {}, Lcom/everyplay/Everyplay/c/r;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Lcom/everyplay/Everyplay/c/r;->b(Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "theme"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    if-eqz v4, :cond_1

    const-string v0, "keyHash"

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    if-eqz v5, :cond_2

    const-string v0, "packageName"

    invoke-virtual {v3, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    if-eqz v6, :cond_3

    const-string v0, "installerPackageName"

    invoke-virtual {v3, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    return-object v3

    :catch_0
    move-exception v0

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    move v2, v1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    const-string v1, "Error while creating webapp settings: "

    invoke-static {v1}, Lcom/everyplay/Everyplay/d/e;->c(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method
