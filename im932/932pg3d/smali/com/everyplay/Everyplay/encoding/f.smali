.class public final Lcom/everyplay/Everyplay/encoding/f;
.super Ljava/lang/Object;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field a:I

.field b:I

.field c:Z

.field d:Landroid/media/MediaCodec;

.field e:Landroid/media/MediaCodec$BufferInfo;

.field f:[Ljava/nio/ByteBuffer;

.field g:I

.field h:Landroid/media/MediaFormat;

.field i:Landroid/media/MediaFormat;

.field j:Landroid/media/MediaCodecInfo;

.field k:Ljava/lang/String;

.field l:J

.field m:J

.field n:J

.field o:J

.field p:Z

.field q:Z

.field r:Z

.field s:Ljava/lang/ref/WeakReference;

.field private t:[Ljava/nio/ByteBuffer;

.field private u:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/everyplay/Everyplay/encoding/b;)V
    .locals 8

    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/everyplay/Everyplay/encoding/f;->a:I

    iput v2, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    iput-boolean v1, p0, Lcom/everyplay/Everyplay/encoding/f;->c:Z

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->e:Landroid/media/MediaCodec$BufferInfo;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->t:[Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->f:[Ljava/nio/ByteBuffer;

    iput v2, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->h:Landroid/media/MediaFormat;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->i:Landroid/media/MediaFormat;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->j:Landroid/media/MediaCodecInfo;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->k:Ljava/lang/String;

    iput-wide v4, p0, Lcom/everyplay/Everyplay/encoding/f;->l:J

    iput-wide v4, p0, Lcom/everyplay/Everyplay/encoding/f;->m:J

    iput-wide v6, p0, Lcom/everyplay/Everyplay/encoding/f;->n:J

    iput-wide v6, p0, Lcom/everyplay/Everyplay/encoding/f;->o:J

    iput-boolean v1, p0, Lcom/everyplay/Everyplay/encoding/f;->p:Z

    iput-boolean v1, p0, Lcom/everyplay/Everyplay/encoding/f;->q:Z

    iput-boolean v1, p0, Lcom/everyplay/Everyplay/encoding/f;->r:Z

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->s:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->u:Ljava/lang/Object;

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->s:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->s:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 5

    iget-wide v0, p0, Lcom/everyplay/Everyplay/encoding/f;->l:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iput-wide p1, p0, Lcom/everyplay/Everyplay/encoding/f;->l:J

    :cond_0
    iput-wide p1, p0, Lcom/everyplay/Everyplay/encoding/f;->m:J

    return-void
.end method


# virtual methods
.method public final a(IIJI)V
    .locals 9

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/everyplay/Everyplay/encoding/f;->p:Z

    if-nez v0, :cond_2

    iget-object v7, p0, Lcom/everyplay/Everyplay/encoding/f;->u:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-wide v0, p0, Lcom/everyplay/Everyplay/encoding/f;->m:J

    cmp-long v0, v0, p3

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    move v1, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    invoke-direct {p0, p3, p4}, Lcom/everyplay/Everyplay/encoding/f;->a(J)V

    :goto_0
    monitor-exit v7

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "queueInputBuffer "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timeStamp invalid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/everyplay/Everyplay/encoding/f;->m:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_2
    iget-boolean v0, p0, Lcom/everyplay/Everyplay/encoding/f;->p:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "queueInputBuffer "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " EOS was already signaled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method public final a()Z
    .locals 6

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->j:Landroid/media/MediaCodecInfo;

    invoke-virtual {v1}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    :cond_0
    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->i:Landroid/media/MediaFormat;

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->h:Landroid/media/MediaFormat;

    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    iget-object v2, p0, Lcom/everyplay/Everyplay/encoding/f;->h:Landroid/media/MediaFormat;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error configuring "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MediaCodec: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->t:[Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->f:[Ljava/nio/ByteBuffer;

    :try_start_1
    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->f:[Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    new-instance v1, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v1}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->e:Landroid/media/MediaCodec$BufferInfo;

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/encoding/f;->p:Z

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/encoding/f;->q:Z

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/encoding/f;->r:Z

    iput-wide v2, p0, Lcom/everyplay/Everyplay/encoding/f;->l:J

    invoke-direct {p0, v2, v3}, Lcom/everyplay/Everyplay/encoding/f;->a(J)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/everyplay/Everyplay/encoding/f;->n:J

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error starting "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " encoder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public final c()Z
    .locals 10

    const/4 v7, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/everyplay/Everyplay/encoding/f;->m:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/everyplay/Everyplay/encoding/f;->p:Z

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    iput v1, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget v1, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    if-ltz v1, :cond_1

    :try_start_1
    iget v2, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/everyplay/Everyplay/encoding/f;->m:J

    const-wide/32 v8, 0x186a0

    add-long/2addr v4, v8

    const/4 v6, 0x4

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/everyplay/Everyplay/encoding/f;->a(IIJI)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/everyplay/Everyplay/encoding/f;->p:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Flushing "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " encoder: firstTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/everyplay/Everyplay/encoding/f;->l:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lastTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/everyplay/Everyplay/encoding/f;->m:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_0
    :goto_0
    move v0, v7

    :goto_1
    return v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dequeueInputBuffer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MediaCodec: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Flushing "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " encoder: _inputIndex not valid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final d()Ljava/nio/ByteBuffer;
    .locals 4

    :try_start_0
    iget v0, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->d:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    :cond_0
    iget v0, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/f;->t:[Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/everyplay/Everyplay/encoding/f;->g:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dequeueInputBuffer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/everyplay/Everyplay/encoding/f;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MediaCodec: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
