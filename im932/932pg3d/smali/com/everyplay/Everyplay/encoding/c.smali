.class final Lcom/everyplay/Everyplay/encoding/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/everyplay/Everyplay/encoding/b;

.field private b:Lcom/everyplay/Everyplay/encoding/d;

.field private c:Lcom/everyplay/Everyplay/encoding/f;

.field private d:Lcom/everyplay/Everyplay/encoding/f;

.field private e:Lcom/everyplay/Everyplay/encoding/b;


# direct methods
.method public constructor <init>(Lcom/everyplay/Everyplay/encoding/b;Lcom/everyplay/Everyplay/encoding/b;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/everyplay/Everyplay/encoding/d;->b:Lcom/everyplay/Everyplay/encoding/d;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->b:Lcom/everyplay/Everyplay/encoding/d;

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/c;->c:Lcom/everyplay/Everyplay/encoding/f;

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/c;->d:Lcom/everyplay/Everyplay/encoding/f;

    iput-object v1, p0, Lcom/everyplay/Everyplay/encoding/c;->e:Lcom/everyplay/Everyplay/encoding/b;

    iput-object p2, p0, Lcom/everyplay/Everyplay/encoding/c;->e:Lcom/everyplay/Everyplay/encoding/b;

    return-void
.end method


# virtual methods
.method public final a(Lcom/everyplay/Everyplay/encoding/d;)V
    .locals 0

    iput-object p1, p0, Lcom/everyplay/Everyplay/encoding/c;->b:Lcom/everyplay/Everyplay/encoding/d;

    return-void
.end method

.method public final run()V
    .locals 12

    const/4 v11, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v10, 0x1

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->a(Lcom/everyplay/Everyplay/encoding/b;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/everyplay/Everyplay/encoding/f;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->c:Lcom/everyplay/Everyplay/encoding/f;

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->a(Lcom/everyplay/Everyplay/encoding/b;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/everyplay/Everyplay/encoding/f;

    iput-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->d:Lcom/everyplay/Everyplay/encoding/f;

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->b(Lcom/everyplay/Everyplay/encoding/b;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->c(Lcom/everyplay/Everyplay/encoding/b;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->d(Lcom/everyplay/Everyplay/encoding/b;)Lcom/everyplay/Everyplay/communication/ay;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->d(Lcom/everyplay/Everyplay/encoding/b;)Lcom/everyplay/Everyplay/communication/ay;

    move-result-object v0

    iget-object v2, p0, Lcom/everyplay/Everyplay/encoding/c;->e:Lcom/everyplay/Everyplay/encoding/b;

    invoke-interface {v0}, Lcom/everyplay/Everyplay/communication/ay;->a()V

    :cond_0
    :goto_0
    const-wide/16 v4, 0x3

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->c:Lcom/everyplay/Everyplay/encoding/f;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    iget-object v2, p0, Lcom/everyplay/Everyplay/encoding/c;->c:Lcom/everyplay/Everyplay/encoding/f;

    invoke-static {v0, v2}, Lcom/everyplay/Everyplay/encoding/b;->a(Lcom/everyplay/Everyplay/encoding/b;Lcom/everyplay/Everyplay/encoding/f;)I

    move-result v0

    :goto_1
    iget-object v2, p0, Lcom/everyplay/Everyplay/encoding/c;->d:Lcom/everyplay/Everyplay/encoding/f;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    iget-object v3, p0, Lcom/everyplay/Everyplay/encoding/c;->d:Lcom/everyplay/Everyplay/encoding/f;

    invoke-static {v2, v3}, Lcom/everyplay/Everyplay/encoding/b;->a(Lcom/everyplay/Everyplay/encoding/b;Lcom/everyplay/Everyplay/encoding/f;)I

    move-result v2

    :goto_2
    iget-object v3, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v3}, Lcom/everyplay/Everyplay/encoding/b;->e(Lcom/everyplay/Everyplay/encoding/b;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v3}, Lcom/everyplay/Everyplay/encoding/b;->f(Lcom/everyplay/Everyplay/encoding/b;)Z

    move-result v3

    if-ne v3, v10, :cond_1

    iget-object v3, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v3}, Lcom/everyplay/Everyplay/encoding/b;->g(Lcom/everyplay/Everyplay/encoding/b;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v3}, Lcom/everyplay/Everyplay/encoding/b;->h(Lcom/everyplay/Everyplay/encoding/b;)I

    move-result v3

    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v6}, Lcom/everyplay/Everyplay/encoding/b;->i(Lcom/everyplay/Everyplay/encoding/b;)I

    move-result v6

    if-ne v3, v6, :cond_1

    iget-object v3, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v3, v10}, Lcom/everyplay/Everyplay/encoding/b;->a(Lcom/everyplay/Everyplay/encoding/b;Z)Z

    :cond_1
    iget-object v3, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v3}, Lcom/everyplay/Everyplay/encoding/b;->j(Lcom/everyplay/Everyplay/encoding/b;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v6}, Lcom/everyplay/Everyplay/encoding/b;->f(Lcom/everyplay/Everyplay/encoding/b;)Z

    move-result v6

    if-nez v6, :cond_3

    if-gez v0, :cond_3

    if-gez v2, :cond_3

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->k(Lcom/everyplay/Everyplay/encoding/b;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->l(Lcom/everyplay/Everyplay/encoding/b;)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_3
    iput-object v11, p0, Lcom/everyplay/Everyplay/encoding/c;->c:Lcom/everyplay/Everyplay/encoding/f;

    iput-object v11, p0, Lcom/everyplay/Everyplay/encoding/c;->d:Lcom/everyplay/Everyplay/encoding/f;

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->d(Lcom/everyplay/Everyplay/encoding/b;)Lcom/everyplay/Everyplay/communication/ay;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->d(Lcom/everyplay/Everyplay/encoding/b;)Lcom/everyplay/Everyplay/communication/ay;

    move-result-object v0

    iget-object v1, p0, Lcom/everyplay/Everyplay/encoding/c;->e:Lcom/everyplay/Everyplay/encoding/b;

    invoke-interface {v0, v1}, Lcom/everyplay/Everyplay/communication/ay;->a(Lcom/everyplay/Everyplay/encoding/b;)V

    :cond_2
    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->n(Lcom/everyplay/Everyplay/encoding/b;)Ljava/lang/Thread;

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->o(Lcom/everyplay/Everyplay/encoding/b;)Lcom/everyplay/Everyplay/encoding/c;

    return-void

    :cond_3
    :try_start_1
    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v6}, Lcom/everyplay/Everyplay/encoding/b;->g(Lcom/everyplay/Everyplay/encoding/b;)Z

    move-result v6

    if-ne v6, v10, :cond_4

    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v6}, Lcom/everyplay/Everyplay/encoding/b;->k(Lcom/everyplay/Everyplay/encoding/b;)V

    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v6}, Lcom/everyplay/Everyplay/encoding/b;->l(Lcom/everyplay/Everyplay/encoding/b;)V

    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v6}, Lcom/everyplay/Everyplay/encoding/b;->b(Lcom/everyplay/Everyplay/encoding/b;)V

    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v6}, Lcom/everyplay/Everyplay/encoding/b;->c(Lcom/everyplay/Everyplay/encoding/b;)V

    iget-object v6, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/everyplay/Everyplay/encoding/b;->a(Lcom/everyplay/Everyplay/encoding/b;Z)Z

    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gez v0, :cond_6

    if-gez v2, :cond_6

    const-wide/16 v2, 0xf

    :goto_4
    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    sget-object v0, Lcom/everyplay/Everyplay/encoding/d;->c:Lcom/everyplay/Everyplay/encoding/d;

    iget-object v2, p0, Lcom/everyplay/Everyplay/encoding/c;->b:Lcom/everyplay/Everyplay/encoding/d;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->f(Lcom/everyplay/Everyplay/encoding/b;)Z

    move-result v0

    if-ne v0, v10, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->a:Lcom/everyplay/Everyplay/encoding/b;

    invoke-static {v0}, Lcom/everyplay/Everyplay/encoding/b;->m(Lcom/everyplay/Everyplay/encoding/b;)Z

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->c:Lcom/everyplay/Everyplay/encoding/f;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->c:Lcom/everyplay/Everyplay/encoding/f;

    invoke-virtual {v0}, Lcom/everyplay/Everyplay/encoding/f;->c()Z

    :cond_5
    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->d:Lcom/everyplay/Everyplay/encoding/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/encoding/c;->d:Lcom/everyplay/Everyplay/encoding/f;

    invoke-virtual {v0}, Lcom/everyplay/Everyplay/encoding/f;->c()Z

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_6
    move-wide v2, v4

    goto :goto_4

    :cond_7
    move v2, v1

    goto/16 :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method
