.class final Lcom/everyplay/Everyplay/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/everyplay/Everyplay/communication/x;


# instance fields
.field final synthetic a:Lcom/everyplay/Everyplay/b;


# direct methods
.method constructor <init>(Lcom/everyplay/Everyplay/b;)V
    .locals 0

    iput-object p1, p0, Lcom/everyplay/Everyplay/c;->a:Lcom/everyplay/Everyplay/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lcom/everyplay/Everyplay/communication/t;->a()Z

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "recording-enabled"

    invoke-static {v0}, Lcom/everyplay/Everyplay/communication/t;->a(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/everyplay/Everyplay/c;->a:Lcom/everyplay/Everyplay/b;

    invoke-static {}, Lcom/everyplay/Everyplay/b;->v()V

    :cond_2
    :goto_1
    sget-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    sget-object v1, Lcom/everyplay/Everyplay/e/h;->b:Lcom/everyplay/Everyplay/e/h;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/everyplay/Everyplay/communication/g;->a(I)V

    goto :goto_0

    :cond_3
    const-string v0, "Device settings updated from server, recording support disabled."

    invoke-static {v0}, Lcom/everyplay/Everyplay/d/e;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    sget-object v1, Lcom/everyplay/Everyplay/e/h;->d:Lcom/everyplay/Everyplay/e/h;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/everyplay/Everyplay/c;->a:Lcom/everyplay/Everyplay/b;

    invoke-virtual {v0}, Lcom/everyplay/Everyplay/b;->o()V

    sget-object v0, Lcom/everyplay/Everyplay/e/h;->b:Lcom/everyplay/Everyplay/e/h;

    sput-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    :cond_4
    sget-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    sget-object v1, Lcom/everyplay/Everyplay/e/h;->c:Lcom/everyplay/Everyplay/e/h;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/everyplay/Everyplay/e/h;->b:Lcom/everyplay/Everyplay/e/h;

    sput-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lcom/everyplay/Everyplay/communication/t;->a()Z

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "recording-enabled"

    invoke-static {v0}, Lcom/everyplay/Everyplay/communication/t;->a(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/everyplay/Everyplay/c;->a:Lcom/everyplay/Everyplay/b;

    invoke-static {}, Lcom/everyplay/Everyplay/b;->v()V

    goto :goto_0

    :cond_1
    const-string v0, "Cached device settings disable recording support, checking current status from server"

    invoke-static {v0}, Lcom/everyplay/Everyplay/d/e;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    invoke-static {}, Lcom/everyplay/Everyplay/communication/t;->a()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    sget-object v1, Lcom/everyplay/Everyplay/e/h;->c:Lcom/everyplay/Everyplay/e/h;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/everyplay/Everyplay/e/h;->b:Lcom/everyplay/Everyplay/e/h;

    sput-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    :cond_2
    sget-object v0, Lcom/everyplay/Everyplay/e/f;->a:Lcom/everyplay/Everyplay/e/h;

    sget-object v1, Lcom/everyplay/Everyplay/e/h;->b:Lcom/everyplay/Everyplay/e/h;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/everyplay/Everyplay/communication/g;->a(I)V

    goto :goto_0
.end method
