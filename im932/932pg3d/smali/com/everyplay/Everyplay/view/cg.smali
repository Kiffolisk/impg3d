.class public final Lcom/everyplay/Everyplay/view/cg;
.super Lcom/everyplay/Everyplay/communication/au;


# instance fields
.field final synthetic b:Lcom/everyplay/Everyplay/view/EveryplayWebView;


# direct methods
.method public constructor <init>(Lcom/everyplay/Everyplay/view/EveryplayWebView;)V
    .locals 0

    iput-object p1, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-direct {p0}, Lcom/everyplay/Everyplay/communication/au;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/everyplay/Everyplay/communication/au;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1, p2}, Lcom/everyplay/Everyplay/communication/au;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Finished url: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->c(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Lcom/everyplay/Everyplay/view/cj;

    move-result-object v0

    sget-object v1, Lcom/everyplay/Everyplay/view/cj;->a:Lcom/everyplay/Everyplay/view/cj;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->d(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lcom/everyplay/Everyplay/e/f;->g()Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-gt v1, v2, :cond_1

    iget-object v1, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    sget-object v2, Lcom/everyplay/Everyplay/communication/EveryplayWebViewInputExtensions;->DATE_PICKER_JS_SNIPPET:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->a(Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lcom/everyplay/Everyplay/d/a;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "native_inputs"

    invoke-static {v1}, Lcom/everyplay/Everyplay/communication/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "on"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-virtual {v2}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/everyplay/Everyplay/R$string;->everyplay_done_text:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/everyplay/Everyplay/communication/EveryplayWebViewInputExtensions;->textInputJsSnippet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "window.settings = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->e(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Lcom/everyplay/Everyplay/communication/bb;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->f(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->f(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0, v4}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->b(Lcom/everyplay/Everyplay/view/EveryplayWebView;Z)Z

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->e(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Lcom/everyplay/Everyplay/communication/bb;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/everyplay/Everyplay/communication/bb;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->a(Lcom/everyplay/Everyplay/view/EveryplayWebView;Ljava/lang/String;)Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->d(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0, v4}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->a(Lcom/everyplay/Everyplay/view/EveryplayWebView;Z)Z

    :cond_5
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/everyplay/Everyplay/communication/au;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->a(Lcom/everyplay/Everyplay/view/EveryplayWebView;Z)Z

    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/everyplay/Everyplay/communication/au;->a:Lcom/everyplay/Everyplay/communication/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0, p4}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->a(Lcom/everyplay/Everyplay/view/EveryplayWebView;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/everyplay/Everyplay/communication/au;->a:Lcom/everyplay/Everyplay/communication/bb;

    invoke-interface {v0, p4}, Lcom/everyplay/Everyplay/communication/bb;->b(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->g(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->b(Lcom/everyplay/Everyplay/view/EveryplayWebView;Z)Z

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/everyplay/Everyplay/communication/au;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 2

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->c(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Lcom/everyplay/Everyplay/view/cj;

    move-result-object v0

    sget-object v1, Lcom/everyplay/Everyplay/view/cj;->c:Lcom/everyplay/Everyplay/view/cj;

    if-eq v0, v1, :cond_0

    invoke-static {p2}, Lcom/everyplay/Everyplay/communication/c/f;->a(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->c(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Lcom/everyplay/Everyplay/view/cj;

    move-result-object v0

    sget-object v2, Lcom/everyplay/Everyplay/view/cj;->a:Lcom/everyplay/Everyplay/view/cj;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->c(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Lcom/everyplay/Everyplay/view/cj;

    move-result-object v0

    sget-object v2, Lcom/everyplay/Everyplay/view/cj;->b:Lcom/everyplay/Everyplay/view/cj;

    if-ne v0, v2, :cond_1

    :cond_0
    :try_start_0
    new-instance v2, Ljava/net/URL;

    const-string v0, "kEveryplayBaseWebUrlKey"

    invoke-static {v0}, Lcom/everyplay/Everyplay/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    :goto_0
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Opening Everyplay browser for url: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    sget-object v0, Lcom/everyplay/Everyplay/view/q;->a:Lcom/everyplay/Everyplay/view/q;

    new-instance v1, Lcom/everyplay/Everyplay/view/ch;

    invoke-direct {v1, p0}, Lcom/everyplay/Everyplay/view/ch;-><init>(Lcom/everyplay/Everyplay/view/cg;)V

    invoke-static {p2, v0, v4, v3, v1}, Lcom/everyplay/Everyplay/view/m;->a(Ljava/lang/String;Lcom/everyplay/Everyplay/view/q;ZZLcom/everyplay/Everyplay/communication/ax;)V

    move v0, v4

    :goto_1
    return v0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    const-string v2, "Couldn\'t create reference url"

    invoke-static {v2}, Lcom/everyplay/Everyplay/d/e;->a(Ljava/lang/String;)V

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/cg;->b:Lcom/everyplay/Everyplay/view/EveryplayWebView;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/EveryplayWebView;->c(Lcom/everyplay/Everyplay/view/EveryplayWebView;)Lcom/everyplay/Everyplay/view/cj;

    move-result-object v0

    sget-object v1, Lcom/everyplay/Everyplay/view/cj;->c:Lcom/everyplay/Everyplay/view/cj;

    if-ne v0, v1, :cond_2

    move v0, v3

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_2
.end method
