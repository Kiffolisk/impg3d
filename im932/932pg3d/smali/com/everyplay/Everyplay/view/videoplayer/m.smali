.class public final Lcom/everyplay/Everyplay/view/videoplayer/m;
.super Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;


# instance fields
.field private m:Lcom/everyplay/Everyplay/c/a/c;

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/everyplay/Everyplay/c/a/c;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->m:Lcom/everyplay/Everyplay/c/a/c;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->n:Z

    const/16 v0, 0x14

    iput v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->l:I

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-direct {v0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Lcom/everyplay/Everyplay/view/videoplayer/p;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/a;

    invoke-direct {v0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Lcom/everyplay/Everyplay/view/videoplayer/p;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/aq;

    invoke-direct {v0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/aq;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Lcom/everyplay/Everyplay/view/videoplayer/p;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    move-result-object v0

    sget-object v1, Lcom/everyplay/Everyplay/view/videoplayer/g;->a:Lcom/everyplay/Everyplay/view/videoplayer/g;

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/view/videoplayer/p;->a(Lcom/everyplay/Everyplay/view/videoplayer/g;)V

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/d;

    invoke-direct {v0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Lcom/everyplay/Everyplay/view/videoplayer/p;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/ah;

    invoke-direct {v0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/ah;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Lcom/everyplay/Everyplay/view/videoplayer/p;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/n;

    invoke-direct {v0, p0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/n;-><init>(Lcom/everyplay/Everyplay/view/videoplayer/m;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Lcom/everyplay/Everyplay/view/videoplayer/p;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/f;

    invoke-direct {v0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Lcom/everyplay/Everyplay/view/videoplayer/p;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->m:Lcom/everyplay/Everyplay/c/a/c;

    sget-object v1, Lcom/everyplay/Everyplay/c/a/g;->a:Lcom/everyplay/Everyplay/c/a/g;

    new-instance v2, Lcom/everyplay/Everyplay/view/videoplayer/o;

    invoke-direct {v2, p0}, Lcom/everyplay/Everyplay/view/videoplayer/o;-><init>(Lcom/everyplay/Everyplay/view/videoplayer/m;)V

    invoke-virtual {v0, v1, v2}, Lcom/everyplay/Everyplay/c/a/c;->a(Lcom/everyplay/Everyplay/c/a/g;Lcom/everyplay/Everyplay/c/a/m;)V

    return-void
.end method


# virtual methods
.method public final getSession()Lcom/everyplay/Everyplay/c/a/c;
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->m:Lcom/everyplay/Everyplay/c/a/c;

    return-object v0
.end method

.method public final h()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->m:Lcom/everyplay/Everyplay/c/a/c;

    sget-object v1, Lcom/everyplay/Everyplay/c/a/g;->a:Lcom/everyplay/Everyplay/c/a/g;

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/c/a/c;->b(Lcom/everyplay/Everyplay/c/a/g;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->m:Lcom/everyplay/Everyplay/c/a/c;

    sget-object v1, Lcom/everyplay/Everyplay/c/a/g;->a:Lcom/everyplay/Everyplay/c/a/g;

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/c/a/c;->b(Lcom/everyplay/Everyplay/c/a/g;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-boolean v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->n:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Ljava/lang/String;ZI)Z

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->m:Lcom/everyplay/Everyplay/c/a/c;

    sget-object v1, Lcom/everyplay/Everyplay/c/a/g;->c:Lcom/everyplay/Everyplay/c/a/g;

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/c/a/c;->b(Lcom/everyplay/Everyplay/c/a/g;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->m:Lcom/everyplay/Everyplay/c/a/c;

    sget-object v1, Lcom/everyplay/Everyplay/c/a/g;->c:Lcom/everyplay/Everyplay/c/a/g;

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/c/a/c;->b(Lcom/everyplay/Everyplay/c/a/g;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-boolean v3, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->n:Z

    invoke-virtual {p0, v0, v3, v2}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->a(Ljava/lang/String;ZI)Z

    :cond_0
    return-void
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/g;->j:Lcom/everyplay/Everyplay/view/videoplayer/g;

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/m;->setState(Lcom/everyplay/Everyplay/view/videoplayer/g;)V

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/m;->b()V

    return-void
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/m;->n:Z

    return v0
.end method
