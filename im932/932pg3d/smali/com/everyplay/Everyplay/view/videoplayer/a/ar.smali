.class public final Lcom/everyplay/Everyplay/view/videoplayer/a/ar;
.super Lcom/everyplay/Everyplay/view/videoplayer/p;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/everyplay/Everyplay/view/videoplayer/ac;


# instance fields
.field private c:Landroid/widget/RelativeLayout;

.field private h:Landroid/media/MediaPlayer;

.field private i:Lcom/everyplay/Everyplay/c/w;

.field private j:Lcom/everyplay/Everyplay/view/EveryplayImageButton;

.field private k:Landroid/view/View;

.field private l:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

.field private m:Z

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/p;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/av;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    iput-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->l:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->m:Z

    iput v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->n:F

    iput v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->o:F

    iput v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->p:F

    iput v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->q:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->r:Z

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/g;->j:Lcom/everyplay/Everyplay/view/videoplayer/g;

    invoke-virtual {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->b(Lcom/everyplay/Everyplay/view/videoplayer/g;)V

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    return-void
.end method

.method static synthetic a(Lcom/everyplay/Everyplay/view/videoplayer/a/ar;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic a(Lcom/everyplay/Everyplay/view/videoplayer/a/ar;Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V

    return-void
.end method

.method private a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/au;->a:[I

    invoke-virtual {p1}, Lcom/everyplay/Everyplay/view/videoplayer/a/av;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    iput-object p1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->l:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setY(F)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setX(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setY(F)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setX(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/everyplay/Everyplay/view/videoplayer/a/ar;)Lcom/everyplay/Everyplay/view/videoplayer/a/av;
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->l:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/everyplay/Everyplay/c/w;)V
    .locals 3

    iput-object p1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->i:Lcom/everyplay/Everyplay/c/w;

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->i:Lcom/everyplay/Everyplay/c/w;

    iget-object v1, v0, Lcom/everyplay/Everyplay/c/w;->n:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/everyplay/Everyplay/c/w;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->b()V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    :try_start_0
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->i:Lcom/everyplay/Everyplay/c/w;

    iget-object v2, v2, Lcom/everyplay/Everyplay/c/w;->n:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/everyplay/Everyplay/view/videoplayer/a/as;

    invoke-direct {v1, p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/as;-><init>(Lcom/everyplay/Everyplay/view/videoplayer/a/ar;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    sget v1, Lcom/everyplay/Everyplay/R$layout;->everyplay_video_microphone:I

    invoke-virtual {p0, v1}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    sget v1, Lcom/everyplay/Everyplay/R$id;->everyplayMicrophoneAvatar:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/everyplay/Everyplay/view/EveryplayImageButton;

    iput-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->j:Lcom/everyplay/Everyplay/view/EveryplayImageButton;

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    sget v1, Lcom/everyplay/Everyplay/R$id;->everyplayShowMicrophoneAvatar:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->j:Lcom/everyplay/Everyplay/view/EveryplayImageButton;

    invoke-virtual {v0, p0}, Lcom/everyplay/Everyplay/view/EveryplayImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->j:Lcom/everyplay/Everyplay/view/EveryplayImageButton;

    invoke-virtual {v0, p0}, Lcom/everyplay/Everyplay/view/EveryplayImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->j:Lcom/everyplay/Everyplay/view/EveryplayImageButton;

    iget-object v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->i:Lcom/everyplay/Everyplay/c/w;

    iget-object v1, v1, Lcom/everyplay/Everyplay/c/w;->p:Lcom/everyplay/Everyplay/c/v;

    iget-object v1, v1, Lcom/everyplay/Everyplay/c/v;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/view/EveryplayImageButton;->setNormalStateUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->l:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    invoke-direct {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V

    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a()V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    goto :goto_2
.end method

.method public final a(Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;II)V
    .locals 2

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x12c

    if-le v0, v1, :cond_0

    const-string v0, "Out of sync!"

    invoke-static {v0}, Lcom/everyplay/Everyplay/d/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;Lcom/everyplay/Everyplay/view/videoplayer/g;)V
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->m:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/g;->g:Lcom/everyplay/Everyplay/view/videoplayer/g;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/everyplay/Everyplay/view/videoplayer/p;->a(Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;Lcom/everyplay/Everyplay/view/videoplayer/g;)V

    return-void

    :cond_1
    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/g;->f:Lcom/everyplay/Everyplay/view/videoplayer/g;

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/g;->i:Lcom/everyplay/Everyplay/view/videoplayer/g;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;)V
    .locals 3

    const/16 v1, 0xfa

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const-string v1, "editorbuttons"

    invoke-virtual {p1, v1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->b(Ljava/lang/String;)Lcom/everyplay/Everyplay/view/videoplayer/p;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/everyplay/Everyplay/view/videoplayer/p;->c()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    invoke-virtual {v1}, Lcom/everyplay/Everyplay/view/videoplayer/p;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :cond_0
    iget-object v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    const-string v0, "microphone"

    return-object v0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->l:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    invoke-direct {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/16 v4, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->r:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->m:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_2
    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->j:Lcom/everyplay/Everyplay/view/EveryplayImageButton;

    invoke-virtual {v2, v1}, Lcom/everyplay/Everyplay/view/EveryplayImageButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->k:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->requestLayout()V

    iget-object v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->invalidate()V

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->m:Z

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/everyplay/Everyplay/view/videoplayer/a/at;

    invoke-direct {v1, p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/at;-><init>(Lcom/everyplay/Everyplay/view/videoplayer/a/ar;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RelativeLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->pause()V

    :cond_5
    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->j:Lcom/everyplay/Everyplay/view/EveryplayImageButton;

    invoke-virtual {v2, v4}, Lcom/everyplay/Everyplay/view/EveryplayImageButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->k:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->h:Landroid/media/MediaPlayer;

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getX()F

    move-result v2

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->n:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->o:F

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getX()F

    move-result v0

    iput v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->p:F

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getY()F

    move-result v0

    iput v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->q:F

    iput-boolean v1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->r:Z

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->o:F

    sub-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setY(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget v3, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->n:F

    sub-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setX(F)V

    goto :goto_1

    :pswitch_2
    iget v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->p:F

    iget-object v3, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->q:F

    iget-object v4, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-double v4, v2

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    float-to-double v2, v3

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iget-object v3, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    const/high16 v4, 0x43200000    # 160.0f

    div-float/2addr v3, v4

    div-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    iput-boolean v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->r:Z

    :cond_1
    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->f()Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    cmpg-float v0, v0, v3

    if-gez v0, :cond_3

    cmpg-float v0, v2, v4

    if-gez v0, :cond_2

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/av;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    invoke-direct {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V

    goto/16 :goto_1

    :cond_2
    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/av;->c:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    invoke-direct {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V

    goto/16 :goto_1

    :cond_3
    cmpg-float v0, v2, v4

    if-gez v0, :cond_4

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/av;->b:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    invoke-direct {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V

    goto/16 :goto_1

    :cond_4
    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/av;->d:Lcom/everyplay/Everyplay/view/videoplayer/a/av;

    invoke-direct {p0, v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/ar;->a(Lcom/everyplay/Everyplay/view/videoplayer/a/av;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
