.class final enum Lcom/everyplay/Everyplay/view/videoplayer/a/al;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

.field public static final enum b:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

.field public static final enum c:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

.field public static final enum d:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

.field private static final synthetic e:[Lcom/everyplay/Everyplay/view/videoplayer/a/al;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    const-string v1, "TOP_LEFT"

    invoke-direct {v0, v1, v2}, Lcom/everyplay/Everyplay/view/videoplayer/a/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v3}, Lcom/everyplay/Everyplay/view/videoplayer/a/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->b:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    const-string v1, "BOTTOM_LEFT"

    invoke-direct {v0, v1, v4}, Lcom/everyplay/Everyplay/view/videoplayer/a/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->c:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    new-instance v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    const-string v1, "BOTTOM_RIGHT"

    invoke-direct {v0, v1, v5}, Lcom/everyplay/Everyplay/view/videoplayer/a/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->d:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    sget-object v1, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    aput-object v1, v0, v2

    sget-object v1, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->b:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    aput-object v1, v0, v3

    sget-object v1, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->c:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    aput-object v1, v0, v4

    sget-object v1, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->d:Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    aput-object v1, v0, v5

    sput-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->e:[Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/everyplay/Everyplay/view/videoplayer/a/al;
    .locals 1

    const-class v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    return-object v0
.end method

.method public static values()[Lcom/everyplay/Everyplay/view/videoplayer/a/al;
    .locals 1

    sget-object v0, Lcom/everyplay/Everyplay/view/videoplayer/a/al;->e:[Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    invoke-virtual {v0}, [Lcom/everyplay/Everyplay/view/videoplayer/a/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/everyplay/Everyplay/view/videoplayer/a/al;

    return-object v0
.end method
