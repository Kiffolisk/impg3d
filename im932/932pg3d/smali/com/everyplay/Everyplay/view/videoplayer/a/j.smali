.class final Lcom/everyplay/Everyplay/view/videoplayer/a/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/everyplay/Everyplay/c/a/n;


# instance fields
.field final synthetic a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;


# direct methods
.method constructor <init>(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)V
    .locals 0

    iput-object p1, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->e(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->e(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->e(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    return-void
.end method

.method public final a(Lcom/everyplay/Everyplay/c/a/g;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Trimming finished successfully: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->d(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Lcom/everyplay/Everyplay/view/videoplayer/EveryplayRangeSlider;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayRangeSlider;->setMinValue(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->d(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Lcom/everyplay/Everyplay/view/videoplayer/EveryplayRangeSlider;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayRangeSlider;->setMaxValue(F)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->d(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Lcom/everyplay/Everyplay/view/videoplayer/EveryplayRangeSlider;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/everyplay/Everyplay/view/videoplayer/EveryplayRangeSlider;->setShowMinMaxButtons(Z)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->e(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->e(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->f(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->g(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v0

    check-cast v0, Lcom/everyplay/Everyplay/view/videoplayer/m;

    invoke-virtual {v0}, Lcom/everyplay/Everyplay/view/videoplayer/m;->i()V

    iget-object v0, p0, Lcom/everyplay/Everyplay/view/videoplayer/a/j;->a:Lcom/everyplay/Everyplay/view/videoplayer/a/g;

    invoke-static {v0}, Lcom/everyplay/Everyplay/view/videoplayer/a/g;->h(Lcom/everyplay/Everyplay/view/videoplayer/a/g;)Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;

    move-result-object v0

    const-string v1, "enterTrimmedState"

    invoke-static {v0, v1}, Lcom/everyplay/Everyplay/communication/y;->a(Lcom/everyplay/Everyplay/view/videoplayer/EveryplayGenericVideoPlayerView;Ljava/lang/String;)V

    return-void
.end method
