.class public final enum Lcom/everyplay/Everyplay/view/ac;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/everyplay/Everyplay/view/ac;

.field public static final enum b:Lcom/everyplay/Everyplay/view/ac;

.field private static final synthetic c:[Lcom/everyplay/Everyplay/view/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/everyplay/Everyplay/view/ac;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/everyplay/Everyplay/view/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/everyplay/Everyplay/view/ac;->a:Lcom/everyplay/Everyplay/view/ac;

    new-instance v0, Lcom/everyplay/Everyplay/view/ac;

    const-string v1, "EXPANDED"

    invoke-direct {v0, v1, v3}, Lcom/everyplay/Everyplay/view/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/everyplay/Everyplay/view/ac;->b:Lcom/everyplay/Everyplay/view/ac;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/everyplay/Everyplay/view/ac;

    sget-object v1, Lcom/everyplay/Everyplay/view/ac;->a:Lcom/everyplay/Everyplay/view/ac;

    aput-object v1, v0, v2

    sget-object v1, Lcom/everyplay/Everyplay/view/ac;->b:Lcom/everyplay/Everyplay/view/ac;

    aput-object v1, v0, v3

    sput-object v0, Lcom/everyplay/Everyplay/view/ac;->c:[Lcom/everyplay/Everyplay/view/ac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/everyplay/Everyplay/view/ac;
    .locals 1

    const-class v0, Lcom/everyplay/Everyplay/view/ac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/everyplay/Everyplay/view/ac;

    return-object v0
.end method

.method public static values()[Lcom/everyplay/Everyplay/view/ac;
    .locals 1

    sget-object v0, Lcom/everyplay/Everyplay/view/ac;->c:[Lcom/everyplay/Everyplay/view/ac;

    invoke-virtual {v0}, [Lcom/everyplay/Everyplay/view/ac;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/everyplay/Everyplay/view/ac;

    return-object v0
.end method
