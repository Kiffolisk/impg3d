.class public Lcom/aunityplugin/MainActivity;
.super Lcom/unity3d/player/UnityPlayerActivity;
.source "MainActivity.java"


# static fields
.field public static eventID:I

.field public static eventIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public am:Landroid/app/AlarmManager;

.field public mContext:Landroid/content/Context;

.field pendingAlarm:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    sput v0, Lcom/aunityplugin/MainActivity;->eventID:I

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/aunityplugin/MainActivity;->eventIDs:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/unity3d/player/UnityPlayerActivity;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/aunityplugin/MainActivity;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public cancelAllSchedules()V
    .locals 5

    .prologue
    .line 91
    sget-object v2, Lcom/aunityplugin/MainActivity;->eventIDs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 99
    sget-object v2, Lcom/aunityplugin/MainActivity;->eventIDs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 100
    return-void

    .line 91
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 92
    .local v1, "id":Ljava/lang/Integer;
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/aunityplugin/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    iput-object v2, p0, Lcom/aunityplugin/MainActivity;->am:Landroid/app/AlarmManager;

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/aunityplugin/NotificationReciever;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v0, "alarmIntent":Landroid/content/Intent;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 95
    const/high16 v4, 0x8000000

    .line 94
    invoke-static {p0, v2, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/aunityplugin/MainActivity;->pendingAlarm:Landroid/app/PendingIntent;

    .line 96
    iget-object v2, p0, Lcom/aunityplugin/MainActivity;->am:Landroid/app/AlarmManager;

    iget-object v4, p0, Lcom/aunityplugin/MainActivity;->pendingAlarm:Landroid/app/PendingIntent;

    invoke-virtual {v2, v4}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 97
    iget-object v2, p0, Lcom/aunityplugin/MainActivity;->pendingAlarm:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->cancel()V

    goto :goto_0
.end method

.method public getExternalDirectory(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 104
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/unity3d/player/UnityPlayerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    iput-object p0, p0, Lcom/aunityplugin/MainActivity;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/aunityplugin/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/unity3d/player/R$menu;->main:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 44
    const/4 v0, 0x1

    return v0
.end method

.method public scanExternalDirectory(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    .line 109
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    .line 110
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 108
    invoke-virtual {p0, v0}, Lcom/aunityplugin/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 111
    return-void
.end method

.method public scheduleNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "appToLaunch"    # Ljava/lang/String;
    .param p2, "txMessageTop"    # Ljava/lang/String;
    .param p3, "txMessageTitle"    # Ljava/lang/String;
    .param p4, "txMessageBody"    # Ljava/lang/String;
    .param p5, "minutes"    # Ljava/lang/String;

    .prologue
    .line 58
    const-string v4, "00"

    const-string v5, "YABIM SCHEDULE"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 60
    .local v1, "cal":Ljava/util/Calendar;
    const/16 v4, 0xd

    invoke-static {p5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 61
    const-string v4, "00"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "YABIM SCHEDULE 2"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/aunityplugin/NotificationReciever;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .local v0, "alarmIntent":Landroid/content/Intent;
    const-string v4, "top"

    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v4, "title"

    invoke-virtual {v0, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v4, "body"

    invoke-virtual {v0, v4, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string v4, "app"

    invoke-virtual {v0, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0}, Lcom/aunityplugin/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "icon_sb"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 69
    .local v3, "sresourceID":I
    invoke-virtual {p0}, Lcom/aunityplugin/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "icon_lb"

    const-string v6, "drawable"

    invoke-virtual {v4, v5, v6, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 70
    .local v2, "lresourceID":I
    const-string v4, "sicon"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    const-string v4, "licon"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 76
    sget v4, Lcom/aunityplugin/MainActivity;->eventID:I

    .line 77
    const/high16 v5, 0x8000000

    .line 76
    invoke-static {p0, v4, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    iput-object v4, p0, Lcom/aunityplugin/MainActivity;->pendingAlarm:Landroid/app/PendingIntent;

    .line 79
    sget-object v4, Lcom/aunityplugin/MainActivity;->eventIDs:Ljava/util/ArrayList;

    sget v5, Lcom/aunityplugin/MainActivity;->eventID:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    sget v4, Lcom/aunityplugin/MainActivity;->eventID:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/aunityplugin/MainActivity;->eventID:I

    .line 85
    const-string v4, "alarm"

    invoke-virtual {p0, v4}, Lcom/aunityplugin/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/AlarmManager;

    iput-object v4, p0, Lcom/aunityplugin/MainActivity;->am:Landroid/app/AlarmManager;

    .line 86
    iget-object v4, p0, Lcom/aunityplugin/MainActivity;->am:Landroid/app/AlarmManager;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcom/aunityplugin/MainActivity;->pendingAlarm:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 87
    const-string v4, "00"

    const-string v5, "Scheduled"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    return-void
.end method

.method public startWebView(Ljava/lang/String;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/aunityplugin/MainActivity;->mContext:Landroid/content/Context;

    const-class v2, Lcom/aunityplugin/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string v1, "url"

    const-string v2, "none"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    :goto_0
    invoke-virtual {p0, v0}, Lcom/aunityplugin/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 55
    return-void

    .line 52
    :cond_0
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
