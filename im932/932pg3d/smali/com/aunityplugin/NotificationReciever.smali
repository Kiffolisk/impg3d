.class public Lcom/aunityplugin/NotificationReciever;
.super Landroid/content/BroadcastReceiver;
.source "NotificationReciever.java"


# instance fields
.field public receiverContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 25
    .param p1, "appToLaunch"    # Ljava/lang/String;
    .param p2, "textTop"    # Ljava/lang/String;
    .param p3, "textTitle"    # Ljava/lang/String;
    .param p4, "textBody"    # Ljava/lang/String;
    .param p5, "sIcon"    # I
    .param p6, "lIcon"    # I

    .prologue
    .line 49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "notification"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/NotificationManager;

    .line 52
    .local v11, "mNotificationManager":Landroid/app/NotificationManager;
    const/16 v22, 0x2

    :try_start_0
    invoke-static/range {v22 .. v22}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v14

    .line 53
    .local v14, "notification":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v0, v14}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v18

    .line 54
    .local v18, "r":Landroid/media/Ringtone;
    invoke-virtual/range {v18 .. v18}, Landroid/media/Ringtone;->play()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .end local v14    # "notification":Landroid/net/Uri;
    .end local v18    # "r":Landroid/media/Ringtone;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 59
    .local v17, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v22

    const/high16 v23, 0x24000000

    invoke-virtual/range {v22 .. v23}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v15

    .line 62
    .local v15, "notificationIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-static {v0, v1, v15, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 64
    .local v5, "contentIntent":Landroid/app/PendingIntent;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 66
    .local v9, "currentApiVersion":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "notification"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/app/NotificationManager;

    .line 68
    .local v13, "nm":Landroid/app/NotificationManager;
    const/16 v22, 0xb

    move/from16 v0, v22

    if-le v9, v0, :cond_1

    .line 69
    new-instance v22, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 70
    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v22

    .line 71
    move-object/from16 v0, v22

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v22

    .line 72
    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v22

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p6

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v22

    .line 74
    move-object/from16 v0, v22

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v22

    .line 75
    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v22

    .line 76
    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v12

    .line 77
    .local v12, "nb":Landroid/app/Notification$Builder;
    const/16 v22, 0x10

    move/from16 v0, v22

    if-ge v9, v0, :cond_0

    .line 79
    const/16 v22, 0x64

    invoke-virtual {v12}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 104
    .end local v12    # "nb":Landroid/app/Notification$Builder;
    :goto_1
    return-void

    .line 81
    .restart local v12    # "nb":Landroid/app/Notification$Builder;
    :cond_0
    const/16 v22, 0x64

    invoke-virtual {v12}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_1

    .line 84
    .end local v12    # "nb":Landroid/app/Notification$Builder;
    :cond_1
    const-string v16, "notification"

    .line 85
    .local v16, "ns":Ljava/lang/String;
    move/from16 v10, p5

    .line 86
    .local v10, "icon":I
    move-object/from16 v19, p2

    .line 87
    .local v19, "tickerText":Ljava/lang/CharSequence;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 89
    .local v20, "when":J
    new-instance v14, Landroid/app/Notification;

    move-object/from16 v0, v19

    move-wide/from16 v1, v20

    invoke-direct {v14, v10, v0, v1, v2}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 91
    .local v14, "notification":Landroid/app/Notification;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    .line 92
    .local v8, "context":Landroid/content/Context;
    move-object/from16 v7, p3

    .line 93
    .local v7, "contentTitle":Ljava/lang/CharSequence;
    move-object/from16 v6, p4

    .line 95
    .local v6, "contentText":Ljava/lang/CharSequence;
    invoke-virtual {v14, v8, v7, v6, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 96
    const/4 v4, 0x1

    .line 97
    .local v4, "HELLO_ID":I
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0, v14}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_1

    .line 55
    .end local v4    # "HELLO_ID":I
    .end local v5    # "contentIntent":Landroid/app/PendingIntent;
    .end local v6    # "contentText":Ljava/lang/CharSequence;
    .end local v7    # "contentTitle":Ljava/lang/CharSequence;
    .end local v8    # "context":Landroid/content/Context;
    .end local v9    # "currentApiVersion":I
    .end local v10    # "icon":I
    .end local v13    # "nm":Landroid/app/NotificationManager;
    .end local v14    # "notification":Landroid/app/Notification;
    .end local v15    # "notificationIntent":Landroid/content/Intent;
    .end local v16    # "ns":Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v19    # "tickerText":Ljava/lang/CharSequence;
    .end local v20    # "when":J
    :catch_0
    move-exception v22

    goto/16 :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    .line 32
    :try_start_0
    iput-object p1, p0, Lcom/aunityplugin/NotificationReciever;->receiverContext:Landroid/content/Context;

    .line 33
    const-string v0, "top"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 34
    .local v2, "textTop":Ljava/lang/String;
    const-string v0, "title"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 35
    .local v3, "textTitle":Ljava/lang/String;
    const-string v0, "body"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 36
    .local v4, "textBody":Ljava/lang/String;
    const-string v0, "sicon"

    const/4 v8, 0x0

    invoke-virtual {p2, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 37
    .local v5, "sicon":I
    const-string v0, "licon"

    const/4 v8, 0x0

    invoke-virtual {p2, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 38
    .local v6, "licon":I
    const-string v0, "app"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, "appToLaunch":Ljava/lang/String;
    move-object v0, p0

    .line 39
    invoke-direct/range {v0 .. v6}, Lcom/aunityplugin/NotificationReciever;->createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 40
    const-string v0, "00"

    const-string v8, "Recieved"

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .end local v1    # "appToLaunch":Ljava/lang/String;
    .end local v2    # "textTop":Ljava/lang/String;
    .end local v3    # "textTitle":Ljava/lang/String;
    .end local v4    # "textBody":Ljava/lang/String;
    .end local v5    # "sicon":I
    .end local v6    # "licon":I
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v7

    .line 43
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "ERROR"

    invoke-static {p1, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 44
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
