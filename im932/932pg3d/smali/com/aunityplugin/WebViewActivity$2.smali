.class Lcom/aunityplugin/WebViewActivity$2;
.super Landroid/webkit/WebViewClient;
.source "WebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/aunityplugin/WebViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/aunityplugin/WebViewActivity;


# direct methods
.method constructor <init>(Lcom/aunityplugin/WebViewActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/aunityplugin/WebViewActivity$2;->this$0:Lcom/aunityplugin/WebViewActivity;

    .line 143
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 146
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 147
    iget-object v2, p0, Lcom/aunityplugin/WebViewActivity$2;->this$0:Lcom/aunityplugin/WebViewActivity;

    invoke-static {v2}, Lcom/aunityplugin/WebViewActivity;->access$1(Lcom/aunityplugin/WebViewActivity;)Landroid/webkit/WebView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 148
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 149
    .local v0, "allClear":Ljava/lang/Boolean;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/aunityplugin/WebViewActivity$2;->this$0:Lcom/aunityplugin/WebViewActivity;

    invoke-static {v2}, Lcom/aunityplugin/WebViewActivity;->access$2(Lcom/aunityplugin/WebViewActivity;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 154
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 155
    iget-object v2, p0, Lcom/aunityplugin/WebViewActivity$2;->this$0:Lcom/aunityplugin/WebViewActivity;

    invoke-static {v2}, Lcom/aunityplugin/WebViewActivity;->access$1(Lcom/aunityplugin/WebViewActivity;)Landroid/webkit/WebView;

    move-result-object v2

    iget-object v3, p0, Lcom/aunityplugin/WebViewActivity$2;->this$0:Lcom/aunityplugin/WebViewActivity;

    invoke-static {v3}, Lcom/aunityplugin/WebViewActivity;->access$2(Lcom/aunityplugin/WebViewActivity;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 158
    .end local v0    # "allClear":Ljava/lang/Boolean;
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 150
    .restart local v0    # "allClear":Ljava/lang/Boolean;
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/aunityplugin/WebViewActivity$2;->this$0:Lcom/aunityplugin/WebViewActivity;

    invoke-static {v2}, Lcom/aunityplugin/WebViewActivity;->access$1(Lcom/aunityplugin/WebViewActivity;)Landroid/webkit/WebView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/aunityplugin/WebViewActivity$2;->this$0:Lcom/aunityplugin/WebViewActivity;

    invoke-static {v3}, Lcom/aunityplugin/WebViewActivity;->access$2(Lcom/aunityplugin/WebViewActivity;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 151
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 149
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
