.class public Lcom/aunityplugin/WebViewActivity;
.super Landroid/app/Activity;
.source "WebViewActivity.java"


# static fields
.field private static final FILECHOOSER_RESULTCODE:I = 0x1


# instance fields
.field alertDialogBuilder:Landroid/app/AlertDialog$Builder;

.field mContext:Landroid/content/Context;

.field private mUploadMessage:Landroid/webkit/ValueCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private permittedURLs:[Ljava/lang/String;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/aunityplugin/WebViewActivity;Landroid/webkit/ValueCallback;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/aunityplugin/WebViewActivity;->mUploadMessage:Landroid/webkit/ValueCallback;

    return-void
.end method

.method static synthetic access$1(Lcom/aunityplugin/WebViewActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/aunityplugin/WebViewActivity;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/aunityplugin/WebViewActivity;->permittedURLs:[Ljava/lang/String;

    return-object v0
.end method

.method public static launchWebViewActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/aunityplugin/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    const-string v1, "url"

    const-string v2, "none"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 49
    return-void

    .line 46
    :cond_0
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 54
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 55
    iget-object v2, p0, Lcom/aunityplugin/WebViewActivity;->mUploadMessage:Landroid/webkit/ValueCallback;

    if-nez v2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    if-eqz p3, :cond_2

    const/4 v2, -0x1

    if-eq p2, v2, :cond_3

    :cond_2
    move-object v0, v1

    .line 59
    .local v0, "result":Landroid/net/Uri;
    :goto_1
    iget-object v2, p0, Lcom/aunityplugin/WebViewActivity;->mUploadMessage:Landroid/webkit/ValueCallback;

    invoke-interface {v2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 60
    iput-object v1, p0, Lcom/aunityplugin/WebViewActivity;->mUploadMessage:Landroid/webkit/ValueCallback;

    goto :goto_0

    .line 58
    .end local v0    # "result":Landroid/net/Uri;
    :cond_3
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v7, -0x1

    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    iput-object p0, p0, Lcom/aunityplugin/WebViewActivity;->mContext:Landroid/content/Context;

    .line 72
    invoke-virtual {p0}, Lcom/aunityplugin/WebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 73
    .local v0, "intent":Landroid/content/Intent;
    const-string v6, "url"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, "url":Ljava/lang/String;
    const-string v6, "::"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/aunityplugin/WebViewActivity;->permittedURLs:[Ljava/lang/String;

    .line 76
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 77
    .local v1, "ll":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 80
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 85
    .local v2, "llLP":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    new-instance v6, Landroid/webkit/WebView;

    invoke-direct {v6, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    .line 88
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 91
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v6, v3}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    const-string v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 94
    iget-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    const-string v7, "https://minecraft.net/profile"

    invoke-virtual {v6, v7}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 99
    :goto_0
    iget-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 100
    new-instance v5, Lcom/aunityplugin/WebViewActivity$1;

    invoke-direct {v5, p0}, Lcom/aunityplugin/WebViewActivity$1;-><init>(Lcom/aunityplugin/WebViewActivity;)V

    .line 137
    .local v5, "wcl":Landroid/webkit/WebChromeClient;
    iget-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v6, v5}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 139
    iget-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 140
    invoke-virtual {p0, v1}, Lcom/aunityplugin/WebViewActivity;->setContentView(Landroid/view/View;)V

    .line 143
    iget-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    new-instance v7, Lcom/aunityplugin/WebViewActivity$2;

    invoke-direct {v7, p0}, Lcom/aunityplugin/WebViewActivity$2;-><init>(Lcom/aunityplugin/WebViewActivity;)V

    invoke-virtual {v6, v7}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 160
    return-void

    .line 96
    .end local v5    # "wcl":Landroid/webkit/WebChromeClient;
    :cond_0
    iget-object v6, p0, Lcom/aunityplugin/WebViewActivity;->webView:Landroid/webkit/WebView;

    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity;->permittedURLs:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 163
    const-string v1, "Info"

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 164
    .local v0, "item":Landroid/view/MenuItem;
    new-instance v1, Lcom/aunityplugin/WebViewActivity$3;

    invoke-direct {v1, p0}, Lcom/aunityplugin/WebViewActivity$3;-><init>(Lcom/aunityplugin/WebViewActivity;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 221
    const/4 v1, 0x1

    return v1
.end method
