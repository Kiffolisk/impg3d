.class Lcom/aunityplugin/WebViewActivity$3;
.super Ljava/lang/Object;
.source "WebViewActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/aunityplugin/WebViewActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/aunityplugin/WebViewActivity;


# direct methods
.method constructor <init>(Lcom/aunityplugin/WebViewActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/16 v13, 0x11

    const/4 v12, -0x1

    const/4 v11, 0x1

    const/16 v10, 0xa

    .line 167
    const-string v4, "To upload skin:\n\n - Log in Majong account (app doesn\'t have access to your data about username and password)\n\n- Click \"HOME\" button on the lower panel\n\n- Click on the \"Choose file\", then on \"Choose Existing\" and select the skin from folder \"Mine Run 3D\" in the gallery.\n\n- Click \"Upload\" button."

    .line 168
    .local v4, "message":Ljava/lang/String;
    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    new-instance v8, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v9, v9, Lcom/aunityplugin/WebViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v8, v7, Lcom/aunityplugin/WebViewActivity;->alertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 169
    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->alertDialogBuilder:Landroid/app/AlertDialog$Builder;

    const-string v8, "Info"

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 170
    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->alertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 172
    invoke-virtual {v7, v11}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    .line 173
    const-string v8, "OK"

    new-instance v9, Lcom/aunityplugin/WebViewActivity$3$1;

    invoke-direct {v9, p0}, Lcom/aunityplugin/WebViewActivity$3$1;-><init>(Lcom/aunityplugin/WebViewActivity$3;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 182
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 183
    .local v2, "llHead":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 185
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v12, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 190
    .local v3, "llLP":Landroid/widget/LinearLayout$LayoutParams;
    iput v13, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 191
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    new-instance v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 193
    .local v6, "tvTitle":Landroid/widget/TextView;
    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 194
    const-string v7, "Info"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 196
    const/high16 v7, 0x41a00000    # 20.0f

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextSize(F)V

    .line 197
    invoke-virtual {v6, v10, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 198
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 200
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v1, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 201
    .local v1, "llBody":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 203
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    new-instance v5, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->mContext:Landroid/content/Context;

    invoke-direct {v5, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 206
    .local v5, "msg":Landroid/widget/TextView;
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    invoke-virtual {v5, v10, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 208
    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 209
    const/high16 v7, 0x41900000    # 18.0f

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextSize(F)V

    .line 210
    new-instance v7, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v7}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 211
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 213
    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->alertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 214
    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->alertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 215
    iget-object v7, p0, Lcom/aunityplugin/WebViewActivity$3;->this$0:Lcom/aunityplugin/WebViewActivity;

    iget-object v7, v7, Lcom/aunityplugin/WebViewActivity;->alertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 217
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 218
    return v11
.end method
