.class public final Lcom/google/example/games/mainlibproj/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/example/games/mainlibproj/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final com_facebook_blue:I = 0x7f07000c

.field public static final com_facebook_loginview_text_color:I = 0x7f070010

.field public static final com_facebook_picker_search_bar_background:I = 0x7f07000a

.field public static final com_facebook_picker_search_bar_text:I = 0x7f07000b

.field public static final com_facebook_usersettingsfragment_connected_shadow_color:I = 0x7f07000e

.field public static final com_facebook_usersettingsfragment_connected_text_color:I = 0x7f07000d

.field public static final com_facebook_usersettingsfragment_not_connected_text_color:I = 0x7f07000f

.field public static final common_action_bar_splitter:I = 0x7f070009

.field public static final common_signin_btn_dark_text_default:I = 0x7f070000

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f070002

.field public static final common_signin_btn_dark_text_focused:I = 0x7f070003

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f070001

.field public static final common_signin_btn_default_background:I = 0x7f070008

.field public static final common_signin_btn_light_text_default:I = 0x7f070004

.field public static final common_signin_btn_light_text_disabled:I = 0x7f070006

.field public static final common_signin_btn_light_text_focused:I = 0x7f070007

.field public static final common_signin_btn_light_text_pressed:I = 0x7f070005

.field public static final common_signin_btn_text_dark:I = 0x7f07002c

.field public static final common_signin_btn_text_light:I = 0x7f07002d

.field public static final everyplay_blue:I = 0x7f070013

.field public static final everyplay_editor_button_background:I = 0x7f070011

.field public static final everyplay_editor_button_text:I = 0x7f070012

.field public static final everyplay_native_overlay_background:I = 0x7f070014

.field public static final everyplay_theme_button_background:I = 0x7f070023

.field public static final everyplay_theme_button_background_active:I = 0x7f070024

.field public static final everyplay_theme_button_text_color:I = 0x7f070025

.field public static final everyplay_theme_distinct_button_background:I = 0x7f070026

.field public static final everyplay_theme_distinct_button_background_active:I = 0x7f070027

.field public static final everyplay_theme_distinct_button_text_color:I = 0x7f070028

.field public static final everyplay_theme_sidemenu_background:I = 0x7f07001a

.field public static final everyplay_theme_sidemenu_item_background:I = 0x7f07001b

.field public static final everyplay_theme_sidemenu_item_background_active:I = 0x7f07001c

.field public static final everyplay_theme_sidemenu_item_badge_background:I = 0x7f07001d

.field public static final everyplay_theme_sidemenu_item_badge_text_color:I = 0x7f07001e

.field public static final everyplay_theme_sidemenu_item_description_text_color:I = 0x7f070022

.field public static final everyplay_theme_sidemenu_item_header_text_color:I = 0x7f070021

.field public static final everyplay_theme_sidemenu_item_secondary_badge_background:I = 0x7f07001f

.field public static final everyplay_theme_sidemenu_item_secondary_badge_text_color:I = 0x7f070020

.field public static final everyplay_theme_splashscreen_background:I = 0x7f070029

.field public static final everyplay_theme_splashscreen_retrybutton_text_color:I = 0x7f07002b

.field public static final everyplay_theme_splashscreen_statustext_color:I = 0x7f07002a

.field public static final everyplay_theme_topbar_actionbutton_text_color:I = 0x7f070016

.field public static final everyplay_theme_topbar_background:I = 0x7f070015

.field public static final everyplay_theme_topbar_icon_active_color:I = 0x7f070019

.field public static final everyplay_theme_topbar_icon_color:I = 0x7f070018

.field public static final everyplay_theme_topbar_title_text_color:I = 0x7f070017


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
