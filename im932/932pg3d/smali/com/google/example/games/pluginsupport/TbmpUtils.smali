.class public Lcom/google/example/games/pluginsupport/TbmpUtils;
.super Ljava/lang/Object;
.source "TbmpUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/util/ArrayList;ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 3
    .param p0, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "variant"    # I
    .param p3, "autoMatchCriteria"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer$InitiateMatchResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "playersToInvite":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig;->builder()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;

    move-result-object v0

    .line 31
    .local v0, "builder":Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;
    if-lez p2, :cond_0

    .line 32
    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;->setVariant(I)Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;

    .line 34
    :cond_0
    if-eqz p1, :cond_1

    .line 35
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;->addInvitedPlayers(Ljava/util/ArrayList;)Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;

    .line 37
    :cond_1
    invoke-virtual {v0, p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;->setAutoMatchCriteria(Landroid/os/Bundle;)Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;

    .line 38
    sget-object v1, Lcom/google/android/gms/games/Games;->TurnBasedMultiplayer:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig$Builder;->build()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig;

    move-result-object v2

    invoke-interface {v1, p0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;->createMatch(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    return-object v1
.end method

.method public static createQuickMatch(Lcom/google/android/gms/common/api/GoogleApiClient;III)Lcom/google/android/gms/common/api/PendingResult;
    .locals 4
    .param p0, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p1, "minOpponents"    # I
    .param p2, "maxOpponents"    # I
    .param p3, "variant"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "III)",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer$InitiateMatchResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    const-wide/16 v2, 0x0

    invoke-static {p1, p2, v2, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfig;->createAutoMatchCriteria(IIJ)Landroid/os/Bundle;

    move-result-object v0

    .line 22
    .local v0, "autoMatchCriteria":Landroid/os/Bundle;
    const/4 v1, 0x0

    invoke-static {p0, v1, p3, v0}, Lcom/google/example/games/pluginsupport/TbmpUtils;->create(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/util/ArrayList;ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    return-object v1
.end method
