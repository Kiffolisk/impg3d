.class public Lcom/google/example/games/pluginsupport/SignInHelperActivity;
.super Lcom/google/example/games/pluginsupport/HelperActivity;
.source "SignInHelperActivity.java"


# instance fields
.field mAttempted:Z

.field mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/example/games/pluginsupport/HelperActivity;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mAttempted:Z

    .line 19
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/example/games/pluginsupport/SignInHelperActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/example/games/pluginsupport/SignInHelperActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->callListener(Z)V

    return-void
.end method

.method private callListener(Z)V
    .locals 3
    .param p1, "succeeded"    # Z

    .prologue
    .line 22
    invoke-static {}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->getInstance()Lcom/google/example/games/pluginsupport/SignInHelperManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->getGameHelperListener()Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    move-result-object v0

    .line 24
    .local v0, "listener":Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;
    invoke-static {}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->getInstance()Lcom/google/example/games/pluginsupport/SignInHelperManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->setGameHelperListener(Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;)V

    .line 25
    if-eqz v0, :cond_0

    .line 26
    if-eqz p1, :cond_1

    .line 27
    invoke-interface {v0}, Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;->onSignInSucceeded()V

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    invoke-interface {v0}, Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;->onSignInFailed()V

    goto :goto_0
.end method


# virtual methods
.method failAndFinish()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->callListener(Z)V

    .line 49
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->finish()V

    .line 50
    return-void
.end method

.method public onSignInFailed()V
    .locals 2

    .prologue
    .line 36
    iget-boolean v1, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mAttempted:Z

    if-nez v1, :cond_0

    .line 37
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mAttempted:Z

    .line 38
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v1}, Lcom/google/example/games/basegameutils/GameHelper;->beginUserInitiatedSignIn()V

    .line 45
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v1}, Lcom/google/example/games/basegameutils/GameHelper;->getSignInError()Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    move-result-object v0

    .line 42
    .local v0, "reason":Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;
    invoke-static {}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->getInstance()Lcom/google/example/games/pluginsupport/SignInHelperManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->setSignInErrorReason(Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->failAndFinish()V

    goto :goto_0
.end method

.method public onSignInSucceeded()V
    .locals 8

    .prologue
    .line 54
    iget-object v3, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v3}, Lcom/google/example/games/basegameutils/GameHelper;->getInvitation()Lcom/google/android/gms/games/multiplayer/Invitation;

    move-result-object v1

    .line 55
    .local v1, "inv":Lcom/google/android/gms/games/multiplayer/Invitation;
    if-eqz v1, :cond_0

    .line 56
    invoke-static {}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->getInstance()Lcom/google/example/games/pluginsupport/SignInHelperManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->setInvitation(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 59
    :cond_0
    iget-object v3, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v3}, Lcom/google/example/games/basegameutils/GameHelper;->getTurnBasedMatch()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    move-result-object v2

    .line 60
    .local v2, "match":Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
    if-eqz v2, :cond_1

    .line 61
    invoke-static {}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->getInstance()Lcom/google/example/games/pluginsupport/SignInHelperManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->setTurnBasedMatch(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 64
    :cond_1
    const/16 v0, 0x3e8

    .line 65
    .local v0, "DELAY":I
    iget-object v3, p0, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/example/games/pluginsupport/SignInHelperActivity$1;

    invoke-direct {v4, p0}, Lcom/google/example/games/pluginsupport/SignInHelperActivity$1;-><init>(Lcom/google/example/games/pluginsupport/SignInHelperActivity;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 72
    return-void
.end method
