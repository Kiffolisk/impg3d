.class public abstract Lcom/google/example/games/pluginsupport/UiHelperActivity;
.super Lcom/google/example/games/pluginsupport/HelperActivity;
.source "UiHelperActivity.java"


# static fields
.field static final RC_UI:I = 0x2694


# instance fields
.field mAttempted:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/example/games/pluginsupport/HelperActivity;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->mAttempted:Z

    return-void
.end method


# virtual methods
.method protected abstract deliverFailure()V
.end method

.method protected abstract deliverSuccess(Landroid/content/Intent;)V
.end method

.method protected abstract getUiIntent()Landroid/content/Intent;
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 51
    const/16 v0, 0x2694

    if-eq p1, v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Ignoring activity result with request code "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 65
    :goto_0
    return-void

    .line 56
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->TAG:Ljava/lang/String;

    const-string v1, "UI cancelled."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->deliverFailure()V

    .line 59
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->finish()V

    goto :goto_0

    .line 62
    :cond_1
    const-string v0, "UI succeeded."

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0, p3}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->deliverSuccess(Landroid/content/Intent;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->finish()V

    goto :goto_0
.end method

.method public onSignInFailed()V
    .locals 4

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/example/games/pluginsupport/HelperActivity;->onSignInFailed()V

    .line 26
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->TAG:Ljava/lang/String;

    const-string v2, "Sign-in failed on UI helper activity."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v1}, Lcom/google/example/games/basegameutils/GameHelper;->getSignInError()Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    move-result-object v0

    .line 28
    .local v0, "reason":Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;
    if-eqz v0, :cond_0

    .line 29
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sign-in failure reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    :cond_0
    const-string v1, "Delivering failure to listener."

    invoke-virtual {p0, v1}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->deliverFailure()V

    .line 33
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->finish()V

    .line 34
    return-void
.end method

.method public onSignInSucceeded()V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Lcom/google/example/games/pluginsupport/HelperActivity;->onSignInSucceeded()V

    .line 39
    iget-boolean v1, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->mAttempted:Z

    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->TAG:Ljava/lang/String;

    const-string v2, "Ignoring onSignInSuceeded because we already launched the UI."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :goto_0
    return-void

    .line 43
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/example/games/pluginsupport/UiHelperActivity;->mAttempted:Z

    .line 44
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->getUiIntent()Landroid/content/Intent;

    move-result-object v0

    .line 45
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "Launching intent"

    invoke-virtual {p0, v1}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 46
    const/16 v1, 0x2694

    invoke-virtual {p0, v0, v1}, Lcom/google/example/games/pluginsupport/UiHelperActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
