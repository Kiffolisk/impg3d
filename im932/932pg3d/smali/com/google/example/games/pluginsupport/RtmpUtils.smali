.class public Lcom/google/example/games/pluginsupport/RtmpUtils;
.super Ljava/lang/Object;
.source "RtmpUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static accept(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)V
    .locals 3
    .param p0, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p1, "invitationId"    # Ljava/lang/String;
    .param p2, "roomUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;
    .param p3, "roomStatusUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;
    .param p4, "messageReceivedListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;

    .prologue
    .line 59
    invoke-static {p2, p3, p4}, Lcom/google/example/games/pluginsupport/RtmpUtils;->makeRoomConfigBuilder(Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    move-result-object v0

    .line 61
    .local v0, "builder":Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->setInvitationIdToAccept(Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    .line 62
    sget-object v1, Lcom/google/android/gms/games/Games;->RealTimeMultiplayer:Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->build()Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig;

    move-result-object v2

    invoke-interface {v1, p0, v2}, Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;->join(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig;)V

    .line 63
    return-void
.end method

.method public static create(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/util/ArrayList;ILandroid/os/Bundle;Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)V
    .locals 3
    .param p0, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "variant"    # I
    .param p3, "autoMatchCriteria"    # Landroid/os/Bundle;
    .param p4, "roomUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;
    .param p5, "roomStatusUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;
    .param p6, "messageReceivedListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Landroid/os/Bundle;",
            "Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;",
            "Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;",
            "Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "playersToInvite":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p4, p5, p6}, Lcom/google/example/games/pluginsupport/RtmpUtils;->makeRoomConfigBuilder(Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    move-result-object v0

    .line 34
    .local v0, "builder":Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;
    if-lez p2, :cond_0

    .line 35
    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->setVariant(I)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    .line 37
    :cond_0
    if-eqz p1, :cond_1

    .line 38
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->addPlayersToInvite(Ljava/util/ArrayList;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    .line 40
    :cond_1
    invoke-virtual {v0, p3}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->setAutoMatchCriteria(Landroid/os/Bundle;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    .line 41
    sget-object v1, Lcom/google/android/gms/games/Games;->RealTimeMultiplayer:Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->build()Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig;

    move-result-object v2

    invoke-interface {v1, p0, v2}, Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;->create(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig;)V

    .line 42
    return-void
.end method

.method public static createQuickGame(Lcom/google/android/gms/common/api/GoogleApiClient;IIILcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)V
    .locals 7
    .param p0, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p1, "minOpponents"    # I
    .param p2, "maxOpponents"    # I
    .param p3, "variant"    # I
    .param p4, "roomUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;
    .param p5, "roomStatusUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;
    .param p6, "messageReceivedListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;

    .prologue
    .line 20
    const-wide/16 v0, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig;->createAutoMatchCriteria(IIJ)Landroid/os/Bundle;

    move-result-object v3

    .line 21
    .local v3, "autoMatchCriteria":Landroid/os/Bundle;
    const/4 v1, 0x0

    move-object v0, p0

    move v2, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/google/example/games/pluginsupport/RtmpUtils;->create(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/util/ArrayList;ILandroid/os/Bundle;Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)V

    .line 23
    return-void
.end method

.method private static makeRoomConfigBuilder(Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;
    .locals 1
    .param p0, "roomUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;
    .param p1, "roomStatusUpdateListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;
    .param p2, "messageReceivedListener"    # Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig;->builder(Lcom/google/android/gms/games/multiplayer/realtime/RoomUpdateListener;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    move-result-object v0

    .line 49
    .local v0, "builder":Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;
    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->setMessageReceivedListener(Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessageReceivedListener;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    .line 50
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;->setRoomStatusUpdateListener(Lcom/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener;)Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig$Builder;

    .line 51
    return-object v0
.end method
