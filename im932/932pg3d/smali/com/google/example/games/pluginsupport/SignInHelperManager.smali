.class public Lcom/google/example/games/pluginsupport/SignInHelperManager;
.super Ljava/lang/Object;
.source "SignInHelperManager.java"


# static fields
.field private static final RC_UNUSED:I = 0x1869f

.field private static final TAG:Ljava/lang/String; = "SignInHelperManager"

.field private static sInstance:Lcom/google/example/games/pluginsupport/SignInHelperManager;


# instance fields
.field mInvitation:Lcom/google/android/gms/games/multiplayer/Invitation;

.field mListener:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

.field mMatch:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

.field mSignInErrorActivityResponse:I

.field mSignInErrorCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/example/games/pluginsupport/SignInHelperManager;

    invoke-direct {v0}, Lcom/google/example/games/pluginsupport/SignInHelperManager;-><init>()V

    sput-object v0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->sInstance:Lcom/google/example/games/pluginsupport/SignInHelperManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mListener:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    .line 22
    iput v1, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorActivityResponse:I

    .line 23
    iput v1, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorCode:I

    .line 24
    iput-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mInvitation:Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 25
    iput-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mMatch:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 27
    return-void
.end method

.method public static getInstance()Lcom/google/example/games/pluginsupport/SignInHelperManager;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->sInstance:Lcom/google/example/games/pluginsupport/SignInHelperManager;

    return-object v0
.end method

.method public static launchSignIn(Landroid/app/Activity;Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;Z)V
    .locals 2
    .param p0, "act"    # Landroid/app/Activity;
    .param p1, "listener"    # Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;
    .param p2, "debugEnabled"    # Z

    .prologue
    .line 83
    sget-object v1, Lcom/google/example/games/pluginsupport/SignInHelperManager;->sInstance:Lcom/google/example/games/pluginsupport/SignInHelperManager;

    invoke-virtual {v1, p1}, Lcom/google/example/games/pluginsupport/SignInHelperManager;->setGameHelperListener(Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;)V

    .line 84
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/example/games/pluginsupport/SignInHelperActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/google/example/games/pluginsupport/SignInHelperActivity;->EXTRA_DEBUG_ENABLED:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void
.end method

.method public static showErrorDialog(Landroid/app/Activity;)V
    .locals 2
    .param p0, "act"    # Landroid/app/Activity;

    .prologue
    .line 90
    sget-object v0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->sInstance:Lcom/google/example/games/pluginsupport/SignInHelperManager;

    iget v0, v0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorCode:I

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->sInstance:Lcom/google/example/games/pluginsupport/SignInHelperManager;

    iget v0, v0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorActivityResponse:I

    sget-object v1, Lcom/google/example/games/pluginsupport/SignInHelperManager;->sInstance:Lcom/google/example/games/pluginsupport/SignInHelperManager;

    iget v1, v1, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorCode:I

    invoke-static {p0, v0, v1}, Lcom/google/example/games/basegameutils/GameHelper;->showFailureDialog(Landroid/app/Activity;II)V

    .line 94
    :cond_0
    return-void
.end method


# virtual methods
.method public forgetInvitation()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mInvitation:Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 65
    return-void
.end method

.method public forgetTurnBasedMatch()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mMatch:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 69
    return-void
.end method

.method public getGameHelperListener()Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mListener:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    return-object v0
.end method

.method public getInvitation()Lcom/google/android/gms/games/multiplayer/Invitation;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mInvitation:Lcom/google/android/gms/games/multiplayer/Invitation;

    return-object v0
.end method

.method public getTurnBasedMatch()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mMatch:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    return-object v0
.end method

.method public hasInvitation()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mInvitation:Lcom/google/android/gms/games/multiplayer/Invitation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTurnBasedMatch()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mMatch:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGameHelperListener(Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mListener:Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;

    .line 31
    return-void
.end method

.method public setInvitation(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 0
    .param p1, "inv"    # Lcom/google/android/gms/games/multiplayer/Invitation;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mInvitation:Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 43
    return-void
.end method

.method setSignInErrorReason(Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;)V
    .locals 1
    .param p1, "reason"    # Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;

    .prologue
    .line 72
    if-eqz p1, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->getActivityResultCode()I

    move-result v0

    iput v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorActivityResponse:I

    .line 74
    invoke-virtual {p1}, Lcom/google/example/games/basegameutils/GameHelper$SignInFailureReason;->getServiceErrorCode()I

    move-result v0

    iput v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorCode:I

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorActivityResponse:I

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mSignInErrorCode:I

    goto :goto_0
.end method

.method public setTurnBasedMatch(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 0
    .param p1, "m"    # Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/example/games/pluginsupport/SignInHelperManager;->mMatch:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 46
    return-void
.end method
