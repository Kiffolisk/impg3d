.class public Lcom/google/example/games/pluginsupport/HelperActivity;
.super Landroid/app/Activity;
.source "HelperActivity.java"

# interfaces
.implements Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;


# static fields
.field static final BG_COLOR:I = 0x40ffffff

.field public static EXTRA_DEBUG_ENABLED:Ljava/lang/String;


# instance fields
.field TAG:Ljava/lang/String;

.field protected mDebugEnabled:Z

.field protected mHelper:Lcom/google/example/games/basegameutils/GameHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "EXTRA_DEBUG_ENABLED"

    sput-object v0, Lcom/google/example/games/pluginsupport/HelperActivity;->EXTRA_DEBUG_ENABLED:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 14
    const-string v0, "PluginSupport"

    iput-object v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->TAG:Ljava/lang/String;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mDebugEnabled:Z

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->TAG:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method protected debugLog(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mDebugEnabled:Z

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityResult("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/HelperActivity;->debugLog(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/example/games/basegameutils/GameHelper;->onActivityResult(IILandroid/content/Intent;)V

    .line 42
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 43
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/HelperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/HelperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v3, Lcom/google/example/games/pluginsupport/HelperActivity;->EXTRA_DEBUG_ENABLED:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mDebugEnabled:Z

    .line 48
    const-string v1, "onCreate()"

    invoke-virtual {p0, v1}, Lcom/google/example/games/pluginsupport/HelperActivity;->debugLog(Ljava/lang/String;)V

    .line 50
    new-instance v1, Lcom/google/example/games/basegameutils/GameHelper;

    const/4 v3, 0x7

    invoke-direct {v1, p0, v3}, Lcom/google/example/games/basegameutils/GameHelper;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    .line 51
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    iget-boolean v3, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mDebugEnabled:Z

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/GameHelper"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/example/games/basegameutils/GameHelper;->enableDebugLog(ZLjava/lang/String;)V

    .line 52
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v1, v2}, Lcom/google/example/games/basegameutils/GameHelper;->setShowErrorDialogs(Z)V

    .line 53
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v1, v2}, Lcom/google/example/games/basegameutils/GameHelper;->setMaxAutoSignInAttempts(I)V

    .line 54
    iget-object v1, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v1, p0}, Lcom/google/example/games/basegameutils/GameHelper;->setup(Lcom/google/example/games/basegameutils/GameHelper$GameHelperListener;)V

    .line 55
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 56
    .local v0, "v":Landroid/view/View;
    const v1, 0x40ffffff    # 7.9999995f

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 57
    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/HelperActivity;->setContentView(Landroid/view/View;)V

    .line 58
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    return-void

    .end local v0    # "v":Landroid/view/View;
    :cond_0
    move v1, v2

    .line 46
    goto :goto_0
.end method

.method public onSignInFailed()V
    .locals 1

    .prologue
    .line 63
    const-string v0, "onSignInFailed()"

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/HelperActivity;->debugLog(Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public onSignInSucceeded()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "onSignInSuceeded()"

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/HelperActivity;->debugLog(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "onStart()"

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/HelperActivity;->debugLog(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v0, p0}, Lcom/google/example/games/basegameutils/GameHelper;->onStart(Landroid/app/Activity;)V

    .line 35
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 36
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "onStop()"

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/HelperActivity;->debugLog(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/google/example/games/pluginsupport/HelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v0}, Lcom/google/example/games/basegameutils/GameHelper;->onStop()V

    .line 28
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 29
    return-void
.end method
