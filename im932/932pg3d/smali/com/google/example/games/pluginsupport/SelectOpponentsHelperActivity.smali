.class public Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;
.super Lcom/google/example/games/pluginsupport/UiHelperActivity;
.source "SelectOpponentsHelperActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;
    }
.end annotation


# static fields
.field public static final EXTRA_IS_RTMP:Ljava/lang/String; = "EXTRA_IS_RTMP"

.field public static final EXTRA_MAX_OPPONENTS:Ljava/lang/String; = "EXTRA_MAX_OPPONENTS"

.field public static final EXTRA_MIN_OPPONENTS:Ljava/lang/String; = "EXTRA_MIN_OPPONENTS"

.field static sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;


# instance fields
.field mDummyObject:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->mDummyObject:Ljava/lang/Object;

    .line 24
    return-void
.end method

.method public static launch(ZLandroid/app/Activity;Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;ZII)V
    .locals 2
    .param p0, "isRtmp"    # Z
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;
    .param p3, "debugEnabled"    # Z
    .param p4, "minOpponents"    # I
    .param p5, "maxOpponents"    # I

    .prologue
    .line 85
    invoke-static {p2}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->setListener(Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;)V

    .line 86
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->EXTRA_DEBUG_ENABLED:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 88
    const-string v1, "EXTRA_IS_RTMP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 89
    const-string v1, "EXTRA_MIN_OPPONENTS"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 90
    const-string v1, "EXTRA_MAX_OPPONENTS"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 91
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 92
    return-void
.end method

.method public static setListener(Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;)V
    .locals 0
    .param p0, "listener"    # Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    .prologue
    .line 31
    sput-object p0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    .line 32
    return-void
.end method


# virtual methods
.method protected deliverFailure()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 36
    sget-object v0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    if-eqz v0, :cond_0

    .line 37
    const-string v0, "Delivering failure to listener."

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 38
    sget-object v0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    iget-object v1, p0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->mDummyObject:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->mDummyObject:Ljava/lang/Object;

    invoke-interface {v0, v3, v1, v3, v2}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;->onSelectOpponentsResult(ZLjava/lang/Object;ZLjava/lang/Object;)V

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    .line 41
    :cond_0
    return-void
.end method

.method protected deliverSuccess(Landroid/content/Intent;)V
    .locals 8
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 46
    const-string v6, "players"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 47
    .local v1, "invitees":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invitee count: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 50
    const/4 v0, 0x0

    .line 51
    .local v0, "autoMatchCriteria":Landroid/os/Bundle;
    const-string v6, "min_automatch_players"

    invoke-virtual {p1, v6, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 52
    .local v3, "minAutoMatchPlayers":I
    const-string v6, "max_automatch_players"

    invoke-virtual {p1, v6, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 53
    .local v2, "maxAutoMatchPlayers":I
    if-gtz v3, :cond_0

    if-lez v2, :cond_4

    .line 54
    :cond_0
    const-wide/16 v6, 0x0

    invoke-static {v3, v2, v6, v7}, Lcom/google/android/gms/games/multiplayer/realtime/RoomConfig;->createAutoMatchCriteria(IIJ)Landroid/os/Bundle;

    move-result-object v0

    .line 56
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Automatch criteria: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 61
    :goto_0
    sget-object v6, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    if-eqz v6, :cond_3

    .line 62
    const-string v6, "Calling listener."

    invoke-virtual {p0, v6}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 63
    sget-object v6, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    if-eqz v0, :cond_1

    move v4, v5

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->mDummyObject:Ljava/lang/Object;

    .end local v0    # "autoMatchCriteria":Landroid/os/Bundle;
    :cond_2
    invoke-interface {v6, v5, v1, v4, v0}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;->onSelectOpponentsResult(ZLjava/lang/Object;ZLjava/lang/Object;)V

    .line 65
    const/4 v4, 0x0

    sput-object v4, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity$Listener;

    .line 67
    :cond_3
    return-void

    .line 58
    .restart local v0    # "autoMatchCriteria":Landroid/os/Bundle;
    :cond_4
    const-string v6, "No automatch criteria."

    invoke-virtual {p0, v6}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->debugLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getUiIntent()Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 71
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "EXTRA_MIN_OPPONENTS"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 72
    .local v2, "minOpponents":I
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "EXTRA_MAX_OPPONENTS"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 73
    .local v1, "maxOpponents":I
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "EXTRA_IS_RTMP"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 74
    .local v0, "isRtmp":Z
    if-eqz v0, :cond_0

    .line 75
    sget-object v3, Lcom/google/android/gms/games/Games;->RealTimeMultiplayer:Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;

    iget-object v4, p0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v4}, Lcom/google/example/games/basegameutils/GameHelper;->getApiClient()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v4

    invoke-interface {v3, v4, v2, v1}, Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;->getSelectOpponentsIntent(Lcom/google/android/gms/common/api/GoogleApiClient;II)Landroid/content/Intent;

    move-result-object v3

    .line 78
    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Lcom/google/android/gms/games/Games;->TurnBasedMultiplayer:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;

    iget-object v4, p0, Lcom/google/example/games/pluginsupport/SelectOpponentsHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v4}, Lcom/google/example/games/basegameutils/GameHelper;->getApiClient()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v4

    invoke-interface {v3, v4, v2, v1, v5}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;->getSelectOpponentsIntent(Lcom/google/android/gms/common/api/GoogleApiClient;IIZ)Landroid/content/Intent;

    move-result-object v3

    goto :goto_0
.end method
