.class public Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;
.super Lcom/google/example/games/pluginsupport/UiHelperActivity;
.source "InvitationInboxHelperActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;
    }
.end annotation


# static fields
.field private static final EXTRA_IS_RTMP:Ljava/lang/String; = "EXTRA_IS_RTMP"

.field static sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/example/games/pluginsupport/UiHelperActivity;-><init>()V

    .line 19
    return-void
.end method

.method public static launch(ZLandroid/app/Activity;Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;Z)V
    .locals 2
    .param p0, "isRtmp"    # Z
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;
    .param p3, "debugEnabled"    # Z

    .prologue
    .line 70
    invoke-static {p2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->setListener(Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;)V

    .line 71
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "EXTRA_IS_RTMP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 73
    sget-object v1, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->EXTRA_DEBUG_ENABLED:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 75
    return-void
.end method

.method public static setListener(Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;)V
    .locals 0
    .param p0, "listener"    # Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    .prologue
    .line 26
    sput-object p0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    .line 27
    return-void
.end method


# virtual methods
.method protected deliverFailure()V
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    if-eqz v0, :cond_0

    .line 32
    const-string v0, "Delivering failure to listener."

    invoke-virtual {p0, v0}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 33
    sget-object v0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    const/4 v1, 0x0

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;->onInvitationInboxResult(ZLjava/lang/String;)V

    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    .line 36
    :cond_0
    return-void
.end method

.method protected deliverSuccess(Landroid/content/Intent;)V
    .locals 6
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 40
    const-string v2, "Parsing invitation/match from UI\'s returned data."

    invoke-virtual {p0, v2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "invitation"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 42
    .local v0, "inv":Lcom/google/android/gms/games/multiplayer/Invitation;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "turn_based_match"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 43
    .local v1, "match":Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invitation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_0

    const-string v2, "null"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 44
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Match: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v1, :cond_1

    const-string v2, "null"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 46
    if-eqz v0, :cond_2

    .line 47
    const-string v2, "Calling listener to deliver invitation."

    invoke-virtual {p0, v2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 48
    sget-object v2, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    const/4 v3, 0x1

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->getInvitationId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;->onInvitationInboxResult(ZLjava/lang/String;)V

    .line 58
    :goto_2
    sput-object v5, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    .line 59
    return-void

    .line 43
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 44
    :cond_1
    const-string v2, "[TurnBasedMatch]"

    goto :goto_1

    .line 49
    :cond_2
    if-eqz v1, :cond_3

    .line 50
    const-string v2, "Calling listener to deliver match."

    invoke-virtual {p0, v2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 51
    sget-object v2, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    invoke-interface {v2, v1}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;->onTurnBasedMatch(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_2

    .line 53
    :cond_3
    iget-object v2, p0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->TAG:Ljava/lang/String;

    const-string v3, "Invitation inbox result came with no invitation and no match!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const-string v2, "Calling listener to deliver failure."

    invoke-virtual {p0, v2}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->debugLog(Ljava/lang/String;)V

    .line 55
    sget-object v2, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->sListener:Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;

    const/4 v3, 0x0

    invoke-interface {v2, v3, v5}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity$Listener;->onInvitationInboxResult(ZLjava/lang/String;)V

    goto :goto_2
.end method

.method protected getUiIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_IS_RTMP"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 64
    .local v0, "isRtmp":Z
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/games/Games;->Invitations:Lcom/google/android/gms/games/multiplayer/Invitations;

    iget-object v2, p0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v2}, Lcom/google/example/games/basegameutils/GameHelper;->getApiClient()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/multiplayer/Invitations;->getInvitationInboxIntent(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/content/Intent;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/gms/games/Games;->TurnBasedMultiplayer:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;

    iget-object v2, p0, Lcom/google/example/games/pluginsupport/InvitationInboxHelperActivity;->mHelper:Lcom/google/example/games/basegameutils/GameHelper;

    invoke-virtual {v2}, Lcom/google/example/games/basegameutils/GameHelper;->getApiClient()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;->getInboxIntent(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method
