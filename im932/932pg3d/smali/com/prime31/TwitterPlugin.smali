.class public Lcom/prime31/TwitterPlugin;
.super Lcom/prime31/TwitterPluginBase;
.source "TwitterPlugin.java"


# static fields
.field private static final apiUploadUrlPrefix:Ljava/lang/String; = "https://api.twitter.com"

.field private static final apiUrlPrefix:Ljava/lang/String; = "https://api.twitter.com"

.field public static callbackUrlPrefix:Ljava/lang/String;


# instance fields
.field private _accessToken:Lorg/scribe/model/Token;

.field private _requestToken:Lorg/scribe/model/Token;

.field private _service:Lorg/scribe/oauth/OAuthService;

.field private _session:Lcom/prime31/TwitterSession;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "twitterplugin"

    sput-object v0, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/prime31/TwitterPluginBase;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/prime31/TwitterPlugin;->continueLoginWithVerifier(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1(Lcom/prime31/TwitterPlugin;)Lorg/scribe/oauth/OAuthService;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_service:Lorg/scribe/oauth/OAuthService;

    return-object v0
.end method

.method static synthetic access$10(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 283
    invoke-direct {p0, p1, p2, p3}, Lcom/prime31/TwitterPlugin;->performRequestInternal(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$2(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2, p3}, Lcom/prime31/TwitterPlugin;->initBlockingThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3(Lcom/prime31/TwitterPlugin;)Lorg/scribe/model/Token;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_requestToken:Lorg/scribe/model/Token;

    return-object v0
.end method

.method static synthetic access$4(Lcom/prime31/TwitterPlugin;Lorg/scribe/model/Token;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/prime31/TwitterPlugin;->_accessToken:Lorg/scribe/model/Token;

    return-void
.end method

.method static synthetic access$5(Lcom/prime31/TwitterPlugin;)Lorg/scribe/model/Token;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_accessToken:Lorg/scribe/model/Token;

    return-object v0
.end method

.method static synthetic access$6(Lcom/prime31/TwitterPlugin;)Lcom/prime31/TwitterSession;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    return-object v0
.end method

.method static synthetic access$7(Lcom/prime31/TwitterPlugin;Lorg/scribe/model/Token;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/prime31/TwitterPlugin;->_requestToken:Lorg/scribe/model/Token;

    return-void
.end method

.method static synthetic access$8(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/prime31/TwitterPlugin;->showLoginDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$9(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Landroid/os/Bundle;[B)V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0, p1, p2, p3}, Lcom/prime31/TwitterPlugin;->performMultipartRequestInternal(Ljava/lang/String;Landroid/os/Bundle;[B)V

    return-void
.end method

.method private continueLoginWithVerifier(Ljava/lang/String;)V
    .locals 2
    .param p1, "oauthVerifier"    # Ljava/lang/String;

    .prologue
    .line 231
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/prime31/TwitterPlugin$2;

    invoke-direct {v1, p0, p1}, Lcom/prime31/TwitterPlugin$2;-><init>(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 279
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 280
    return-void
.end method

.method private initBlockingThread(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "consumerKey"    # Ljava/lang/String;
    .param p2, "consumerSecret"    # Ljava/lang/String;
    .param p3, "callbackUrlScheme"    # Ljava/lang/String;

    .prologue
    .line 115
    sput-object p3, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    .line 117
    const-string v0, "Prime31"

    const-string v1, "initializing Twitter service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    new-instance v0, Lcom/prime31/TwitterSession;

    invoke-virtual {p0}, Lcom/prime31/TwitterPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/prime31/TwitterSession;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    .line 121
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    invoke-virtual {v0}, Lcom/prime31/TwitterSession;->getAccessToken()Lorg/scribe/model/Token;

    move-result-object v0

    iput-object v0, p0, Lcom/prime31/TwitterPlugin;->_accessToken:Lorg/scribe/model/Token;

    .line 124
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    invoke-virtual {v0}, Lcom/prime31/TwitterSession;->getConsumerKey()Ljava/lang/String;

    move-result-object p1

    .line 127
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    invoke-virtual {v0}, Lcom/prime31/TwitterSession;->getConsumerSecret()Ljava/lang/String;

    move-result-object p2

    .line 128
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    invoke-virtual {v0}, Lcom/prime31/TwitterSession;->getCallbackUrlPrefix()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    .line 135
    :goto_0
    new-instance v0, Lorg/scribe/builder/ServiceBuilder;

    invoke-direct {v0}, Lorg/scribe/builder/ServiceBuilder;-><init>()V

    const-class v1, Lorg/scribe/builder/api/TwitterApi$SSL;

    invoke-virtual {v0, v1}, Lorg/scribe/builder/ServiceBuilder;->provider(Ljava/lang/Class;)Lorg/scribe/builder/ServiceBuilder;

    move-result-object v0

    .line 136
    invoke-virtual {v0, p1}, Lorg/scribe/builder/ServiceBuilder;->apiKey(Ljava/lang/String;)Lorg/scribe/builder/ServiceBuilder;

    move-result-object v0

    .line 137
    invoke-virtual {v0, p2}, Lorg/scribe/builder/ServiceBuilder;->apiSecret(Ljava/lang/String;)Lorg/scribe/builder/ServiceBuilder;

    move-result-object v0

    .line 138
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "://connect"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/scribe/builder/ServiceBuilder;->callback(Ljava/lang/String;)Lorg/scribe/builder/ServiceBuilder;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lorg/scribe/builder/ServiceBuilder;->build()Lorg/scribe/oauth/OAuthService;

    move-result-object v0

    .line 135
    iput-object v0, p0, Lcom/prime31/TwitterPlugin;->_service:Lorg/scribe/oauth/OAuthService;

    .line 141
    const-string v0, "Prime31"

    const-string v1, "initialized Twitter service."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const-string v0, "loginFailed"

    const-string v1, "twitterInitialized"

    invoke-virtual {p0, v0, v1}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    invoke-virtual {v0, p1, p2, p3}, Lcom/prime31/TwitterSession;->storeAppDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static onNewIntent(Landroid/content/Intent;)V
    .locals 14
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x0

    .line 53
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    .line 54
    .local v6, "targetUri":Landroid/net/Uri;
    if-eqz v6, :cond_0

    .line 56
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 57
    .local v3, "launchUrl":Ljava/lang/String;
    sget-object v11, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    invoke-virtual {v3, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 59
    const-string v10, "Prime31"

    const-string v11, "deep link not for TwitterPlugin. bailing."

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    .end local v3    # "launchUrl":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 63
    .restart local v3    # "launchUrl":Ljava/lang/String;
    :cond_1
    const-string v11, "Prime31"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "incoming deep link being processed by TwitterPlugin: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const-string v11, "Prime31"

    const-string v12, "login complete. Attempting to fetch the oauth_verifier or see if there is a canceled login."

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :try_start_0
    sget-object v11, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    const-string v12, "http"

    invoke-virtual {v3, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 68
    const/4 v2, 0x0

    .line 70
    .local v2, "foundVerifier":Z
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 71
    .local v7, "url":Ljava/net/URL;
    invoke-virtual {v7}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, "query":Ljava/lang/String;
    const/4 v9, 0x0

    .line 74
    .local v9, "verifier":Ljava/lang/String;
    const-string v11, "&"

    invoke-virtual {v5, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "array":[Ljava/lang/String;
    array-length v11, v0

    :goto_1
    if-lt v10, v11, :cond_2

    .line 89
    :goto_2
    if-eqz v2, :cond_4

    .line 90
    invoke-static {}, Lcom/prime31/TwitterPlugin;->instance()Lcom/prime31/TwitterPlugin;

    move-result-object v10

    invoke-direct {v10, v9}, Lcom/prime31/TwitterPlugin;->continueLoginWithVerifier(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 94
    .end local v0    # "array":[Ljava/lang/String;
    .end local v2    # "foundVerifier":Z
    .end local v5    # "query":Ljava/lang/String;
    .end local v7    # "url":Ljava/net/URL;
    .end local v9    # "verifier":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 97
    invoke-static {}, Lcom/prime31/TwitterPlugin;->instance()Lcom/prime31/TwitterPlugin;

    move-result-object v10

    const-string v11, "loginFailed"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "malformedURL: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 76
    .end local v1    # "e":Ljava/net/MalformedURLException;
    .restart local v0    # "array":[Ljava/lang/String;
    .restart local v2    # "foundVerifier":Z
    .restart local v5    # "query":Ljava/lang/String;
    .restart local v7    # "url":Ljava/net/URL;
    .restart local v9    # "verifier":Ljava/lang/String;
    :cond_2
    :try_start_1
    aget-object v4, v0, v10

    .line 78
    .local v4, "parameter":Ljava/lang/String;
    const-string v12, "="

    invoke-virtual {v4, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 80
    .local v8, "v":[Ljava/lang/String;
    const/4 v12, 0x0

    aget-object v12, v8, v12

    const-string v13, "UTF-8"

    invoke-static {v12, v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "oauth_verifier"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 82
    const/4 v10, 0x1

    aget-object v10, v8, v10

    const-string v11, "UTF-8"

    invoke-static {v10, v11}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 83
    const/4 v2, 0x1

    .line 84
    const-string v10, "Prime31"

    const-string v11, "found verifier"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 99
    .end local v0    # "array":[Ljava/lang/String;
    .end local v2    # "foundVerifier":Z
    .end local v4    # "parameter":Ljava/lang/String;
    .end local v5    # "query":Ljava/lang/String;
    .end local v7    # "url":Ljava/net/URL;
    .end local v8    # "v":[Ljava/lang/String;
    .end local v9    # "verifier":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 101
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 102
    invoke-static {}, Lcom/prime31/TwitterPlugin;->instance()Lcom/prime31/TwitterPlugin;

    move-result-object v10

    const-string v11, "loginFailed"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "couldn\'t decode URL: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 76
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v0    # "array":[Ljava/lang/String;
    .restart local v2    # "foundVerifier":Z
    .restart local v4    # "parameter":Ljava/lang/String;
    .restart local v5    # "query":Ljava/lang/String;
    .restart local v7    # "url":Ljava/net/URL;
    .restart local v8    # "v":[Ljava/lang/String;
    .restart local v9    # "verifier":Ljava/lang/String;
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 92
    .end local v4    # "parameter":Ljava/lang/String;
    .end local v8    # "v":[Ljava/lang/String;
    :cond_4
    :try_start_2
    invoke-static {}, Lcom/prime31/TwitterPlugin;->instance()Lcom/prime31/TwitterPlugin;

    move-result-object v10

    const-string v11, "loginFailed"

    const-string v12, "cancelled"

    invoke-virtual {v10, v11, v12}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method

.method private performMultipartRequestInternal(Ljava/lang/String;Landroid/os/Bundle;[B)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "params"    # Landroid/os/Bundle;
    .param p3, "image"    # [B

    .prologue
    .line 332
    :try_start_0
    new-instance v4, Lorg/scribe/model/OAuthRequest;

    sget-object v6, Lorg/scribe/model/Verb;->POST:Lorg/scribe/model/Verb;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "https://api.twitter.com"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/scribe/model/OAuthRequest;-><init>(Lorg/scribe/model/Verb;Ljava/lang/String;)V

    .line 333
    .local v4, "request":Lorg/scribe/model/OAuthRequest;
    const-string v0, "---------------------------14737809831466499882746641449"

    .line 334
    .local v0, "boundary":Ljava/lang/String;
    const-string v6, "Content-Type"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "multipart/form-data; boundary="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lorg/scribe/model/OAuthRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v6, "Connection"

    const-string v7, "Keep-Alive"

    invoke-virtual {v4, v6, v7}, Lorg/scribe/model/OAuthRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 339
    .local v3, "os":Ljava/io/ByteArrayOutputStream;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "--"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 340
    const-string v6, "Content-Disposition: attachment; name=\"media[]\"; filename=\"screenshot.png\"\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 341
    const-string v6, "Content-Type: application/octet-stream\r\n\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 342
    invoke-virtual {v3, p3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 343
    const-string v6, "\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 346
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "--"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 347
    const-string v6, "Content-Disposition: form-data; name=\"status\"\r\n\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 348
    const-string v6, "status"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 349
    const-string v6, "\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 352
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "--"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "--\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 355
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/scribe/model/OAuthRequest;->addPayload([B)V

    .line 358
    iget-object v6, p0, Lcom/prime31/TwitterPlugin;->_service:Lorg/scribe/oauth/OAuthService;

    iget-object v7, p0, Lcom/prime31/TwitterPlugin;->_accessToken:Lorg/scribe/model/Token;

    invoke-interface {v6, v7, v4}, Lorg/scribe/oauth/OAuthService;->signRequest(Lorg/scribe/model/Token;Lorg/scribe/model/OAuthRequest;)V

    .line 359
    invoke-virtual {v4}, Lorg/scribe/model/OAuthRequest;->send()Lorg/scribe/model/Response;

    move-result-object v5

    .line 361
    .local v5, "response":Lorg/scribe/model/Response;
    const-string v6, "Prime31"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Twitter response status code: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/scribe/model/Response;->getCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    invoke-virtual {v5}, Lorg/scribe/model/Response;->isSuccessful()Z

    move-result v6

    if-nez v6, :cond_0

    .line 364
    new-instance v6, Ljava/lang/Exception;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Twitter returned status code: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/scribe/model/Response;->getCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    .end local v0    # "boundary":Ljava/lang/String;
    .end local v3    # "os":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "request":Lorg/scribe/model/OAuthRequest;
    .end local v5    # "response":Lorg/scribe/model/Response;
    :catch_0
    move-exception v1

    .line 370
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 371
    .local v2, "message":Ljava/lang/String;
    const-string v6, "requestFailed"

    if-eqz v2, :cond_1

    .end local v2    # "message":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v6, v2}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 366
    .restart local v0    # "boundary":Ljava/lang/String;
    .restart local v3    # "os":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "request":Lorg/scribe/model/OAuthRequest;
    .restart local v5    # "response":Lorg/scribe/model/Response;
    :cond_0
    :try_start_1
    const-string v6, "requestSucceeded"

    invoke-virtual {v5}, Lorg/scribe/model/Response;->getBody()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 371
    .end local v0    # "boundary":Ljava/lang/String;
    .end local v3    # "os":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "request":Lorg/scribe/model/OAuthRequest;
    .end local v5    # "response":Lorg/scribe/model/Response;
    .restart local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "message":Ljava/lang/String;
    :cond_1
    const-string v2, "Error performing request"

    goto :goto_0
.end method

.method private performRequestInternal(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "params"    # Landroid/os/Bundle;

    .prologue
    .line 289
    :try_start_0
    const-string v7, "get"

    invoke-virtual {p1, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_2

    .line 290
    sget-object v6, Lorg/scribe/model/Verb;->GET:Lorg/scribe/model/Verb;

    .line 295
    .local v6, "verb":Lorg/scribe/model/Verb;
    :goto_0
    const-string v7, "/"

    invoke-virtual {p2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 296
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "/"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 298
    :cond_0
    new-instance v4, Lorg/scribe/model/OAuthRequest;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "https://api.twitter.com"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/scribe/model/OAuthRequest;-><init>(Lorg/scribe/model/Verb;Ljava/lang/String;)V

    .line 301
    .local v4, "request":Lorg/scribe/model/OAuthRequest;
    if-eqz p3, :cond_1

    .line 303
    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 304
    .local v0, "allKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 312
    .end local v0    # "allKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    iget-object v7, p0, Lcom/prime31/TwitterPlugin;->_service:Lorg/scribe/oauth/OAuthService;

    iget-object v8, p0, Lcom/prime31/TwitterPlugin;->_accessToken:Lorg/scribe/model/Token;

    invoke-interface {v7, v8, v4}, Lorg/scribe/oauth/OAuthService;->signRequest(Lorg/scribe/model/Token;Lorg/scribe/model/OAuthRequest;)V

    .line 313
    invoke-virtual {v4}, Lorg/scribe/model/OAuthRequest;->send()Lorg/scribe/model/Response;

    move-result-object v5

    .line 315
    .local v5, "response":Lorg/scribe/model/Response;
    invoke-virtual {v5}, Lorg/scribe/model/Response;->isSuccessful()Z

    move-result v7

    if-nez v7, :cond_4

    .line 316
    new-instance v7, Ljava/lang/Exception;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Twitter returned status code: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/scribe/model/Response;->getCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    .end local v4    # "request":Lorg/scribe/model/OAuthRequest;
    .end local v5    # "response":Lorg/scribe/model/Response;
    .end local v6    # "verb":Lorg/scribe/model/Verb;
    :catch_0
    move-exception v1

    .line 322
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 323
    .local v3, "message":Ljava/lang/String;
    const-string v7, "requestFailed"

    if-eqz v3, :cond_5

    .end local v3    # "message":Ljava/lang/String;
    :goto_2
    invoke-virtual {p0, v7, v3}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_3
    return-void

    .line 292
    :cond_2
    :try_start_1
    sget-object v6, Lorg/scribe/model/Verb;->POST:Lorg/scribe/model/Verb;

    .restart local v6    # "verb":Lorg/scribe/model/Verb;
    goto :goto_0

    .line 304
    .restart local v0    # "allKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v4    # "request":Lorg/scribe/model/OAuthRequest;
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 306
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v2, v8}, Lorg/scribe/model/OAuthRequest;->addQuerystringParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 318
    .end local v0    # "allKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v2    # "key":Ljava/lang/String;
    .restart local v5    # "response":Lorg/scribe/model/Response;
    :cond_4
    const-string v7, "requestSucceeded"

    invoke-virtual {v5}, Lorg/scribe/model/Response;->getBody()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 323
    .end local v4    # "request":Lorg/scribe/model/OAuthRequest;
    .end local v5    # "response":Lorg/scribe/model/Response;
    .end local v6    # "verb":Lorg/scribe/model/Verb;
    .restart local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "message":Ljava/lang/String;
    :cond_5
    const-string v3, "Error performing request"

    goto :goto_2
.end method

.method private showLoginDialog(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/prime31/TwitterPlugin;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const-string v0, "loginSucceeded"

    iget-object v1, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    invoke-virtual {v1}, Lcom/prime31/TwitterSession;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :goto_0
    return-void

    .line 155
    :cond_0
    new-instance v0, Lcom/prime31/TwitterPlugin$1;

    invoke-direct {v0, p0, p1}, Lcom/prime31/TwitterPlugin$1;-><init>(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/prime31/TwitterPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "consumerKey"    # Ljava/lang/String;
    .param p2, "consumerSecret"    # Ljava/lang/String;
    .param p3, "callbackUrlScheme"    # Ljava/lang/String;

    .prologue
    .line 384
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/prime31/TwitterPlugin$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/prime31/TwitterPlugin$3;-><init>(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 390
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 391
    return-void
.end method

.method public isLoggedIn()Z
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_accessToken:Lorg/scribe/model/Token;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public logout()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 458
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_session:Lcom/prime31/TwitterSession;

    invoke-virtual {v0}, Lcom/prime31/TwitterSession;->resetAccessToken()V

    .line 459
    iput-object v1, p0, Lcom/prime31/TwitterPlugin;->_accessToken:Lorg/scribe/model/Token;

    .line 460
    iput-object v1, p0, Lcom/prime31/TwitterPlugin;->_requestToken:Lorg/scribe/model/Token;

    .line 461
    return-void
.end method

.method public performRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "jsonString"    # Ljava/lang/String;

    .prologue
    .line 483
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/prime31/TwitterPlugin$6;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/prime31/TwitterPlugin$6;-><init>(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 513
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 514
    return-void
.end method

.method public postUpdateWithImage(Ljava/lang/String;[B)V
    .locals 2
    .param p1, "update"    # Ljava/lang/String;
    .param p2, "image"    # [B

    .prologue
    .line 466
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/prime31/TwitterPlugin$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/prime31/TwitterPlugin$5;-><init>(Lcom/prime31/TwitterPlugin;[BLjava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 477
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 478
    return-void
.end method

.method public showLoginDialog(Z)V
    .locals 2
    .param p1, "useExternalBrowserForAuthentication"    # Z

    .prologue
    .line 402
    iget-object v0, p0, Lcom/prime31/TwitterPlugin;->_service:Lorg/scribe/oauth/OAuthService;

    if-nez v0, :cond_0

    .line 404
    const-string v0, "Prime31"

    const-string v1, "service is null. We can\'t do anything until init is called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :goto_0
    return-void

    .line 408
    :cond_0
    if-eqz p1, :cond_1

    .line 410
    const-string v0, "Prime31"

    const-string v1, "you have requested for an external browser to be used for authentication so please ensure you have properly setup your intent-filter in your AndroidManifest"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    sget-object v0, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    const-string v1, "twitterplugin"

    if-ne v0, v1, :cond_1

    .line 413
    const-string v0, "Prime31"

    const-string v1, "WARNING: you did not pass in a custom callbackUrlScheme to the init method. It is highly recommended to avoid conflicts that you use a custom callbackUrlScheme."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_1
    const-string v0, "Prime31"

    const-string v1, "spawning new thread to fetch reqeust token"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/prime31/TwitterPlugin$4;

    invoke-direct {v1, p0, p1}, Lcom/prime31/TwitterPlugin$4;-><init>(Lcom/prime31/TwitterPlugin;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 452
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
