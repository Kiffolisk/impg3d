.class Lcom/prime31/FacebookPlugin$5;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin;->reauthorizeWithPublishPermissions(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/FacebookPlugin;

.field private final synthetic val$defaultAudience:Ljava/lang/String;

.field private final synthetic val$permissions:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$5;->this$0:Lcom/prime31/FacebookPlugin;

    iput-object p2, p0, Lcom/prime31/FacebookPlugin$5;->val$permissions:Ljava/util/List;

    iput-object p3, p0, Lcom/prime31/FacebookPlugin$5;->val$defaultAudience:Ljava/lang/String;

    .line 367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 371
    new-instance v0, Lcom/facebook/Session$NewPermissionsRequest;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/prime31/FacebookPlugin$5;->val$permissions:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/facebook/Session$NewPermissionsRequest;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 372
    .local v0, "req":Lcom/facebook/Session$NewPermissionsRequest;
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$5;->val$defaultAudience:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/SessionDefaultAudience;->valueOf(Ljava/lang/String;)Lcom/facebook/SessionDefaultAudience;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/Session$NewPermissionsRequest;->setDefaultAudience(Lcom/facebook/SessionDefaultAudience;)Lcom/facebook/Session$NewPermissionsRequest;

    .line 373
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/Session;->requestNewPublishPermissions(Lcom/facebook/Session$NewPermissionsRequest;)V

    .line 374
    return-void
.end method
