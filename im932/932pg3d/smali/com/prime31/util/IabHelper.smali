.class public Lcom/prime31/util/IabHelper;
.super Ljava/lang/Object;
.source "IabHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;,
        Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;,
        Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;,
        Lcom/prime31/util/IabHelper$OnIabSetupFinishedListener;,
        Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;
    }
.end annotation


# static fields
.field public static final BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE:I = 0x3

.field public static final BILLING_RESPONSE_RESULT_DEVELOPER_ERROR:I = 0x5

.field public static final BILLING_RESPONSE_RESULT_ERROR:I = 0x6

.field public static final BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED:I = 0x7

.field public static final BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED:I = 0x8

.field public static final BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE:I = 0x4

.field public static final BILLING_RESPONSE_RESULT_OK:I = 0x0

.field public static final BILLING_RESPONSE_RESULT_USER_CANCELED:I = 0x1

.field public static final GET_SKU_DETAILS_ITEM_LIST:Ljava/lang/String; = "ITEM_ID_LIST"

.field public static final GET_SKU_DETAILS_ITEM_TYPE_LIST:Ljava/lang/String; = "ITEM_TYPE_LIST"

.field public static final IABHELPER_BAD_RESPONSE:I = -0x3ea

.field public static final IABHELPER_ERROR_BASE:I = -0x3e8

.field public static final IABHELPER_INVALID_CONSUMPTION:I = -0x3f2

.field public static final IABHELPER_MISSING_TOKEN:I = -0x3ef

.field public static final IABHELPER_REMOTE_EXCEPTION:I = -0x3e9

.field public static final IABHELPER_SEND_INTENT_FAILED:I = -0x3ec

.field public static final IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE:I = -0x3f1

.field public static final IABHELPER_UNKNOWN_ERROR:I = -0x3f0

.field public static final IABHELPER_UNKNOWN_PURCHASE_RESPONSE:I = -0x3ee

.field public static final IABHELPER_USER_CANCELLED:I = -0x3ed

.field public static final IABHELPER_VERIFICATION_FAILED:I = -0x3eb

.field public static final INAPP_CONTINUATION_TOKEN:Ljava/lang/String; = "INAPP_CONTINUATION_TOKEN"

.field public static final ITEM_TYPE_INAPP:Ljava/lang/String; = "inapp"

.field public static final ITEM_TYPE_SUBS:Ljava/lang/String; = "subs"

.field public static final RESPONSE_BUY_INTENT:Ljava/lang/String; = "BUY_INTENT"

.field public static final RESPONSE_CODE:Ljava/lang/String; = "RESPONSE_CODE"

.field public static final RESPONSE_GET_SKU_DETAILS_LIST:Ljava/lang/String; = "DETAILS_LIST"

.field public static final RESPONSE_INAPP_ITEM_LIST:Ljava/lang/String; = "INAPP_PURCHASE_ITEM_LIST"

.field public static final RESPONSE_INAPP_PURCHASE_DATA:Ljava/lang/String; = "INAPP_PURCHASE_DATA"

.field public static final RESPONSE_INAPP_PURCHASE_DATA_LIST:Ljava/lang/String; = "INAPP_PURCHASE_DATA_LIST"

.field public static final RESPONSE_INAPP_SIGNATURE:Ljava/lang/String; = "INAPP_DATA_SIGNATURE"

.field public static final RESPONSE_INAPP_SIGNATURE_LIST:Ljava/lang/String; = "INAPP_DATA_SIGNATURE_LIST"

.field static final TAG:Ljava/lang/String; = "Prime31-IABH"

.field public static autoVerifySignatures:Z


# instance fields
.field _asyncInProgress:Z

.field _asyncOperation:Ljava/lang/String;

.field _context:Landroid/content/Context;

.field _disposed:Z

.field _purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

.field _purchasingItemType:Ljava/lang/String;

.field _requestCode:I

.field _service:Lcom/android/vending/billing/IInAppBillingService;

.field _serviceConn:Landroid/content/ServiceConnection;

.field _setupDone:Z

.field _signatureBase64:Ljava/lang/String;

.field _subscriptionsSupported:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    sput-boolean v0, Lcom/prime31/util/IabHelper;->autoVerifySignatures:Z

    .line 170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "base64PublicKey"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-boolean v0, p0, Lcom/prime31/util/IabHelper;->_setupDone:Z

    .line 100
    iput-boolean v0, p0, Lcom/prime31/util/IabHelper;->_subscriptionsSupported:Z

    .line 104
    iput-boolean v0, p0, Lcom/prime31/util/IabHelper;->_disposed:Z

    .line 108
    iput-boolean v0, p0, Lcom/prime31/util/IabHelper;->_asyncInProgress:Z

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/prime31/util/IabHelper;->_asyncOperation:Ljava/lang/String;

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/prime31/util/IabHelper;->_signatureBase64:Ljava/lang/String;

    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    .line 192
    iput-object p2, p0, Lcom/prime31/util/IabHelper;->_signatureBase64:Ljava/lang/String;

    .line 193
    const-string v0, "IAB helper created."

    invoke-static {v0}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method private checkNotDisposed()V
    .locals 2

    .prologue
    .line 352
    iget-boolean v0, p0, Lcom/prime31/util/IabHelper;->_disposed:Z

    if-eqz v0, :cond_0

    .line 353
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IabHelper was disposed of, so it cannot be used."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_0
    return-void
.end method

.method public static getResponseDesc(I)Ljava/lang/String;
    .locals 5
    .param p0, "code"    # I

    .prologue
    .line 911
    const-string v3, "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned"

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 912
    .local v0, "iab_msgs":[Ljava/lang/String;
    const-string v3, "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error-1009:Subscriptions not available/-1010:Invalid consumption attempt"

    .line 913
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 915
    .local v1, "iabhelper_msgs":[Ljava/lang/String;
    const/16 v3, -0x3e8

    if-gt p0, v3, :cond_1

    .line 917
    rsub-int v2, p0, -0x3e8

    .line 918
    .local v2, "index":I
    if-ltz v2, :cond_0

    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 919
    aget-object v3, v1, v2

    .line 926
    .end local v2    # "index":I
    :goto_0
    return-object v3

    .line 921
    .restart local v2    # "index":I
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ":Unknown IAB Helper Error"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 923
    .end local v2    # "index":I
    :cond_1
    if-ltz p0, :cond_2

    array-length v3, v0

    if-lt p0, v3, :cond_3

    .line 924
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ":Unknown"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 926
    :cond_3
    aget-object v3, v0, p0

    goto :goto_0
.end method


# virtual methods
.method checkSetupDone(Ljava/lang/String;)V
    .locals 3
    .param p1, "operation"    # Ljava/lang/String;

    .prologue
    .line 933
    iget-boolean v0, p0, Lcom/prime31/util/IabHelper;->_setupDone:Z

    if-nez v0, :cond_0

    .line 935
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal state for operation ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): IAB helper is not set up."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 936
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IAB helper is not set up. Can\'t perform operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 938
    :cond_0
    return-void
.end method

.method consume(Lcom/prime31/util/Purchase;)V
    .locals 8
    .param p1, "itemInfo"    # Lcom/prime31/util/Purchase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/prime31/util/IabException;
        }
    .end annotation

    .prologue
    .line 791
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 792
    const-string v4, "consume"

    invoke-virtual {p0, v4}, Lcom/prime31/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 794
    iget-object v4, p1, Lcom/prime31/util/Purchase;->_itemType:Ljava/lang/String;

    const-string v5, "inapp"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 795
    new-instance v4, Lcom/prime31/util/IabException;

    const/16 v5, -0x3f2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Items of type \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p1, Lcom/prime31/util/Purchase;->_itemType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' can\'t be consumed."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;)V

    throw v4

    .line 799
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/prime31/util/Purchase;->getToken()Ljava/lang/String;

    move-result-object v3

    .line 800
    .local v3, "token":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/prime31/util/Purchase;->getSku()Ljava/lang/String;

    move-result-object v2

    .line 801
    .local v2, "sku":Ljava/lang/String;
    if-eqz v3, :cond_1

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 803
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Can\'t consume "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". No token."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 804
    new-instance v4, Lcom/prime31/util/IabException;

    const/16 v5, -0x3ef

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PurchaseInfo is missing token for sku: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 819
    .end local v2    # "sku":Ljava/lang/String;
    .end local v3    # "token":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v4, Lcom/prime31/util/IabException;

    const/16 v5, -0x3e9

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Remote exception while consuming. PurchaseInfo: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6, v0}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 807
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v2    # "sku":Ljava/lang/String;
    .restart local v3    # "token":Ljava/lang/String;
    :cond_2
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Consuming sku: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", token: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 808
    iget-object v4, p0, Lcom/prime31/util/IabHelper;->_service:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6, v3}, Lcom/android/vending/billing/IInAppBillingService;->consumePurchase(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 809
    .local v1, "response":I
    if-nez v1, :cond_3

    .line 811
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Successfully consumed sku: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 823
    return-void

    .line 815
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error consuming consuming sku "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Lcom/prime31/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 816
    new-instance v4, Lcom/prime31/util/IabException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error consuming sku "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public consumeAsync(Lcom/prime31/util/Purchase;Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;)V
    .locals 2
    .param p1, "purchase"    # Lcom/prime31/util/Purchase;
    .param p2, "listener"    # Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;

    .prologue
    .line 875
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 876
    const-string v1, "consume"

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 877
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 878
    .local v0, "purchases":Ljava/util/List;, "Ljava/util/List<Lcom/prime31/util/Purchase;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 879
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/prime31/util/IabHelper;->consumeAsyncInternal(Ljava/util/List;Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;)V

    .line 880
    return-void
.end method

.method public consumeAsync(Ljava/util/List;Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/prime31/util/Purchase;",
            ">;",
            "Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 895
    .local p1, "purchases":Ljava/util/List;, "Ljava/util/List<Lcom/prime31/util/Purchase;>;"
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 896
    const-string v0, "consume"

    invoke-virtual {p0, v0}, Lcom/prime31/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 897
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/prime31/util/IabHelper;->consumeAsyncInternal(Ljava/util/List;Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;)V

    .line 898
    return-void
.end method

.method consumeAsyncInternal(Ljava/util/List;Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;)V
    .locals 7
    .param p2, "singleListener"    # Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;
    .param p3, "multiListener"    # Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/prime31/util/Purchase;",
            ">;",
            "Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;",
            "Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1175
    .local p1, "purchases":Ljava/util/List;, "Ljava/util/List<Lcom/prime31/util/Purchase;>;"
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 1176
    .local v4, "handler":Landroid/os/Handler;
    const-string v0, "consume"

    invoke-virtual {p0, v0}, Lcom/prime31/util/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 1177
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/prime31/util/IabHelper$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/prime31/util/IabHelper$3;-><init>(Lcom/prime31/util/IabHelper;Ljava/util/List;Lcom/prime31/util/IabHelper$OnConsumeFinishedListener;Landroid/os/Handler;Lcom/prime31/util/IabHelper$OnConsumeMultiFinishedListener;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1218
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 1219
    return-void
.end method

.method public dispose()V
    .locals 4

    .prologue
    .line 325
    const-string v1, "Disposing."

    invoke-static {v1}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 329
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/prime31/util/IabHelper;->_setupDone:Z

    .line 330
    iget-object v1, p0, Lcom/prime31/util/IabHelper;->_serviceConn:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    .line 332
    const-string v1, "Unbinding from service."

    invoke-static {v1}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 333
    iget-object v1, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    iget-object v2, p0, Lcom/prime31/util/IabHelper;->_serviceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 337
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/prime31/util/IabHelper;->_disposed:Z

    .line 338
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    .line 339
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/prime31/util/IabHelper;->_serviceConn:Landroid/content/ServiceConnection;

    .line 340
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/prime31/util/IabHelper;->_service:Lcom/android/vending/billing/IInAppBillingService;

    .line 341
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :goto_0
    return-void

    .line 343
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Prime31-IABH"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception disposing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method flagEndAsync()V
    .locals 2

    .prologue
    .line 999
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "flagEndAsync"

    invoke-static {v0, v1}, Lcom/prime31/IABConstants;->logEntering(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ending async operation: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/prime31/util/IabHelper;->_asyncOperation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1001
    const-string v0, ""

    iput-object v0, p0, Lcom/prime31/util/IabHelper;->_asyncOperation:Ljava/lang/String;

    .line 1002
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/prime31/util/IabHelper;->_asyncInProgress:Z

    .line 1003
    return-void
.end method

.method flagStartAsync(Ljava/lang/String;)V
    .locals 3
    .param p1, "operation"    # Ljava/lang/String;

    .prologue
    .line 989
    iget-boolean v0, p0, Lcom/prime31/util/IabHelper;->_asyncInProgress:Z

    if-eqz v0, :cond_0

    .line 990
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t start async operation ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") because another async operation("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/prime31/util/IabHelper;->_asyncOperation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is in progress."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 991
    :cond_0
    iput-object p1, p0, Lcom/prime31/util/IabHelper;->_asyncOperation:Ljava/lang/String;

    .line 992
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/prime31/util/IabHelper;->_asyncInProgress:Z

    .line 993
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting async operation: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 994
    return-void
.end method

.method getResponseCodeFromBundle(Landroid/os/Bundle;)I
    .locals 4
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 945
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 946
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 948
    const-string v1, "Bundle with null response code, assuming OK (known issue)"

    invoke-static {v1}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 949
    const/4 v1, 0x0

    .line 954
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 951
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 952
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 953
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 954
    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 957
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2
    const-string v1, "Unexpected type for bundle response code."

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 958
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 959
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type for bundle response code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method getResponseCodeFromIntent(Landroid/content/Intent;)I
    .locals 4
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 968
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 969
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 971
    const-string v1, "Intent with no response code, assuming OK (known issue)"

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 972
    const/4 v1, 0x0

    .line 977
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 974
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 975
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 976
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 977
    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 980
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2
    const-string v1, "Unexpected type for intent response code."

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 981
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 982
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type for intent response code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public handleActivityResult(IILandroid/content/Intent;)Z
    .locals 14
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 513
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "handleActivityResult"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    aput-object p3, v11, v12

    invoke-static {v9, v10, v11}, Lcom/prime31/IABConstants;->logEntering(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 514
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "current _requestCode: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, p0, Lcom/prime31/util/IabHelper;->_requestCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 517
    iget v9, p0, Lcom/prime31/util/IabHelper;->_requestCode:I

    if-eq p1, v9, :cond_0

    .line 518
    const/4 v9, 0x0

    .line 619
    :goto_0
    return v9

    .line 520
    :cond_0
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 521
    const-string v9, "handleActivityResult"

    invoke-virtual {p0, v9}, Lcom/prime31/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 524
    invoke-virtual {p0}, Lcom/prime31/util/IabHelper;->flagEndAsync()V

    .line 526
    if-nez p3, :cond_2

    .line 528
    const-string v9, "Null data in IAB activity result."

    invoke-virtual {p0, v9}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 529
    new-instance v7, Lcom/prime31/util/IabResult;

    const/16 v9, -0x3ea

    const-string v10, "Null data in IAB result"

    invoke-direct {v7, v9, v10}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 530
    .local v7, "result":Lcom/prime31/util/IabResult;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_1

    .line 532
    const-string v9, "calling purchaseListener.onIabPurchaseFinished"

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 533
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 535
    :cond_1
    const/4 v9, 0x1

    goto :goto_0

    .line 538
    .end local v7    # "result":Lcom/prime31/util/IabResult;
    :cond_2
    move-object/from16 v0, p3

    invoke-virtual {p0, v0}, Lcom/prime31/util/IabHelper;->getResponseCodeFromIntent(Landroid/content/Intent;)I

    move-result v6

    .line 539
    .local v6, "responseCode":I
    const-string v9, "INAPP_PURCHASE_DATA"

    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 540
    .local v5, "purchaseData":Ljava/lang/String;
    const-string v9, "INAPP_DATA_SIGNATURE"

    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 542
    .local v1, "dataSignature":Ljava/lang/String;
    const/4 v9, -0x1

    move/from16 v0, p2

    if-ne v0, v9, :cond_b

    if-nez v6, :cond_b

    .line 544
    const-string v9, "Successful resultcode from purchase activity."

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 545
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Purchase data: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 546
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Data signature: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 547
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Extras: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 548
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Expected item type: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/prime31/util/IabHelper;->_purchasingItemType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 550
    if-eqz v5, :cond_3

    if-nez v1, :cond_5

    .line 552
    :cond_3
    const-string v9, "BUG: either purchaseData or dataSignature is null."

    invoke-virtual {p0, v9}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 553
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Extras: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    invoke-virtual {v10}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 554
    new-instance v7, Lcom/prime31/util/IabResult;

    const/16 v9, -0x3f0

    const-string v10, "IAB returned null purchaseData or dataSignature"

    invoke-direct {v7, v9, v10}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 555
    .restart local v7    # "result":Lcom/prime31/util/IabResult;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_4

    .line 556
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 557
    :cond_4
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 560
    .end local v7    # "result":Lcom/prime31/util/IabResult;
    :cond_5
    const/4 v3, 0x0

    .line 563
    .local v3, "purchase":Lcom/prime31/util/Purchase;
    :try_start_0
    new-instance v4, Lcom/prime31/util/Purchase;

    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchasingItemType:Ljava/lang/String;

    invoke-direct {v4, v9, v5, v1}, Lcom/prime31/util/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 564
    .end local v3    # "purchase":Lcom/prime31/util/Purchase;
    .local v4, "purchase":Lcom/prime31/util/Purchase;
    :try_start_1
    invoke-virtual {v4}, Lcom/prime31/util/Purchase;->getSku()Ljava/lang/String;

    move-result-object v8

    .line 567
    .local v8, "sku":Ljava/lang/String;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_6

    .line 568
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    invoke-interface {v9, v5, v1}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseCompleteAwaitingVerification(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    :cond_6
    sget-boolean v9, Lcom/prime31/util/IabHelper;->autoVerifySignatures:Z

    if-eqz v9, :cond_8

    invoke-virtual {v4}, Lcom/prime31/util/Purchase;->isTestSku()Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_signatureBase64:Ljava/lang/String;

    invoke-static {v9, v5, v1}, Lcom/prime31/util/Security;->verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 572
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Purchase signature verification FAILED for sku "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 573
    new-instance v7, Lcom/prime31/util/IabResult;

    const/16 v9, -0x3eb

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Signature verification failed for sku "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v9, v10}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 574
    .restart local v7    # "result":Lcom/prime31/util/IabResult;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_7

    .line 575
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    invoke-interface {v9, v7, v4}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 576
    :cond_7
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 578
    .end local v7    # "result":Lcom/prime31/util/IabResult;
    :cond_8
    const-string v9, "Purchase signature successfully verified."

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 590
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_9

    .line 592
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    new-instance v10, Lcom/prime31/util/IabResult;

    const/4 v11, 0x0

    const-string v12, "Success"

    invoke-direct {v10, v11, v12}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-interface {v9, v10, v4}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 619
    .end local v4    # "purchase":Lcom/prime31/util/Purchase;
    .end local v8    # "sku":Ljava/lang/String;
    :cond_9
    :goto_1
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 580
    .restart local v3    # "purchase":Lcom/prime31/util/Purchase;
    :catch_0
    move-exception v2

    .line 582
    .local v2, "e":Lorg/json/JSONException;
    :goto_2
    const-string v9, "Failed to parse purchase data."

    invoke-virtual {p0, v9}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 583
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 584
    new-instance v7, Lcom/prime31/util/IabResult;

    const/16 v9, -0x3ea

    const-string v10, "Failed to parse purchase data."

    invoke-direct {v7, v9, v10}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 585
    .restart local v7    # "result":Lcom/prime31/util/IabResult;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_a

    .line 586
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 587
    :cond_a
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 595
    .end local v2    # "e":Lorg/json/JSONException;
    .end local v3    # "purchase":Lcom/prime31/util/Purchase;
    .end local v7    # "result":Lcom/prime31/util/IabResult;
    :cond_b
    const/4 v9, -0x1

    move/from16 v0, p2

    if-ne v0, v9, :cond_c

    .line 598
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Result code was OK but in-app billing response was not OK: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/prime31/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 599
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_9

    .line 601
    new-instance v7, Lcom/prime31/util/IabResult;

    const-string v9, "Problem purchashing item."

    invoke-direct {v7, v6, v9}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 602
    .restart local v7    # "result":Lcom/prime31/util/IabResult;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    goto :goto_1

    .line 605
    .end local v7    # "result":Lcom/prime31/util/IabResult;
    :cond_c
    if-nez p2, :cond_d

    .line 607
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Purchase canceled - Response: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/prime31/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 608
    new-instance v7, Lcom/prime31/util/IabResult;

    const/16 v9, -0x3ed

    const-string v10, "User canceled."

    invoke-direct {v7, v9, v10}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 609
    .restart local v7    # "result":Lcom/prime31/util/IabResult;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_9

    .line 610
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    goto :goto_1

    .line 614
    .end local v7    # "result":Lcom/prime31/util/IabResult;
    :cond_d
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Purchase failed. Result code: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ". Response: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v6}, Lcom/prime31/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 615
    new-instance v7, Lcom/prime31/util/IabResult;

    const/16 v9, -0x3ee

    const-string v10, "Unknown purchase response."

    invoke-direct {v7, v9, v10}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 616
    .restart local v7    # "result":Lcom/prime31/util/IabResult;
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v9, :cond_9

    .line 617
    iget-object v9, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    goto/16 :goto_1

    .line 580
    .end local v7    # "result":Lcom/prime31/util/IabResult;
    .restart local v4    # "purchase":Lcom/prime31/util/Purchase;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "purchase":Lcom/prime31/util/Purchase;
    .restart local v3    # "purchase":Lcom/prime31/util/Purchase;
    goto/16 :goto_2
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;)V
    .locals 6
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    .prologue
    .line 389
    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/prime31/util/IabHelper;->launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 390
    return-void
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
    .locals 7
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;
    .param p5, "extraData"    # Ljava/lang/String;

    .prologue
    .line 395
    const-string v3, "inapp"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/prime31/util/IabHelper;->launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)Z

    .line 396
    return-void
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)Z
    .locals 13
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "itemType"    # Ljava/lang/String;
    .param p4, "requestCode"    # I
    .param p5, "listener"    # Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;
    .param p6, "extraData"    # Ljava/lang/String;

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 442
    const-string v1, "launchPurchaseFlow"

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 443
    const-string v1, "launchPurchaseFlow"

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 448
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Constructing buy intent for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", itemType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 449
    move/from16 v0, p4

    iput v0, p0, Lcom/prime31/util/IabHelper;->_requestCode:I

    .line 450
    iget-object v1, p0, Lcom/prime31/util/IabHelper;->_service:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    invoke-interface/range {v1 .. v6}, Lcom/android/vending/billing/IInAppBillingService;->getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 451
    .local v8, "buyIntentBundle":Landroid/os/Bundle;
    invoke-virtual {p0, v8}, Lcom/prime31/util/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v11

    .line 452
    .local v11, "response":I
    if-eqz v11, :cond_1

    .line 454
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to buy item, Error response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v11}, Lcom/prime31/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 456
    new-instance v12, Lcom/prime31/util/IabResult;

    const-string v1, "Unable to buy item"

    invoke-direct {v12, v11, v1}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 457
    .local v12, "result":Lcom/prime31/util/IabResult;
    if-eqz p5, :cond_0

    .line 458
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v12, v1}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 459
    :cond_0
    invoke-virtual {p0}, Lcom/prime31/util/IabHelper;->flagEndAsync()V

    .line 460
    const/4 v1, 0x0

    .line 490
    .end local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v11    # "response":I
    .end local v12    # "result":Lcom/prime31/util/IabResult;
    :goto_0
    return v1

    .line 463
    .restart local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .restart local v11    # "response":I
    :cond_1
    const-string v1, "BUY_INTENT"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/app/PendingIntent;

    .line 464
    .local v10, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Launching buy intent for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 465
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/prime31/util/IabHelper;->_purchaseListener:Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    .line 466
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/prime31/util/IabHelper;->_purchasingItemType:Ljava/lang/String;

    .line 467
    invoke-virtual {v10}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object v1, p1

    move/from16 v3, p4

    invoke-virtual/range {v1 .. v7}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 490
    const/4 v1, 0x1

    goto :goto_0

    .line 469
    .end local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v10    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v11    # "response":I
    :catch_0
    move-exception v9

    .line 471
    .local v9, "e":Landroid/content/IntentSender$SendIntentException;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SendIntentException while launching purchase flow for sku "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 472
    invoke-virtual {v9}, Landroid/content/IntentSender$SendIntentException;->printStackTrace()V

    .line 474
    new-instance v12, Lcom/prime31/util/IabResult;

    const/16 v1, -0x3ec

    const-string v2, "Failed to send intent."

    invoke-direct {v12, v1, v2}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 475
    .restart local v12    # "result":Lcom/prime31/util/IabResult;
    if-eqz p5, :cond_2

    .line 476
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v12, v1}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 477
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 479
    .end local v9    # "e":Landroid/content/IntentSender$SendIntentException;
    .end local v12    # "result":Lcom/prime31/util/IabResult;
    :catch_1
    move-exception v9

    .line 481
    .local v9, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteException while launching purchase flow for sku "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 482
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    .line 484
    new-instance v12, Lcom/prime31/util/IabResult;

    const/16 v1, -0x3e9

    const-string v2, "Remote exception while starting purchase flow"

    invoke-direct {v12, v1, v2}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 485
    .restart local v12    # "result":Lcom/prime31/util/IabResult;
    if-eqz p5, :cond_3

    .line 486
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v12, v1}, Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/prime31/util/IabResult;Lcom/prime31/util/Purchase;)V

    .line 487
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;)V
    .locals 6
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;

    .prologue
    .line 401
    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/prime31/util/IabHelper;->launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 402
    return-void
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
    .locals 7
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;
    .param p5, "extraData"    # Ljava/lang/String;

    .prologue
    .line 407
    const-string v3, "subs"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/prime31/util/IabHelper;->launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/prime31/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)Z

    .line 408
    return-void
.end method

.method logError(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 1224
    const-string v0, "Prime31-IABH"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "In-app billing error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1225
    return-void
.end method

.method logWarn(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 1230
    const-string v0, "Prime31-IABH"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "In-app billing warning: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1231
    return-void
.end method

.method public queryInventory(ZLjava/util/List;)Lcom/prime31/util/Inventory;
    .locals 1
    .param p1, "querySkuDetails"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/prime31/util/Inventory;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/prime31/util/IabException;
        }
    .end annotation

    .prologue
    .line 625
    .local p2, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/prime31/util/IabHelper;->queryInventory(ZLjava/util/List;Ljava/util/List;)Lcom/prime31/util/Inventory;

    move-result-object v0

    return-object v0
.end method

.method public queryInventory(ZLjava/util/List;Ljava/util/List;)Lcom/prime31/util/Inventory;
    .locals 6
    .param p1, "querySkuDetails"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/prime31/util/Inventory;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/prime31/util/IabException;
        }
    .end annotation

    .prologue
    .line 646
    .local p2, "moreItemSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "moreSubsSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 647
    const-string v3, "queryInventory"

    invoke-virtual {p0, v3}, Lcom/prime31/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 650
    :try_start_0
    new-instance v1, Lcom/prime31/util/Inventory;

    invoke-direct {v1}, Lcom/prime31/util/Inventory;-><init>()V

    .line 651
    .local v1, "inv":Lcom/prime31/util/Inventory;
    const-string v3, "inapp"

    invoke-virtual {p0, v1, v3}, Lcom/prime31/util/IabHelper;->queryPurchases(Lcom/prime31/util/Inventory;Ljava/lang/String;)I

    move-result v2

    .line 652
    .local v2, "r":I
    if-eqz v2, :cond_0

    .line 653
    new-instance v3, Lcom/prime31/util/IabException;

    const-string v4, "Error refreshing inventory (querying owned items)."

    invoke-direct {v3, v2, v4}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 679
    .end local v1    # "inv":Lcom/prime31/util/Inventory;
    .end local v2    # "r":I
    :catch_0
    move-exception v0

    .line 681
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v3, Lcom/prime31/util/IabException;

    const/16 v4, -0x3e9

    const-string v5, "Remote exception while refreshing inventory."

    invoke-direct {v3, v4, v5, v0}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 655
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "inv":Lcom/prime31/util/Inventory;
    .restart local v2    # "r":I
    :cond_0
    if-eqz p1, :cond_1

    .line 657
    :try_start_1
    const-string v3, "inapp"

    invoke-virtual {p0, v3, v1, p2}, Lcom/prime31/util/IabHelper;->querySkuDetails(Ljava/lang/String;Lcom/prime31/util/Inventory;Ljava/util/List;)I

    move-result v2

    .line 658
    if-eqz v2, :cond_1

    .line 659
    new-instance v3, Lcom/prime31/util/IabException;

    const-string v4, "Error refreshing inventory (querying prices of items)."

    invoke-direct {v3, v2, v4}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 683
    .end local v1    # "inv":Lcom/prime31/util/Inventory;
    .end local v2    # "r":I
    :catch_1
    move-exception v0

    .line 685
    .local v0, "e":Lorg/json/JSONException;
    new-instance v3, Lcom/prime31/util/IabException;

    const/16 v4, -0x3ea

    const-string v5, "Error parsing JSON response while refreshing inventory."

    invoke-direct {v3, v4, v5, v0}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 663
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "inv":Lcom/prime31/util/Inventory;
    .restart local v2    # "r":I
    :cond_1
    :try_start_2
    iget-boolean v3, p0, Lcom/prime31/util/IabHelper;->_subscriptionsSupported:Z

    if-eqz v3, :cond_3

    .line 665
    const-string v3, "subs"

    invoke-virtual {p0, v1, v3}, Lcom/prime31/util/IabHelper;->queryPurchases(Lcom/prime31/util/Inventory;Ljava/lang/String;)I

    move-result v2

    .line 666
    if-eqz v2, :cond_2

    .line 667
    new-instance v3, Lcom/prime31/util/IabException;

    const-string v4, "Error refreshing inventory (querying owned subscriptions)."

    invoke-direct {v3, v2, v4}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3

    .line 669
    :cond_2
    if-eqz p1, :cond_3

    .line 671
    const-string v3, "subs"

    invoke-virtual {p0, v3, v1, p2}, Lcom/prime31/util/IabHelper;->querySkuDetails(Ljava/lang/String;Lcom/prime31/util/Inventory;Ljava/util/List;)I

    move-result v2

    .line 672
    if-eqz v2, :cond_3

    .line 673
    new-instance v3, Lcom/prime31/util/IabException;

    const-string v4, "Error refreshing inventory (querying prices of subscriptions)."

    invoke-direct {v3, v2, v4}, Lcom/prime31/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 677
    :cond_3
    return-object v1
.end method

.method public queryInventoryAsync(Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;

    .prologue
    .line 768
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/prime31/util/IabHelper;->queryInventoryAsync(ZLjava/util/List;Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;)V

    .line 769
    return-void
.end method

.method public queryInventoryAsync(ZLcom/prime31/util/IabHelper$QueryInventoryFinishedListener;)V
    .locals 1
    .param p1, "querySkuDetails"    # Z
    .param p2, "listener"    # Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;

    .prologue
    .line 774
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/prime31/util/IabHelper;->queryInventoryAsync(ZLjava/util/List;Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;)V

    .line 775
    return-void
.end method

.method public queryInventoryAsync(ZLjava/util/List;Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;)V
    .locals 7
    .param p1, "querySkuDetails"    # Z
    .param p3, "listener"    # Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 722
    .local p2, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    .line 723
    .local v5, "handler":Landroid/os/Handler;
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 724
    const-string v0, "queryInventory"

    invoke-virtual {p0, v0}, Lcom/prime31/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 725
    const-string v0, "refresh inventory"

    invoke-virtual {p0, v0}, Lcom/prime31/util/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 727
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/prime31/util/IabHelper$2;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/prime31/util/IabHelper$2;-><init>(Lcom/prime31/util/IabHelper;ZLjava/util/List;Lcom/prime31/util/IabHelper$QueryInventoryFinishedListener;Landroid/os/Handler;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 762
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 763
    return-void
.end method

.method queryPurchases(Lcom/prime31/util/Inventory;Ljava/lang/String;)I
    .locals 20
    .param p1, "inv"    # Lcom/prime31/util/Inventory;
    .param p2, "itemType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1009
    const-string v17, "Querying owned items..."

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1010
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Package name: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1011
    const/16 v16, 0x0

    .line 1012
    .local v16, "verificationFailed":Z
    const/4 v4, 0x0

    .line 1016
    .local v4, "continueToken":Ljava/lang/String;
    :cond_0
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Calling getPurchases with continuation token: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1017
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/prime31/util/IabHelper;->_service:Lcom/android/vending/billing/IInAppBillingService;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, p2

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/IInAppBillingService;->getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 1019
    .local v6, "ownedItems":Landroid/os/Bundle;
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/prime31/util/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v12

    .line 1020
    .local v12, "response":I
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Owned items response: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1021
    if-eqz v12, :cond_1

    .line 1023
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "getPurchases() failed: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v12}, Lcom/prime31/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1083
    .end local v12    # "response":I
    :goto_0
    return v12

    .line 1026
    .restart local v12    # "response":I
    :cond_1
    const-string v17, "INAPP_PURCHASE_ITEM_LIST"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    const-string v17, "INAPP_PURCHASE_DATA_LIST"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    const-string v17, "INAPP_DATA_SIGNATURE_LIST"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 1028
    :cond_2
    const-string v17, "Bundle returned from getPurchases() doesn\'t contain required fields."

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 1029
    const/16 v12, -0x3ea

    goto :goto_0

    .line 1032
    :cond_3
    const-string v17, "INAPP_PURCHASE_ITEM_LIST"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 1033
    .local v7, "ownedSkus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v17, "INAPP_PURCHASE_DATA_LIST"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 1034
    .local v10, "purchaseDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v17, "INAPP_DATA_SIGNATURE_LIST"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    .line 1036
    .local v14, "signatureList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v5, v0, :cond_4

    .line 1078
    const-string v17, "INAPP_CONTINUATION_TOKEN"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1079
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Continuation token: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1081
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 1083
    if-eqz v16, :cond_9

    const/16 v17, -0x3eb

    :goto_2
    move/from16 v12, v17

    goto :goto_0

    .line 1038
    :cond_4
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1039
    .local v9, "purchaseData":Ljava/lang/String;
    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 1040
    .local v13, "signature":Ljava/lang/String;
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 1044
    .local v15, "sku":Ljava/lang/String;
    const/4 v11, 0x1

    .line 1045
    .local v11, "purchaseIsVerified":Z
    sget-boolean v17, Lcom/prime31/util/IabHelper;->autoVerifySignatures:Z

    if-eqz v17, :cond_5

    .line 1047
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "verified sku: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1048
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/prime31/util/IabHelper;->_signatureBase64:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v9, v13}, Lcom/prime31/util/Security;->verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 1051
    :cond_5
    new-instance v8, Lcom/prime31/util/Purchase;

    move-object/from16 v0, p2

    invoke-direct {v8, v0, v9, v13}, Lcom/prime31/util/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    .local v8, "purchase":Lcom/prime31/util/Purchase;
    invoke-virtual {v8}, Lcom/prime31/util/Purchase;->isTestSku()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1054
    const/4 v11, 0x1

    .line 1055
    const-string v17, "Prime31-IABH"

    const-string v18, "skipping signature verification because this is a test product"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    :cond_6
    if-eqz v11, :cond_8

    .line 1060
    invoke-virtual {v8}, Lcom/prime31/util/Purchase;->getToken()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1062
    const-string v17, "BUG: empty/null token!"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/prime31/util/IabHelper;->logWarn(Ljava/lang/String;)V

    .line 1063
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Purchase data: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1067
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/prime31/util/Inventory;->addPurchase(Lcom/prime31/util/Purchase;)V

    .line 1036
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 1071
    :cond_8
    const-string v17, "Purchase signature verification **FAILED**. Not adding item."

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/prime31/util/IabHelper;->logWarn(Ljava/lang/String;)V

    .line 1072
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "   Purchase data: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1073
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "   Signature: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1074
    const/16 v16, 0x1

    goto :goto_3

    .line 1083
    .end local v8    # "purchase":Lcom/prime31/util/Purchase;
    .end local v9    # "purchaseData":Ljava/lang/String;
    .end local v11    # "purchaseIsVerified":Z
    .end local v13    # "signature":Ljava/lang/String;
    .end local v15    # "sku":Ljava/lang/String;
    :cond_9
    const/16 v17, 0x0

    goto/16 :goto_2
.end method

.method querySkuDetails(Ljava/lang/String;Lcom/prime31/util/Inventory;Ljava/util/List;)I
    .locals 23
    .param p1, "itemType"    # Ljava/lang/String;
    .param p2, "inv"    # Lcom/prime31/util/Inventory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/prime31/util/Inventory;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1089
    .local p3, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v19, "Querying SKU details."

    invoke-static/range {v19 .. v19}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1090
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1091
    .local v15, "skuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/prime31/util/Inventory;->getAllOwnedSkus()Ljava/util/List;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1093
    if-eqz p3, :cond_1

    .line 1095
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_2

    .line 1102
    :cond_1
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-nez v19, :cond_3

    .line 1104
    const-string v19, "queryPrices: nothing to do because there are no SKUs."

    invoke-static/range {v19 .. v19}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1105
    const/4 v10, 0x0

    .line 1169
    :goto_1
    return v10

    .line 1095
    :cond_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 1097
    .local v13, "sku":Ljava/lang/String;
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_0

    .line 1098
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1112
    .end local v13    # "sku":Ljava/lang/String;
    :cond_3
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1114
    .local v8, "packs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v19

    div-int/lit8 v7, v19, 0x14

    .line 1115
    .local v7, "n":I
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v19

    rem-int/lit8 v6, v19, 0x14

    .line 1117
    .local v6, "mod":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-lt v5, v7, :cond_6

    .line 1126
    if-eqz v6, :cond_4

    .line 1128
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1129
    .local v17, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    mul-int/lit8 v19, v7, 0x14

    mul-int/lit8 v20, v7, 0x14

    add-int v20, v20, v6

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_8

    .line 1132
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    .end local v17    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_5
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_9

    .line 1169
    const/4 v10, 0x0

    goto :goto_1

    .line 1119
    :cond_6
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1120
    .restart local v17    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    mul-int/lit8 v19, v5, 0x14

    mul-int/lit8 v20, v5, 0x14

    add-int/lit8 v20, v20, 0x14

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_7

    .line 1123
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1117
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1120
    :cond_7
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 1121
    .local v12, "s":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1129
    .end local v12    # "s":Ljava/lang/String;
    :cond_8
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 1130
    .restart local v12    # "s":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1137
    .end local v12    # "s":Ljava/lang/String;
    .end local v17    # "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_9
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/ArrayList;

    .line 1139
    .local v16, "skuPartList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 1140
    .local v9, "querySkus":Landroid/os/Bundle;
    const-string v20, "ITEM_ID_LIST"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/prime31/util/IabHelper;->_service:Lcom/android/vending/billing/IInAppBillingService;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, p1

    invoke-interface {v0, v1, v2, v3, v9}, Lcom/android/vending/billing/IInAppBillingService;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v14

    .line 1144
    .local v14, "skuDetails":Landroid/os/Bundle;
    const-string v20, "DETAILS_LIST"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_b

    .line 1146
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/prime31/util/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v10

    .line 1147
    .local v10, "response":I
    if-eqz v10, :cond_a

    .line 1149
    const-string v19, "Prime31-IABH"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "getSkuDetails() failed: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v10}, Lcom/prime31/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1154
    :cond_a
    const-string v19, "getSkuDetails() returned a bundle with neither an error nor a detail list."

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/prime31/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 1155
    const/16 v10, -0x3ea

    goto/16 :goto_1

    .line 1160
    .end local v10    # "response":I
    :cond_b
    const-string v20, "DETAILS_LIST"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 1161
    .local v11, "responseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_5
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_5

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 1163
    .local v18, "thisResponse":Ljava/lang/String;
    new-instance v4, Lcom/prime31/util/SkuDetails;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v1}, Lcom/prime31/util/SkuDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    .local v4, "d":Lcom/prime31/util/SkuDetails;
    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Got sku details: "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 1165
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/prime31/util/Inventory;->addSkuDetails(Lcom/prime31/util/SkuDetails;)V

    goto :goto_5
.end method

.method public startSetup(Lcom/prime31/util/IabHelper$OnIabSetupFinishedListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/prime31/util/IabHelper$OnIabSetupFinishedListener;

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/prime31/util/IabHelper;->checkNotDisposed()V

    .line 225
    iget-boolean v1, p0, Lcom/prime31/util/IabHelper;->_setupDone:Z

    if-eqz v1, :cond_0

    .line 226
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "IAB helper is already set up."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 229
    :cond_0
    const-string v1, "Starting in-app billing service"

    invoke-static {v1}, Lcom/prime31/IABConstants;->logDebug(Ljava/lang/String;)V

    .line 230
    new-instance v1, Lcom/prime31/util/IabHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/prime31/util/IabHelper$1;-><init>(Lcom/prime31/util/IabHelper;Lcom/prime31/util/IabHelper$OnIabSetupFinishedListener;)V

    iput-object v1, p0, Lcom/prime31/util/IabHelper;->_serviceConn:Landroid/content/ServiceConnection;

    .line 294
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    iget-object v1, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 299
    iget-object v1, p0, Lcom/prime31/util/IabHelper;->_context:Landroid/content/Context;

    iget-object v2, p0, Lcom/prime31/util/IabHelper;->_serviceConn:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 307
    :cond_1
    :goto_0
    return-void

    .line 304
    :cond_2
    if-eqz p1, :cond_1

    .line 305
    new-instance v1, Lcom/prime31/util/IabResult;

    const/4 v2, 0x3

    const-string v3, "Billing service unavailable on device."

    invoke-direct {v1, v2, v3}, Lcom/prime31/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/prime31/util/IabHelper$OnIabSetupFinishedListener;->onIabSetupFinished(Lcom/prime31/util/IabResult;)V

    goto :goto_0
.end method

.method public subscriptionsSupported()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/prime31/util/IabHelper;->_subscriptionsSupported:Z

    return v0
.end method
