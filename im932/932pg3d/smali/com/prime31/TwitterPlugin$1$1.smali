.class Lcom/prime31/TwitterPlugin$1$1;
.super Ljava/lang/Object;
.source "TwitterPlugin.java"

# interfaces
.implements Lcom/prime31/TwitterDialog$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/TwitterPlugin$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/prime31/TwitterPlugin$1;


# direct methods
.method constructor <init>(Lcom/prime31/TwitterPlugin$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/TwitterPlugin$1$1;->this$1:Lcom/prime31/TwitterPlugin$1;

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;)V
    .locals 12
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 163
    const-string v9, "Prime31"

    const-string v10, "login complete. fetching oauth_verifier"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :try_start_0
    sget-object v9, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    const-string v10, "http"

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 168
    const/4 v2, 0x0

    .line 170
    .local v2, "foundVerifier":Z
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 171
    .local v5, "url":Ljava/net/URL;
    invoke-virtual {v5}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v4

    .line 172
    .local v4, "query":Ljava/lang/String;
    const/4 v7, 0x0

    .line 174
    .local v7, "verifier":Ljava/lang/String;
    const-string v9, "&"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "array":[Ljava/lang/String;
    array-length v9, v0

    :goto_0
    if-lt v8, v9, :cond_0

    .line 189
    :goto_1
    if-eqz v2, :cond_2

    .line 190
    iget-object v8, p0, Lcom/prime31/TwitterPlugin$1$1;->this$1:Lcom/prime31/TwitterPlugin$1;

    invoke-static {v8}, Lcom/prime31/TwitterPlugin$1;->access$0(Lcom/prime31/TwitterPlugin$1;)Lcom/prime31/TwitterPlugin;

    move-result-object v8

    invoke-static {v8, v7}, Lcom/prime31/TwitterPlugin;->access$0(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V

    .line 204
    .end local v0    # "array":[Ljava/lang/String;
    .end local v2    # "foundVerifier":Z
    .end local v4    # "query":Ljava/lang/String;
    .end local v5    # "url":Ljava/net/URL;
    .end local v7    # "verifier":Ljava/lang/String;
    :goto_2
    return-void

    .line 176
    .restart local v0    # "array":[Ljava/lang/String;
    .restart local v2    # "foundVerifier":Z
    .restart local v4    # "query":Ljava/lang/String;
    .restart local v5    # "url":Ljava/net/URL;
    .restart local v7    # "verifier":Ljava/lang/String;
    :cond_0
    aget-object v3, v0, v8

    .line 178
    .local v3, "parameter":Ljava/lang/String;
    const-string v10, "="

    invoke-virtual {v3, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 180
    .local v6, "v":[Ljava/lang/String;
    const/4 v10, 0x0

    aget-object v10, v6, v10

    const-string v11, "UTF-8"

    invoke-static {v10, v11}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "oauth_verifier"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 182
    const/4 v8, 0x1

    aget-object v8, v6, v8

    const-string v9, "UTF-8"

    invoke-static {v8, v9}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 183
    const/4 v2, 0x1

    .line 184
    const-string v8, "Prime31"

    const-string v9, "found verifier"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 194
    .end local v0    # "array":[Ljava/lang/String;
    .end local v2    # "foundVerifier":Z
    .end local v3    # "parameter":Ljava/lang/String;
    .end local v4    # "query":Ljava/lang/String;
    .end local v5    # "url":Ljava/net/URL;
    .end local v6    # "v":[Ljava/lang/String;
    .end local v7    # "verifier":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 197
    iget-object v8, p0, Lcom/prime31/TwitterPlugin$1$1;->this$1:Lcom/prime31/TwitterPlugin$1;

    invoke-static {v8}, Lcom/prime31/TwitterPlugin$1;->access$0(Lcom/prime31/TwitterPlugin$1;)Lcom/prime31/TwitterPlugin;

    move-result-object v8

    const-string v9, "loginFailed"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "malformedURL: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 176
    .end local v1    # "e":Ljava/net/MalformedURLException;
    .restart local v0    # "array":[Ljava/lang/String;
    .restart local v2    # "foundVerifier":Z
    .restart local v3    # "parameter":Ljava/lang/String;
    .restart local v4    # "query":Ljava/lang/String;
    .restart local v5    # "url":Ljava/net/URL;
    .restart local v6    # "v":[Ljava/lang/String;
    .restart local v7    # "verifier":Ljava/lang/String;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 192
    .end local v3    # "parameter":Ljava/lang/String;
    .end local v6    # "v":[Ljava/lang/String;
    :cond_2
    :try_start_1
    iget-object v8, p0, Lcom/prime31/TwitterPlugin$1$1;->this$1:Lcom/prime31/TwitterPlugin$1;

    invoke-static {v8}, Lcom/prime31/TwitterPlugin$1;->access$0(Lcom/prime31/TwitterPlugin$1;)Lcom/prime31/TwitterPlugin;

    move-result-object v8

    const-string v9, "loginFailed"

    const-string v10, "cancelled"

    invoke-virtual {v8, v9, v10}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 199
    .end local v0    # "array":[Ljava/lang/String;
    .end local v2    # "foundVerifier":Z
    .end local v4    # "query":Ljava/lang/String;
    .end local v5    # "url":Ljava/net/URL;
    .end local v7    # "verifier":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 201
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 202
    iget-object v8, p0, Lcom/prime31/TwitterPlugin$1$1;->this$1:Lcom/prime31/TwitterPlugin$1;

    invoke-static {v8}, Lcom/prime31/TwitterPlugin$1;->access$0(Lcom/prime31/TwitterPlugin$1;)Lcom/prime31/TwitterPlugin;

    move-result-object v8

    const-string v9, "loginFailed"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "couldn\'t decode URL: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public onError(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/prime31/TwitterPlugin$1$1;->this$1:Lcom/prime31/TwitterPlugin$1;

    invoke-static {v0}, Lcom/prime31/TwitterPlugin$1;->access$0(Lcom/prime31/TwitterPlugin$1;)Lcom/prime31/TwitterPlugin;

    move-result-object v0

    const-string v1, "loginFailed"

    invoke-virtual {v0, v1, p1}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    return-void
.end method
