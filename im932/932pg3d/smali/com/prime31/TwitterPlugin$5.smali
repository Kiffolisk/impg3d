.class Lcom/prime31/TwitterPlugin$5;
.super Ljava/lang/Object;
.source "TwitterPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/TwitterPlugin;->postUpdateWithImage(Ljava/lang/String;[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/TwitterPlugin;

.field private final synthetic val$image:[B

.field private final synthetic val$update:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/prime31/TwitterPlugin;[BLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/TwitterPlugin$5;->this$0:Lcom/prime31/TwitterPlugin;

    iput-object p2, p0, Lcom/prime31/TwitterPlugin$5;->val$image:[B

    iput-object p3, p0, Lcom/prime31/TwitterPlugin$5;->val$update:Ljava/lang/String;

    .line 466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 470
    const-string v1, "Prime31"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "got image bytes with length: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/prime31/TwitterPlugin$5;->val$image:[B

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 473
    .local v0, "params":Landroid/os/Bundle;
    const-string v1, "status"

    iget-object v2, p0, Lcom/prime31/TwitterPlugin$5;->val$update:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v1, p0, Lcom/prime31/TwitterPlugin$5;->this$0:Lcom/prime31/TwitterPlugin;

    const-string v2, "/1.1/statuses/update_with_media.json"

    iget-object v3, p0, Lcom/prime31/TwitterPlugin$5;->val$image:[B

    invoke-static {v1, v2, v0, v3}, Lcom/prime31/TwitterPlugin;->access$9(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Landroid/os/Bundle;[B)V

    .line 476
    return-void
.end method
