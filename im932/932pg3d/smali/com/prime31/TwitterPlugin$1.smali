.class Lcom/prime31/TwitterPlugin$1;
.super Ljava/lang/Object;
.source "TwitterPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/TwitterPlugin;->showLoginDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/TwitterPlugin;

.field private final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/TwitterPlugin$1;->this$0:Lcom/prime31/TwitterPlugin;

    iput-object p2, p0, Lcom/prime31/TwitterPlugin$1;->val$url:Ljava/lang/String;

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/prime31/TwitterPlugin$1;)Lcom/prime31/TwitterPlugin;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/prime31/TwitterPlugin$1;->this$0:Lcom/prime31/TwitterPlugin;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 159
    new-instance v1, Lcom/prime31/TwitterPlugin$1$1;

    invoke-direct {v1, p0}, Lcom/prime31/TwitterPlugin$1$1;-><init>(Lcom/prime31/TwitterPlugin$1;)V

    .line 213
    .local v1, "listener":Lcom/prime31/TwitterDialog$DialogListener;
    const-string v2, "Prime31"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "prepping login dialog with url: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/prime31/TwitterPlugin$1;->val$url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    new-instance v0, Lcom/prime31/TwitterDialog;

    iget-object v2, p0, Lcom/prime31/TwitterPlugin$1;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-virtual {v2}, Lcom/prime31/TwitterPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/prime31/TwitterPlugin$1;->val$url:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lcom/prime31/TwitterDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/prime31/TwitterDialog$DialogListener;)V

    .line 215
    .local v0, "dialog":Lcom/prime31/TwitterDialog;
    new-instance v2, Lcom/prime31/TwitterPlugin$1$2;

    invoke-direct {v2, p0}, Lcom/prime31/TwitterPlugin$1$2;-><init>(Lcom/prime31/TwitterPlugin$1;)V

    invoke-virtual {v0, v2}, Lcom/prime31/TwitterDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 223
    invoke-virtual {v0}, Lcom/prime31/TwitterDialog;->show()V

    .line 224
    return-void
.end method
