.class Lcom/prime31/TwitterPlugin$4$1;
.super Ljava/lang/Object;
.source "TwitterPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/TwitterPlugin$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/prime31/TwitterPlugin$4;

.field private final synthetic val$authUrl:Ljava/lang/String;

.field private final synthetic val$useExternalBrowserForAuthentication:Z


# direct methods
.method constructor <init>(Lcom/prime31/TwitterPlugin$4;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/TwitterPlugin$4$1;->this$1:Lcom/prime31/TwitterPlugin$4;

    iput-boolean p2, p0, Lcom/prime31/TwitterPlugin$4$1;->val$useExternalBrowserForAuthentication:Z

    iput-object p3, p0, Lcom/prime31/TwitterPlugin$4$1;->val$authUrl:Ljava/lang/String;

    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 433
    iget-boolean v0, p0, Lcom/prime31/TwitterPlugin$4$1;->val$useExternalBrowserForAuthentication:Z

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "Prime31"

    const-string v1, "opening external brwoser for authentication"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    iget-object v0, p0, Lcom/prime31/TwitterPlugin$4$1;->this$1:Lcom/prime31/TwitterPlugin$4;

    invoke-static {v0}, Lcom/prime31/TwitterPlugin$4;->access$0(Lcom/prime31/TwitterPlugin$4;)Lcom/prime31/TwitterPlugin;

    move-result-object v0

    invoke-virtual {v0}, Lcom/prime31/TwitterPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/prime31/TwitterPlugin$4$1;->val$authUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 443
    :goto_0
    return-void

    .line 440
    :cond_0
    const-string v0, "Prime31"

    const-string v1, "showing login dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v0, p0, Lcom/prime31/TwitterPlugin$4$1;->this$1:Lcom/prime31/TwitterPlugin$4;

    invoke-static {v0}, Lcom/prime31/TwitterPlugin$4;->access$0(Lcom/prime31/TwitterPlugin$4;)Lcom/prime31/TwitterPlugin;

    move-result-object v0

    iget-object v1, p0, Lcom/prime31/TwitterPlugin$4$1;->val$authUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/prime31/TwitterPlugin;->access$8(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V

    goto :goto_0
.end method
