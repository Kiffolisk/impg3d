.class Lcom/prime31/TwitterPlugin$2;
.super Ljava/lang/Object;
.source "TwitterPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/TwitterPlugin;->continueLoginWithVerifier(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/TwitterPlugin;

.field private final synthetic val$oauthVerifier:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/prime31/TwitterPlugin;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    iput-object p2, p0, Lcom/prime31/TwitterPlugin$2;->val$oauthVerifier:Ljava/lang/String;

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 235
    const-string v7, "Prime31"

    const-string v8, "continuing login with verifier"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    const/4 v4, 0x0

    .line 240
    .local v4, "response":Lorg/scribe/model/Response;
    :try_start_0
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v7}, Lcom/prime31/TwitterPlugin;->access$1(Lcom/prime31/TwitterPlugin;)Lorg/scribe/oauth/OAuthService;

    move-result-object v7

    if-nez v7, :cond_0

    .line 242
    const-string v7, "Prime31"

    const-string v8, "null service found. this probably means we got jettisoned while in the background. doing blocking init on background thread now"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10}, Lcom/prime31/TwitterPlugin;->access$2(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_0
    new-instance v6, Lorg/scribe/model/Verifier;

    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->val$oauthVerifier:Ljava/lang/String;

    invoke-direct {v6, v7}, Lorg/scribe/model/Verifier;-><init>(Ljava/lang/String;)V

    .line 247
    .local v6, "verifier":Lorg/scribe/model/Verifier;
    const-string v7, "Prime31"

    const-string v8, "Fetching access token"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    iget-object v8, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v8}, Lcom/prime31/TwitterPlugin;->access$1(Lcom/prime31/TwitterPlugin;)Lorg/scribe/oauth/OAuthService;

    move-result-object v8

    iget-object v9, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v9}, Lcom/prime31/TwitterPlugin;->access$3(Lcom/prime31/TwitterPlugin;)Lorg/scribe/model/Token;

    move-result-object v9

    invoke-interface {v8, v9, v6}, Lorg/scribe/oauth/OAuthService;->getAccessToken(Lorg/scribe/model/Token;Lorg/scribe/model/Verifier;)Lorg/scribe/model/Token;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/prime31/TwitterPlugin;->access$4(Lcom/prime31/TwitterPlugin;Lorg/scribe/model/Token;)V

    .line 249
    const-string v7, "Prime31"

    const-string v8, "Got access token. preparing request to verify_credentials"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    new-instance v3, Lorg/scribe/model/OAuthRequest;

    sget-object v7, Lorg/scribe/model/Verb;->GET:Lorg/scribe/model/Verb;

    const-string v8, "https://api.twitter.com/1.1/account/verify_credentials.json"

    invoke-direct {v3, v7, v8}, Lorg/scribe/model/OAuthRequest;-><init>(Lorg/scribe/model/Verb;Ljava/lang/String;)V

    .line 252
    .local v3, "request":Lorg/scribe/model/OAuthRequest;
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v7}, Lcom/prime31/TwitterPlugin;->access$1(Lcom/prime31/TwitterPlugin;)Lorg/scribe/oauth/OAuthService;

    move-result-object v7

    iget-object v8, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v8}, Lcom/prime31/TwitterPlugin;->access$5(Lcom/prime31/TwitterPlugin;)Lorg/scribe/model/Token;

    move-result-object v8

    invoke-interface {v7, v8, v3}, Lorg/scribe/oauth/OAuthService;->signRequest(Lorg/scribe/model/Token;Lorg/scribe/model/OAuthRequest;)V

    .line 253
    invoke-virtual {v3}, Lorg/scribe/model/OAuthRequest;->send()Lorg/scribe/model/Response;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 268
    :try_start_1
    const-string v7, "Prime31"

    const-string v8, "credentials verified. getting body"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    new-instance v2, Lorg/json/JSONObject;

    invoke-virtual {v4}, Lorg/scribe/model/Response;->getBody()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 270
    .local v2, "post":Lorg/json/JSONObject;
    const-string v7, "name"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 271
    .local v5, "username":Ljava/lang/String;
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v7}, Lcom/prime31/TwitterPlugin;->access$6(Lcom/prime31/TwitterPlugin;)Lcom/prime31/TwitterSession;

    move-result-object v7

    iget-object v8, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v8}, Lcom/prime31/TwitterPlugin;->access$5(Lcom/prime31/TwitterPlugin;)Lorg/scribe/model/Token;

    move-result-object v8

    invoke-virtual {v7, v8, v5}, Lcom/prime31/TwitterSession;->storeAccessToken(Lorg/scribe/model/Token;Ljava/lang/String;)V

    .line 272
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    const-string v8, "loginSucceeded"

    invoke-virtual {v7, v8, v5}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 278
    .end local v2    # "post":Lorg/json/JSONObject;
    .end local v3    # "request":Lorg/scribe/model/OAuthRequest;
    .end local v5    # "username":Ljava/lang/String;
    .end local v6    # "verifier":Lorg/scribe/model/Verifier;
    :goto_0
    return-void

    .line 255
    :catch_0
    move-exception v1

    .line 257
    .local v1, "iae":Ljava/lang/IllegalArgumentException;
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    const-string v8, "loginFailed"

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    .end local v1    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    const-string v8, "loginFailed"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 274
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "request":Lorg/scribe/model/OAuthRequest;
    .restart local v6    # "verifier":Lorg/scribe/model/Verifier;
    :catch_2
    move-exception v0

    .line 276
    .local v0, "e":Lorg/json/JSONException;
    iget-object v7, p0, Lcom/prime31/TwitterPlugin$2;->this$0:Lcom/prime31/TwitterPlugin;

    const-string v8, "loginFailed"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
