.class Lcom/prime31/TwitterPlugin$4;
.super Ljava/lang/Object;
.source "TwitterPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/TwitterPlugin;->showLoginDialog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/TwitterPlugin;

.field private final synthetic val$useExternalBrowserForAuthentication:Z


# direct methods
.method constructor <init>(Lcom/prime31/TwitterPlugin;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    iput-boolean p2, p0, Lcom/prime31/TwitterPlugin$4;->val$useExternalBrowserForAuthentication:Z

    .line 417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/prime31/TwitterPlugin$4;)Lcom/prime31/TwitterPlugin;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 423
    :try_start_0
    iget-object v2, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    iget-object v3, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v3}, Lcom/prime31/TwitterPlugin;->access$1(Lcom/prime31/TwitterPlugin;)Lorg/scribe/oauth/OAuthService;

    move-result-object v3

    invoke-interface {v3}, Lorg/scribe/oauth/OAuthService;->getRequestToken()Lorg/scribe/model/Token;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/prime31/TwitterPlugin;->access$7(Lcom/prime31/TwitterPlugin;Lorg/scribe/model/Token;)V

    .line 424
    const-string v2, "Prime31"

    const-string v3, "received reqeust token"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v2, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v2}, Lcom/prime31/TwitterPlugin;->access$1(Lcom/prime31/TwitterPlugin;)Lorg/scribe/oauth/OAuthService;

    move-result-object v2

    iget-object v3, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    invoke-static {v3}, Lcom/prime31/TwitterPlugin;->access$3(Lcom/prime31/TwitterPlugin;)Lorg/scribe/model/Token;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/scribe/oauth/OAuthService;->getAuthorizationUrl(Lorg/scribe/model/Token;)Ljava/lang/String;

    move-result-object v0

    .line 428
    .local v0, "authUrl":Ljava/lang/String;
    iget-object v2, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    new-instance v3, Lcom/prime31/TwitterPlugin$4$1;

    iget-boolean v4, p0, Lcom/prime31/TwitterPlugin$4;->val$useExternalBrowserForAuthentication:Z

    invoke-direct {v3, p0, v4, v0}, Lcom/prime31/TwitterPlugin$4$1;-><init>(Lcom/prime31/TwitterPlugin$4;ZLjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/prime31/TwitterPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    .end local v0    # "authUrl":Ljava/lang/String;
    :goto_0
    return-void

    .line 446
    :catch_0
    move-exception v1

    .line 448
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/prime31/TwitterPlugin$4;->this$0:Lcom/prime31/TwitterPlugin;

    const-string v3, "loginFailed"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/prime31/TwitterPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v2, "Prime31"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error getting token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
