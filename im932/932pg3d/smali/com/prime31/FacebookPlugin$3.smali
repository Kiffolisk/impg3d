.class Lcom/prime31/FacebookPlugin$3;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin;->loginWithPublishPermissions([Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/FacebookPlugin;

.field private final synthetic val$permissions:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$3;->this$0:Lcom/prime31/FacebookPlugin;

    iput-object p2, p0, Lcom/prime31/FacebookPlugin$3;->val$permissions:[Ljava/lang/String;

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 234
    iget-object v4, p0, Lcom/prime31/FacebookPlugin$3;->val$permissions:[Ljava/lang/String;

    array-length v4, v4

    new-array v2, v4, [Ljava/lang/String;

    .line 235
    .local v2, "permissionsStringArray":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/prime31/FacebookPlugin$3;->val$permissions:[Ljava/lang/String;

    array-length v4, v4

    if-lt v0, v4, :cond_1

    .line 240
    new-instance v1, Lcom/facebook/Session$OpenRequest;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/facebook/Session$OpenRequest;-><init>(Landroid/app/Activity;)V

    .line 241
    .local v1, "openRequest":Lcom/facebook/Session$OpenRequest;
    iget-object v4, p0, Lcom/prime31/FacebookPlugin$3;->this$0:Lcom/prime31/FacebookPlugin;

    invoke-static {v4}, Lcom/prime31/FacebookPlugin;->access$0(Lcom/prime31/FacebookPlugin;)Lcom/facebook/SessionLoginBehavior;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/Session$OpenRequest;->setLoginBehavior(Lcom/facebook/SessionLoginBehavior;)Lcom/facebook/Session$OpenRequest;

    .line 242
    invoke-virtual {v1, v2}, Lcom/facebook/Session$OpenRequest;->setPermissions([Ljava/lang/String;)Lcom/facebook/Session$OpenRequest;

    .line 243
    invoke-static {}, Lcom/prime31/FacebookPlugin;->instance()Lcom/prime31/FacebookPlugin;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/Session$OpenRequest;->setCallback(Lcom/facebook/Session$StatusCallback;)Lcom/facebook/Session$OpenRequest;

    .line 246
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    if-nez v4, :cond_0

    .line 248
    new-instance v4, Lcom/facebook/Session$Builder;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/Session$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lcom/facebook/Session$Builder;->build()Lcom/facebook/Session;

    move-result-object v3

    .line 249
    .local v3, "session":Lcom/facebook/Session;
    invoke-static {v3}, Lcom/facebook/Session;->setActiveSession(Lcom/facebook/Session;)V

    .line 252
    .end local v3    # "session":Lcom/facebook/Session;
    :cond_0
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/facebook/Session;->openForPublish(Lcom/facebook/Session$OpenRequest;)V

    .line 253
    return-void

    .line 236
    .end local v1    # "openRequest":Lcom/facebook/Session$OpenRequest;
    :cond_1
    iget-object v4, p0, Lcom/prime31/FacebookPlugin$3;->val$permissions:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
