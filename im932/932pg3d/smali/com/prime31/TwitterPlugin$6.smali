.class Lcom/prime31/TwitterPlugin$6;
.super Ljava/lang/Object;
.source "TwitterPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/TwitterPlugin;->performRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/TwitterPlugin;

.field private final synthetic val$jsonString:Ljava/lang/String;

.field private final synthetic val$path:Ljava/lang/String;

.field private final synthetic val$type:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/TwitterPlugin$6;->this$0:Lcom/prime31/TwitterPlugin;

    iput-object p2, p0, Lcom/prime31/TwitterPlugin$6;->val$jsonString:Ljava/lang/String;

    iput-object p3, p0, Lcom/prime31/TwitterPlugin$6;->val$type:Ljava/lang/String;

    iput-object p4, p0, Lcom/prime31/TwitterPlugin$6;->val$path:Ljava/lang/String;

    .line 483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 489
    const/4 v0, 0x0

    .line 492
    .local v0, "bundle":Landroid/os/Bundle;
    :try_start_0
    iget-object v6, p0, Lcom/prime31/TwitterPlugin$6;->val$jsonString:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/prime31/TwitterPlugin$6;->val$jsonString:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 494
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 495
    .end local v0    # "bundle":Landroid/os/Bundle;
    .local v1, "bundle":Landroid/os/Bundle;
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    iget-object v6, p0, Lcom/prime31/TwitterPlugin$6;->val$jsonString:Ljava/lang/String;

    invoke-direct {v4, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 496
    .local v4, "json":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 498
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v6

    if-nez v6, :cond_1

    move-object v0, v1

    .line 506
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v4    # "json":Lorg/json/JSONObject;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :try_start_2
    iget-object v6, p0, Lcom/prime31/TwitterPlugin$6;->this$0:Lcom/prime31/TwitterPlugin;

    iget-object v7, p0, Lcom/prime31/TwitterPlugin$6;->val$type:Ljava/lang/String;

    iget-object v8, p0, Lcom/prime31/TwitterPlugin$6;->val$path:Ljava/lang/String;

    invoke-static {v6, v7, v8, v0}, Lcom/prime31/TwitterPlugin;->access$10(Lcom/prime31/TwitterPlugin;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 512
    :goto_1
    return-void

    .line 500
    .end local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v4    # "json":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 502
    .local v5, "key":Ljava/lang/String;
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 508
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v4    # "json":Lorg/json/JSONObject;
    .end local v5    # "key":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 510
    .end local v1    # "bundle":Landroid/os/Bundle;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .local v2, "e":Lorg/json/JSONException;
    :goto_2
    const-string v6, "Prime31"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "error decoding request data: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 508
    .end local v2    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v2

    goto :goto_2
.end method
