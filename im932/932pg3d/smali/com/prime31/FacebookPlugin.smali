.class public Lcom/prime31/FacebookPlugin;
.super Lcom/prime31/FacebookPluginBase;
.source "FacebookPlugin.java"

# interfaces
.implements Lcom/facebook/Session$StatusCallback;
.implements Lcom/facebook/widget/WebDialog$OnCompleteListener;
.implements Lcom/facebook/widget/FacebookDialog$Callback;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field private static _appLaunchUrl:Ljava/lang/String;

.field private static _uiHelperInstance:Lcom/facebook/UiLifecycleHelper;


# instance fields
.field private _loginBehavior:Lcom/facebook/SessionLoginBehavior;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, ""

    sput-object v0, Lcom/prime31/FacebookPlugin;->_appLaunchUrl:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/prime31/FacebookPluginBase;-><init>()V

    .line 38
    sget-object v0, Lcom/facebook/SessionLoginBehavior;->SSO_WITH_FALLBACK:Lcom/facebook/SessionLoginBehavior;

    iput-object v0, p0, Lcom/prime31/FacebookPlugin;->_loginBehavior:Lcom/facebook/SessionLoginBehavior;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/prime31/FacebookPlugin;)Lcom/facebook/SessionLoginBehavior;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/prime31/FacebookPlugin;->_loginBehavior:Lcom/facebook/SessionLoginBehavior;

    return-object v0
.end method

.method static synthetic access$1()Lcom/facebook/UiLifecycleHelper;
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v0

    return-object v0
.end method

.method public static onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p0, "requestCode"    # I
    .param p1, "responseCode"    # I
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 100
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v0

    invoke-static {}, Lcom/prime31/FacebookPlugin;->instance()Lcom/prime31/FacebookPlugin;

    move-result-object v1

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/facebook/UiLifecycleHelper;->onActivityResult(IILandroid/content/Intent;Lcom/facebook/widget/FacebookDialog$Callback;)V

    .line 101
    return-void
.end method

.method public static onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p0, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v0

    .line 61
    .local v0, "helper":Lcom/facebook/UiLifecycleHelper;
    invoke-virtual {v0, p0}, Lcom/facebook/UiLifecycleHelper;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 64
    .local v1, "targetUri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 66
    const-string v2, "Prime31"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "incoming deep link from Facebook: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/prime31/FacebookPlugin;->_appLaunchUrl:Ljava/lang/String;

    .line 69
    :cond_0
    return-void
.end method

.method public static onDestroy()V
    .locals 1

    .prologue
    .line 124
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/UiLifecycleHelper;->onDestroy()V

    .line 125
    return-void
.end method

.method public static onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 89
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 90
    .local v0, "targetUri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 92
    const-string v1, "Prime31"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "incoming deep link from Facebook: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/prime31/FacebookPlugin;->_appLaunchUrl:Ljava/lang/String;

    .line 95
    :cond_0
    return-void
.end method

.method public static onPause()V
    .locals 1

    .prologue
    .line 112
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/UiLifecycleHelper;->onPause()V

    .line 113
    return-void
.end method

.method public static onResume()V
    .locals 4

    .prologue
    .line 74
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/UiLifecycleHelper;->onResume()V

    .line 78
    :try_start_0
    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/AppEventsLogger;->activateApp(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 80
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 82
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "Prime31"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AppEventsLogger failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p0, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 106
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/facebook/UiLifecycleHelper;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 107
    return-void
.end method

.method public static onStop()V
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/prime31/FacebookPlugin;->uiHelper()Lcom/facebook/UiLifecycleHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/UiLifecycleHelper;->onStop()V

    .line 119
    return-void
.end method

.method private printKeyHash()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 136
    :try_start_0
    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 v6, 0x40

    invoke-virtual {v5, v2, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 139
    .local v0, "info":Landroid/content/pm/PackageInfo;
    iget-object v5, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v6, v5

    :goto_0
    if-lt v4, v6, :cond_0

    .line 152
    .end local v0    # "info":Landroid/content/pm/PackageInfo;
    .end local v2    # "packageName":Ljava/lang/String;
    :goto_1
    return-void

    .line 139
    .restart local v0    # "info":Landroid/content/pm/PackageInfo;
    .restart local v2    # "packageName":Ljava/lang/String;
    :cond_0
    aget-object v3, v5, v4

    .line 141
    .local v3, "signature":Landroid/content/pm/Signature;
    const-string v7, "SHA"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 142
    .local v1, "md":Ljava/security/MessageDigest;
    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/security/MessageDigest;->update([B)V

    .line 143
    const-string v7, "Prime31"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "your keyhash for this specific apk is: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 150
    .end local v0    # "info":Landroid/content/pm/PackageInfo;
    .end local v1    # "md":Ljava/security/MessageDigest;
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "signature":Landroid/content/pm/Signature;
    :catch_0
    move-exception v4

    goto :goto_1

    .line 148
    :catch_1
    move-exception v4

    goto :goto_1

    .line 146
    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method private static uiHelper()Lcom/facebook/UiLifecycleHelper;
    .locals 3

    .prologue
    .line 44
    sget-object v0, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    if-nez v0, :cond_0

    .line 47
    invoke-static {}, Lcom/prime31/FacebookPlugin;->instance()Lcom/prime31/FacebookPlugin;

    .line 48
    new-instance v0, Lcom/facebook/UiLifecycleHelper;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {}, Lcom/prime31/FacebookPlugin;->instance()Lcom/prime31/FacebookPlugin;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/UiLifecycleHelper;-><init>(Landroid/app/Activity;Lcom/facebook/Session$StatusCallback;)V

    sput-object v0, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    .line 51
    :cond_0
    sget-object v0, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    return-object v0
.end method


# virtual methods
.method public call(Lcom/facebook/Session;Lcom/facebook/SessionState;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "session"    # Lcom/facebook/Session;
    .param p2, "state"    # Lcom/facebook/SessionState;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 603
    new-instance v0, Lcom/prime31/FacebookPlugin$9;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/prime31/FacebookPlugin$9;-><init>(Lcom/prime31/FacebookPlugin;Lcom/facebook/SessionState;Ljava/lang/Exception;Lcom/facebook/Session;)V

    invoke-virtual {p0, v0}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    .line 632
    return-void
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 285
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/Session;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    .line 286
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getAppLaunchUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    sget-object v0, Lcom/prime31/FacebookPlugin;->_appLaunchUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionPermissions()Ljava/lang/String;
    .locals 6

    .prologue
    .line 292
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v3

    if-nez v3, :cond_0

    .line 293
    const-string v3, "[]"

    .line 308
    :goto_0
    return-object v3

    .line 297
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 298
    .local v1, "json":Lorg/json/JSONArray;
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/Session;->getPermissions()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 301
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 298
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 299
    .local v2, "p":Ljava/lang/String;
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 303
    .end local v1    # "json":Lorg/json/JSONArray;
    .end local v2    # "p":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Prime31"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "error creating JSON from permissions: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const-string v3, "[]"

    goto :goto_0
.end method

.method public graphRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "graphPath"    # Ljava/lang/String;
    .param p2, "httpMethod"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 497
    invoke-virtual {p0, p3}, Lcom/prime31/FacebookPlugin;->bundleFromJSON(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 499
    .local v0, "parameters":Landroid/os/Bundle;
    new-instance v1, Lcom/prime31/FacebookPlugin$8;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/prime31/FacebookPlugin$8;-><init>(Lcom/prime31/FacebookPlugin;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    .line 518
    return-void
.end method

.method public init(Z)V
    .locals 1
    .param p1, "printKeyHash"    # Z

    .prologue
    .line 173
    if-eqz p1, :cond_0

    .line 174
    invoke-direct {p0}, Lcom/prime31/FacebookPlugin;->printKeyHash()V

    .line 176
    :cond_0
    new-instance v0, Lcom/prime31/FacebookPlugin$1;

    invoke-direct {v0, p0}, Lcom/prime31/FacebookPlugin$1;-><init>(Lcom/prime31/FacebookPlugin;)V

    .line 187
    invoke-virtual {v0}, Lcom/prime31/FacebookPlugin$1;->start()V

    .line 188
    return-void
.end method

.method public isSessionValid()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 271
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 272
    const-string v1, "Prime31"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "session state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :goto_0
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 277
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v1

    sget-object v2, Lcom/facebook/SessionState;->OPENED:Lcom/facebook/SessionState;

    if-eq v1, v2, :cond_2

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v1

    sget-object v2, Lcom/facebook/SessionState;->OPENED_TOKEN_UPDATED:Lcom/facebook/SessionState;

    if-eq v1, v2, :cond_2

    .line 278
    :cond_0
    :goto_1
    return v0

    .line 274
    :cond_1
    const-string v1, "Prime31"

    const-string v2, "session is null so it has not yet been started"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 277
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public logEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;

    .prologue
    .line 546
    sget-object v1, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v1}, Lcom/facebook/UiLifecycleHelper;->getAppEventsLogger()Lcom/facebook/AppEventsLogger;

    move-result-object v0

    .line 547
    .local v0, "logger":Lcom/facebook/AppEventsLogger;
    if-nez v0, :cond_0

    .line 549
    const-string v1, "Prime31"

    const-string v2, "null logger. Most likely this means a session is not currently active."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    :goto_0
    return-void

    .line 553
    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/AppEventsLogger;->logEvent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public logEventAndValueToSum(Ljava/lang/String;D)V
    .locals 4
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "valueToSum"    # D

    .prologue
    .line 572
    sget-object v1, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v1}, Lcom/facebook/UiLifecycleHelper;->getAppEventsLogger()Lcom/facebook/AppEventsLogger;

    move-result-object v0

    .line 573
    .local v0, "logger":Lcom/facebook/AppEventsLogger;
    if-nez v0, :cond_0

    .line 575
    const-string v1, "Prime31"

    const-string v2, "null logger. Most likely this means a session is not currently active."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :goto_0
    return-void

    .line 579
    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/AppEventsLogger;->logEvent(Ljava/lang/String;D)V

    goto :goto_0
.end method

.method public logEventAndValueToSumWithParameters(Ljava/lang/String;DLjava/lang/String;)V
    .locals 4
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "valueToSum"    # D
    .param p4, "json"    # Ljava/lang/String;

    .prologue
    .line 585
    sget-object v1, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v1}, Lcom/facebook/UiLifecycleHelper;->getAppEventsLogger()Lcom/facebook/AppEventsLogger;

    move-result-object v0

    .line 586
    .local v0, "logger":Lcom/facebook/AppEventsLogger;
    if-nez v0, :cond_0

    .line 588
    const-string v1, "Prime31"

    const-string v2, "null logger. Most likely this means a session is not currently active."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    :goto_0
    return-void

    .line 592
    :cond_0
    invoke-virtual {p0, p4}, Lcom/prime31/FacebookPlugin;->bundleFromJSON(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/facebook/AppEventsLogger;->logEvent(Ljava/lang/String;DLandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public logEventWithParameters(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 559
    sget-object v1, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v1}, Lcom/facebook/UiLifecycleHelper;->getAppEventsLogger()Lcom/facebook/AppEventsLogger;

    move-result-object v0

    .line 560
    .local v0, "logger":Lcom/facebook/AppEventsLogger;
    if-nez v0, :cond_0

    .line 562
    const-string v1, "Prime31"

    const-string v2, "null logger. Most likely this means a session is not currently active."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :goto_0
    return-void

    .line 566
    :cond_0
    invoke-virtual {p0, p2}, Lcom/prime31/FacebookPlugin;->bundleFromJSON(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/AppEventsLogger;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public loginWithPublishPermissions([Ljava/lang/String;)V
    .locals 1
    .param p1, "permissions"    # [Ljava/lang/String;

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/prime31/FacebookPlugin;->logout()V

    .line 229
    new-instance v0, Lcom/prime31/FacebookPlugin$3;

    invoke-direct {v0, p0, p1}, Lcom/prime31/FacebookPlugin$3;-><init>(Lcom/prime31/FacebookPlugin;[Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    .line 255
    return-void
.end method

.method public loginWithReadPermissions([Ljava/lang/String;)V
    .locals 1
    .param p1, "permissions"    # [Ljava/lang/String;

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/prime31/FacebookPlugin;->logout()V

    .line 196
    new-instance v0, Lcom/prime31/FacebookPlugin$2;

    invoke-direct {v0, p0, p1}, Lcom/prime31/FacebookPlugin$2;-><init>(Lcom/prime31/FacebookPlugin;[Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    .line 221
    return-void
.end method

.method public logout()V
    .locals 3

    .prologue
    .line 260
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/Session;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    const-string v0, "Prime31"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "logging out. session state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/Session;->getState()Lcom/facebook/SessionState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/Session;->closeAndClearTokenInformation()V

    .line 264
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/facebook/Session;->setActiveSession(Lcom/facebook/Session;)V

    .line 266
    :cond_0
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;Lcom/facebook/FacebookException;)V
    .locals 2
    .param p1, "values"    # Landroid/os/Bundle;
    .param p2, "error"    # Lcom/facebook/FacebookException;

    .prologue
    .line 642
    if-nez p2, :cond_0

    .line 643
    const-string v0, "dialogCompletedWithUrl"

    invoke-static {p1}, Lcom/facebook/android/Util;->encodeUrl(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    :goto_0
    return-void

    .line 645
    :cond_0
    const-string v0, "dialogFailedWithError"

    invoke-virtual {p2}, Lcom/facebook/FacebookException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onComplete(Lcom/facebook/widget/FacebookDialog$PendingCall;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "pendingCall"    # Lcom/facebook/widget/FacebookDialog$PendingCall;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 657
    const-string v0, "shareDialogSucceeded"

    invoke-virtual {p0, p2}, Lcom/prime31/FacebookPlugin;->bundleToJSON(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    const-string v0, "Prime31"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onComplete PendingCall Callback called with Bundle: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    return-void
.end method

.method public onError(Lcom/facebook/widget/FacebookDialog$PendingCall;Ljava/lang/Exception;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "pendingCall"    # Lcom/facebook/widget/FacebookDialog$PendingCall;
    .param p2, "error"    # Ljava/lang/Exception;
    .param p3, "data"    # Landroid/os/Bundle;

    .prologue
    .line 665
    const-string v0, "shareDialogFailed"

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const-string v0, "Prime31"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onError PendingCall Callback called with Bundle: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    return-void
.end method

.method public reauthorizeWithPublishPermissions(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "permissionsJson"    # Ljava/lang/String;
    .param p2, "defaultAudience"    # Ljava/lang/String;

    .prologue
    .line 347
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/Session;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 349
    :cond_0
    const-string v4, "Prime31"

    const-string v5, "Attempting to reauth when there is no active session. Aborting"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :goto_0
    return-void

    .line 353
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 357
    .local v3, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 358
    .local v2, "json":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-lt v1, v4, :cond_2

    .line 367
    new-instance v4, Lcom/prime31/FacebookPlugin$5;

    invoke-direct {v4, p0, v3, p2}, Lcom/prime31/FacebookPlugin$5;-><init>(Lcom/prime31/FacebookPlugin;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 359
    :cond_2
    :try_start_1
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 358
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 361
    .end local v1    # "i":I
    .end local v2    # "json":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Prime31"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bailing due to error sending permissions: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public reauthorizeWithReadPermissions(Ljava/lang/String;)V
    .locals 7
    .param p1, "permissionsJson"    # Ljava/lang/String;

    .prologue
    .line 314
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/Session;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 316
    :cond_0
    const-string v4, "Prime31"

    const-string v5, "Attempting to reauth when there is no active session. Aborting"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :goto_0
    return-void

    .line 320
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 324
    .local v3, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 325
    .local v2, "json":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-lt v1, v4, :cond_2

    .line 334
    new-instance v4, Lcom/prime31/FacebookPlugin$4;

    invoke-direct {v4, p0, v3}, Lcom/prime31/FacebookPlugin$4;-><init>(Lcom/prime31/FacebookPlugin;Ljava/util/List;)V

    invoke-virtual {p0, v4}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 326
    :cond_2
    :try_start_1
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 325
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 328
    .end local v1    # "i":I
    .end local v2    # "json":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Prime31"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bailing due to error sending permissions: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public restRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "httpMethod"    # Ljava/lang/String;
    .param p2, "restMethod"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 523
    const-string v0, "Prime31"

    const-string v1, "Facebook REST requests are no longer supported by Facebook since the Graph API 2.1 was released"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    return-void
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 3
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 533
    sget-object v1, Lcom/prime31/FacebookPlugin;->_uiHelperInstance:Lcom/facebook/UiLifecycleHelper;

    invoke-virtual {v1}, Lcom/facebook/UiLifecycleHelper;->getAppEventsLogger()Lcom/facebook/AppEventsLogger;

    move-result-object v0

    .line 534
    .local v0, "logger":Lcom/facebook/AppEventsLogger;
    if-nez v0, :cond_0

    .line 536
    const-string v1, "Prime31"

    const-string v2, "null logger. Most likely this means a session is not currently active."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :goto_0
    return-void

    .line 540
    :cond_0
    invoke-static {p1}, Lcom/facebook/Settings;->setAppVersion(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setSessionLoginBehavior(Ljava/lang/String;)V
    .locals 1
    .param p1, "loginBehavior"    # Ljava/lang/String;

    .prologue
    .line 161
    invoke-static {p1}, Lcom/facebook/SessionLoginBehavior;->valueOf(Ljava/lang/String;)Lcom/facebook/SessionLoginBehavior;

    move-result-object v0

    iput-object v0, p0, Lcom/prime31/FacebookPlugin;->_loginBehavior:Lcom/facebook/SessionLoginBehavior;

    .line 162
    return-void
.end method

.method public showDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "dialogType"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 460
    new-instance v0, Lcom/prime31/FacebookPlugin$7;

    invoke-direct {v0, p0, p2, p1}, Lcom/prime31/FacebookPlugin$7;-><init>(Lcom/prime31/FacebookPlugin;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    .line 492
    return-void
.end method

.method public showFacebookShareDialog(Ljava/lang/String;)V
    .locals 1
    .param p1, "jsonString"    # Ljava/lang/String;

    .prologue
    .line 381
    new-instance v0, Lcom/prime31/FacebookPlugin$6;

    invoke-direct {v0, p0, p1}, Lcom/prime31/FacebookPlugin$6;-><init>(Lcom/prime31/FacebookPlugin;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/prime31/FacebookPlugin;->runSafelyOnUiThread(Ljava/lang/Runnable;)V

    .line 455
    return-void
.end method
