.class Lcom/prime31/FacebookPlugin$4;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin;->reauthorizeWithReadPermissions(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/FacebookPlugin;

.field private final synthetic val$permissions:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$4;->this$0:Lcom/prime31/FacebookPlugin;

    iput-object p2, p0, Lcom/prime31/FacebookPlugin$4;->val$permissions:Ljava/util/List;

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 338
    new-instance v0, Lcom/facebook/Session$NewPermissionsRequest;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/prime31/FacebookPlugin$4;->val$permissions:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/facebook/Session$NewPermissionsRequest;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 339
    .local v0, "req":Lcom/facebook/Session$NewPermissionsRequest;
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/Session;->requestNewReadPermissions(Lcom/facebook/Session$NewPermissionsRequest;)V

    .line 340
    return-void
.end method
