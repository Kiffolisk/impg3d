.class Lcom/prime31/FacebookPlugin$8;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin;->graphRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/FacebookPlugin;

.field private final synthetic val$graphPath:Ljava/lang/String;

.field private final synthetic val$httpMethod:Ljava/lang/String;

.field private final synthetic val$parameters:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$8;->this$0:Lcom/prime31/FacebookPlugin;

    iput-object p2, p0, Lcom/prime31/FacebookPlugin$8;->val$graphPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/prime31/FacebookPlugin$8;->val$parameters:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/prime31/FacebookPlugin$8;->val$httpMethod:Ljava/lang/String;

    .line 499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/prime31/FacebookPlugin$8;)Lcom/prime31/FacebookPlugin;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/prime31/FacebookPlugin$8;->this$0:Lcom/prime31/FacebookPlugin;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 503
    new-instance v0, Lcom/facebook/Request;

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/prime31/FacebookPlugin$8;->val$graphPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/prime31/FacebookPlugin$8;->val$parameters:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/prime31/FacebookPlugin$8;->val$httpMethod:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/HttpMethod;->valueOf(Ljava/lang/String;)Lcom/facebook/HttpMethod;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/Request;-><init>(Lcom/facebook/Session;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/HttpMethod;)V

    .line 504
    .local v0, "req":Lcom/facebook/Request;
    new-instance v1, Lcom/prime31/FacebookPlugin$8$1;

    invoke-direct {v1, p0}, Lcom/prime31/FacebookPlugin$8$1;-><init>(Lcom/prime31/FacebookPlugin$8;)V

    invoke-virtual {v0, v1}, Lcom/facebook/Request;->setCallback(Lcom/facebook/Request$Callback;)V

    .line 515
    invoke-virtual {v0}, Lcom/facebook/Request;->executeAsync()Lcom/facebook/RequestAsyncTask;

    .line 516
    return-void
.end method
