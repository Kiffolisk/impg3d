.class Lcom/prime31/FacebookPlugin$9;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin;->call(Lcom/facebook/Session;Lcom/facebook/SessionState;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/FacebookPlugin;

.field private final synthetic val$exception:Ljava/lang/Exception;

.field private final synthetic val$session:Lcom/facebook/Session;

.field private final synthetic val$state:Lcom/facebook/SessionState;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin;Lcom/facebook/SessionState;Ljava/lang/Exception;Lcom/facebook/Session;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$9;->this$0:Lcom/prime31/FacebookPlugin;

    iput-object p2, p0, Lcom/prime31/FacebookPlugin$9;->val$state:Lcom/facebook/SessionState;

    iput-object p3, p0, Lcom/prime31/FacebookPlugin$9;->val$exception:Ljava/lang/Exception;

    iput-object p4, p0, Lcom/prime31/FacebookPlugin$9;->val$session:Lcom/facebook/Session;

    .line 603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 608
    const-string v1, "Prime31"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FacebookPlugin: openActiveSession.call with state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/prime31/FacebookPlugin$9;->val$state:Lcom/facebook/SessionState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/prime31/FacebookPlugin$9;->val$exception:Ljava/lang/Exception;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->val$session:Lcom/facebook/Session;

    invoke-virtual {v1}, Lcom/facebook/Session;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 612
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->val$state:Lcom/facebook/SessionState;

    sget-object v2, Lcom/facebook/SessionState;->OPENED_TOKEN_UPDATED:Lcom/facebook/SessionState;

    if-ne v1, v2, :cond_1

    .line 613
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->this$0:Lcom/prime31/FacebookPlugin;

    const-string v2, "reauthorizationSucceeded"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_0
    :goto_0
    return-void

    .line 614
    :cond_1
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->val$exception:Ljava/lang/Exception;

    if-eqz v1, :cond_2

    .line 615
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->this$0:Lcom/prime31/FacebookPlugin;

    const-string v2, "reauthorizationFailed"

    iget-object v3, p0, Lcom/prime31/FacebookPlugin$9;->val$exception:Ljava/lang/Exception;

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 617
    :cond_2
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->this$0:Lcom/prime31/FacebookPlugin;

    const-string v2, "sessionOpened"

    iget-object v3, p0, Lcom/prime31/FacebookPlugin$9;->val$session:Lcom/facebook/Session;

    invoke-virtual {v3}, Lcom/facebook/Session;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 619
    :cond_3
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->val$state:Lcom/facebook/SessionState;

    sget-object v2, Lcom/facebook/SessionState;->CLOSED_LOGIN_FAILED:Lcom/facebook/SessionState;

    if-ne v1, v2, :cond_0

    .line 621
    const-string v0, "Unknown error"

    .line 622
    .local v0, "error":Ljava/lang/String;
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->val$exception:Ljava/lang/Exception;

    if-eqz v1, :cond_4

    .line 623
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->val$exception:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 624
    :cond_4
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$9;->this$0:Lcom/prime31/FacebookPlugin;

    const-string v2, "loginFailed"

    invoke-virtual {v1, v2, v0}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const-string v1, "Prime31"

    const-string v2, "force closing session to workaround login issue"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/facebook/Session;->setActiveSession(Lcom/facebook/Session;)V

    goto :goto_0
.end method
