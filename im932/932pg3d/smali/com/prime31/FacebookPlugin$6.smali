.class Lcom/prime31/FacebookPlugin$6;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin;->showFacebookShareDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/FacebookPlugin;

.field private final synthetic val$jsonString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$6;->this$0:Lcom/prime31/FacebookPlugin;

    iput-object p2, p0, Lcom/prime31/FacebookPlugin$6;->val$jsonString:Ljava/lang/String;

    .line 381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 385
    const/4 v9, 0x0

    .line 386
    .local v9, "link":Ljava/lang/String;
    const/4 v1, 0x0

    .line 387
    .local v1, "caption":Ljava/lang/String;
    const/4 v10, 0x0

    .line 388
    .local v10, "name":Ljava/lang/String;
    const/4 v2, 0x0

    .line 389
    .local v2, "description":Ljava/lang/String;
    const/4 v11, 0x0

    .line 390
    .local v11, "picture":Ljava/lang/String;
    const/4 v5, 0x0

    .line 395
    .local v5, "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    iget-object v12, p0, Lcom/prime31/FacebookPlugin$6;->val$jsonString:Ljava/lang/String;

    invoke-direct {v8, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 396
    .local v8, "jsonObject":Lorg/json/JSONObject;
    const-string v12, "link"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 397
    const-string v12, "link"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 399
    :cond_0
    const-string v12, "caption"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 400
    const-string v12, "caption"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 402
    :cond_1
    const-string v12, "name"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 403
    const-string v12, "name"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 405
    :cond_2
    const-string v12, "description"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 406
    const-string v12, "description"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 408
    :cond_3
    const-string v12, "picture"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 409
    const-string v12, "picture"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 411
    :cond_4
    const-string v12, "friends"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 413
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    .end local v5    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v6, "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_1
    const-string v12, "friends"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 415
    .local v0, "arr":Lorg/json/JSONArray;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v12

    if-lt v7, v12, :cond_c

    move-object v5, v6

    .line 425
    .end local v0    # "arr":Lorg/json/JSONArray;
    .end local v6    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "i":I
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    :goto_1
    new-instance v3, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-direct {v3, v12}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;-><init>(Landroid/app/Activity;)V

    .line 426
    .local v3, "dialog":Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;
    if-eqz v9, :cond_6

    .line 427
    invoke-virtual {v3, v9}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->setLink(Ljava/lang/String;)Lcom/facebook/widget/FacebookDialog$ShareDialogBuilderBase;

    .line 429
    :cond_6
    if-eqz v1, :cond_7

    .line 430
    invoke-virtual {v3, v1}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->setCaption(Ljava/lang/String;)Lcom/facebook/widget/FacebookDialog$ShareDialogBuilderBase;

    .line 432
    :cond_7
    if-eqz v10, :cond_8

    .line 433
    invoke-virtual {v3, v10}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->setName(Ljava/lang/String;)Lcom/facebook/widget/FacebookDialog$ShareDialogBuilderBase;

    .line 435
    :cond_8
    if-eqz v2, :cond_9

    .line 436
    invoke-virtual {v3, v2}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->setDescription(Ljava/lang/String;)Lcom/facebook/widget/FacebookDialog$ShareDialogBuilderBase;

    .line 438
    :cond_9
    if-eqz v11, :cond_a

    .line 439
    invoke-virtual {v3, v11}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->setPicture(Ljava/lang/String;)Lcom/facebook/widget/FacebookDialog$ShareDialogBuilderBase;

    .line 441
    :cond_a
    if-eqz v5, :cond_b

    .line 442
    invoke-virtual {v3, v5}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->setFriends(Ljava/util/List;)Lcom/facebook/widget/FacebookDialog$ShareDialogBuilderBase;

    .line 444
    :cond_b
    invoke-virtual {v3}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->canPresent()Z

    move-result v12

    if-eqz v12, :cond_d

    .line 446
    invoke-static {}, Lcom/prime31/FacebookPlugin;->access$1()Lcom/facebook/UiLifecycleHelper;

    move-result-object v12

    invoke-virtual {v3}, Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;->build()Lcom/facebook/widget/FacebookDialog;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/widget/FacebookDialog;->present()Lcom/facebook/widget/FacebookDialog$PendingCall;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/facebook/UiLifecycleHelper;->trackPendingDialogCall(Lcom/facebook/widget/FacebookDialog$PendingCall;)V

    .line 453
    :goto_2
    return-void

    .line 416
    .end local v3    # "dialog":Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;
    .end local v5    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v0    # "arr":Lorg/json/JSONArray;
    .restart local v6    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v7    # "i":I
    .restart local v8    # "jsonObject":Lorg/json/JSONObject;
    :cond_c
    :try_start_2
    invoke-virtual {v0, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 415
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 420
    .end local v0    # "arr":Lorg/json/JSONArray;
    .end local v6    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "i":I
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v4

    .line 422
    .local v4, "e":Ljava/lang/Exception;
    :goto_3
    const-string v12, "Prime31"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "error parsing out dialog parameters: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 450
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "dialog":Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;
    :cond_d
    const-string v12, "Prime31"

    const-string v13, "Cannot present native share dialog. Falling back to standard dialog"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    iget-object v12, p0, Lcom/prime31/FacebookPlugin$6;->this$0:Lcom/prime31/FacebookPlugin;

    const-string v13, "stream.publish"

    iget-object v14, p0, Lcom/prime31/FacebookPlugin$6;->val$jsonString:Ljava/lang/String;

    invoke-virtual {v12, v13, v14}, Lcom/prime31/FacebookPlugin;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 420
    .end local v3    # "dialog":Lcom/facebook/widget/FacebookDialog$ShareDialogBuilder;
    .end local v5    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v8    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v4

    move-object v5, v6

    .end local v6    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v5    # "friendsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_3
.end method
