.class public Lcom/prime31/TwitterSession;
.super Ljava/lang/Object;
.source "TwitterSession.java"


# static fields
.field private static final APP_CALLBACK_URL:Ljava/lang/String; = "app_callback_url_prefix"

.field private static final APP_CONSUMER_KEY:Ljava/lang/String; = "app_consumer_key"

.field private static final APP_CONSUMER_SECRET:Ljava/lang/String; = "app_consumer_secret"

.field private static final SHARED:Ljava/lang/String; = "Twitter_Preferences"

.field private static final TWEET_AUTH_KEY:Ljava/lang/String; = "auth_key"

.field private static final TWEET_AUTH_SECRET_KEY:Ljava/lang/String; = "auth_secret_key"

.field private static final TWEET_USER_NAME:Ljava/lang/String; = "user_name"


# instance fields
.field private editor:Landroid/content/SharedPreferences$Editor;

.field private sharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "Twitter_Preferences"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    .line 29
    iget-object v0, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    .line 30
    return-void
.end method


# virtual methods
.method public getAccessToken()Lorg/scribe/model/Token;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 89
    iget-object v3, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    const-string v4, "auth_key"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "token":Ljava/lang/String;
    iget-object v3, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    const-string v4, "auth_secret_key"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "tokenSecret":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 93
    new-instance v2, Lorg/scribe/model/Token;

    invoke-direct {v2, v0, v1}, Lorg/scribe/model/Token;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_0
    return-object v2
.end method

.method public getCallbackUrlPrefix()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    const-string v1, "app_callback_url_prefix"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConsumerKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    const-string v1, "app_consumer_key"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConsumerSecret()Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    const-string v1, "app_consumer_secret"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/prime31/TwitterSession;->sharedPref:Landroid/content/SharedPreferences;

    const-string v1, "user_name"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public resetAccessToken()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "auth_key"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 56
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "auth_secret_key"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 57
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "user_name"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 59
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 60
    return-void
.end method

.method public storeAccessToken(Lorg/scribe/model/Token;Ljava/lang/String;)V
    .locals 3
    .param p1, "accessToken"    # Lorg/scribe/model/Token;
    .param p2, "username"    # Ljava/lang/String;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "auth_key"

    invoke-virtual {p1}, Lorg/scribe/model/Token;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 36
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "auth_secret_key"

    invoke-virtual {p1}, Lorg/scribe/model/Token;->getSecret()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 37
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "user_name"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 39
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 40
    return-void
.end method

.method public storeAppDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "consumerKey"    # Ljava/lang/String;
    .param p2, "consumerSecret"    # Ljava/lang/String;
    .param p3, "callbackUrlScheme"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "app_consumer_key"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 46
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "app_consumer_secret"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 47
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "app_callback_url_prefix"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 49
    iget-object v0, p0, Lcom/prime31/TwitterSession;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 50
    return-void
.end method
