.class public Lcom/prime31/TwitterDialog;
.super Landroid/app/Dialog;
.source "TwitterDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/prime31/TwitterDialog$DialogListener;,
        Lcom/prime31/TwitterDialog$TwitterWebViewClient;
    }
.end annotation


# static fields
.field static final FILL:Landroid/widget/FrameLayout$LayoutParams;

.field static final MARGIN:I = 0x4

.field static final PADDING:I = 0x2

.field private static final TAG:Ljava/lang/String; = "Prime31-TD"


# instance fields
.field private _content:Landroid/widget/FrameLayout;

.field private _listener:Lcom/prime31/TwitterDialog$DialogListener;

.field private _spinner:Landroid/app/ProgressDialog;

.field private _title:Landroid/widget/TextView;

.field private _url:Ljava/lang/String;

.field private _webView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 27
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/prime31/TwitterDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/prime31/TwitterDialog$DialogListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/prime31/TwitterDialog$DialogListener;

    .prologue
    .line 54
    const v0, 0x1030007

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 56
    iput-object p2, p0, Lcom/prime31/TwitterDialog;->_url:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/prime31/TwitterDialog;->_listener:Lcom/prime31/TwitterDialog$DialogListener;

    .line 58
    return-void
.end method

.method static synthetic access$0(Lcom/prime31/TwitterDialog;)Lcom/prime31/TwitterDialog$DialogListener;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_listener:Lcom/prime31/TwitterDialog$DialogListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/prime31/TwitterDialog;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_spinner:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2(Lcom/prime31/TwitterDialog;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_content:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/prime31/TwitterDialog;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/prime31/TwitterDialog;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    return-object v0
.end method

.method private clearAndDisableCookies()V
    .locals 3

    .prologue
    .line 128
    const-string v1, "Prime31-TD"

    const-string v2, "clearing all cookies"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {p0}, Lcom/prime31/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 131
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 132
    .local v0, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 133
    return-void
.end method

.method private setUpTitle()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x4

    .line 85
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/prime31/TwitterDialog;->requestWindowFeature(I)Z

    .line 89
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/prime31/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    const-string v1, "Twitter"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 93
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 94
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    const v1, -0x442817

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 95
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 96
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 99
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_content:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/prime31/TwitterDialog;->_title:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 100
    return-void
.end method

.method private setUpWebView()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 107
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/prime31/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    .line 110
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 111
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 114
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 115
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 116
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/prime31/TwitterDialog$TwitterWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/prime31/TwitterDialog$TwitterWebViewClient;-><init>(Lcom/prime31/TwitterDialog;Lcom/prime31/TwitterDialog$TwitterWebViewClient;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 117
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/prime31/TwitterDialog;->_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    sget-object v1, Lcom/prime31/TwitterDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 119
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_content:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/prime31/TwitterDialog;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 122
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, -0x1

    .line 64
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 66
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/prime31/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/prime31/TwitterDialog;->_spinner:Landroid/app/ProgressDialog;

    .line 68
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_spinner:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 69
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_spinner:Landroid/app/ProgressDialog;

    const-string v1, "Loading..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 71
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/prime31/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/prime31/TwitterDialog;->_content:Landroid/widget/FrameLayout;

    .line 74
    invoke-direct {p0}, Lcom/prime31/TwitterDialog;->setUpTitle()V

    .line 75
    invoke-direct {p0}, Lcom/prime31/TwitterDialog;->setUpWebView()V

    .line 77
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_content:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/prime31/TwitterDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v0, p0, Lcom/prime31/TwitterDialog;->_content:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 79
    const-string v0, "Prime31-TD"

    const-string v1, "showing Twitter dialog filling parents horizontally and vertically"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    return-void
.end method
