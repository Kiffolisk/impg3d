.class Lcom/prime31/FacebookPlugin$7;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin;->showDialog(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/FacebookPlugin;

.field private final synthetic val$dialogType:Ljava/lang/String;

.field private final synthetic val$json:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$7;->this$0:Lcom/prime31/FacebookPlugin;

    iput-object p2, p0, Lcom/prime31/FacebookPlugin$7;->val$json:Ljava/lang/String;

    iput-object p3, p0, Lcom/prime31/FacebookPlugin$7;->val$dialogType:Ljava/lang/String;

    .line 460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 464
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$7;->this$0:Lcom/prime31/FacebookPlugin;

    iget-object v2, p0, Lcom/prime31/FacebookPlugin$7;->val$json:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/prime31/FacebookPlugin;->bundleFromJSON(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 465
    .local v3, "parameters":Landroid/os/Bundle;
    if-nez v3, :cond_0

    .line 466
    new-instance v3, Landroid/os/Bundle;

    .end local v3    # "parameters":Landroid/os/Bundle;
    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 468
    .restart local v3    # "parameters":Landroid/os/Bundle;
    :cond_0
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$7;->val$dialogType:Ljava/lang/String;

    const-string v2, "feeds"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 470
    new-instance v1, Lcom/facebook/widget/WebDialog$FeedDialogBuilder;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    invoke-direct {v1, v2, v4, v3}, Lcom/facebook/widget/WebDialog$FeedDialogBuilder;-><init>(Landroid/content/Context;Lcom/facebook/Session;Landroid/os/Bundle;)V

    .line 471
    iget-object v2, p0, Lcom/prime31/FacebookPlugin$7;->this$0:Lcom/prime31/FacebookPlugin;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/WebDialog$FeedDialogBuilder;->setOnCompleteListener(Lcom/facebook/widget/WebDialog$OnCompleteListener;)Lcom/facebook/widget/WebDialog$BuilderBase;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/WebDialog$FeedDialogBuilder;

    invoke-virtual {v1}, Lcom/facebook/widget/WebDialog$FeedDialogBuilder;->build()Lcom/facebook/widget/WebDialog;

    move-result-object v6

    .line 472
    .local v6, "apprequestsDialog":Lcom/facebook/widget/WebDialog;
    invoke-virtual {v6}, Lcom/facebook/widget/WebDialog;->show()V

    .line 490
    .end local v6    # "apprequestsDialog":Lcom/facebook/widget/WebDialog;
    :goto_0
    return-void

    .line 474
    :cond_1
    iget-object v1, p0, Lcom/prime31/FacebookPlugin$7;->val$dialogType:Ljava/lang/String;

    const-string v2, "apprequests"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 476
    new-instance v1, Lcom/facebook/widget/WebDialog$RequestsDialogBuilder;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v4

    invoke-direct {v1, v2, v4, v3}, Lcom/facebook/widget/WebDialog$RequestsDialogBuilder;-><init>(Landroid/content/Context;Lcom/facebook/Session;Landroid/os/Bundle;)V

    .line 477
    iget-object v2, p0, Lcom/prime31/FacebookPlugin$7;->this$0:Lcom/prime31/FacebookPlugin;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/WebDialog$RequestsDialogBuilder;->setOnCompleteListener(Lcom/facebook/widget/WebDialog$OnCompleteListener;)Lcom/facebook/widget/WebDialog$BuilderBase;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/WebDialog$RequestsDialogBuilder;

    invoke-virtual {v1}, Lcom/facebook/widget/WebDialog$RequestsDialogBuilder;->build()Lcom/facebook/widget/WebDialog;

    move-result-object v7

    .line 478
    .local v7, "feedDialog":Lcom/facebook/widget/WebDialog;
    invoke-virtual {v7}, Lcom/facebook/widget/WebDialog;->show()V

    goto :goto_0

    .line 483
    .end local v7    # "feedDialog":Lcom/facebook/widget/WebDialog;
    :cond_2
    const-string v1, "app_id"

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/Session;->getApplicationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const-string v1, "access_token"

    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/Session;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const-string v1, "redirect_uri"

    const-string v2, "fbconnect://success"

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    new-instance v0, Lcom/facebook/widget/WebDialog;

    invoke-static {}, Lcom/prime31/FacebookPlugin;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/prime31/FacebookPlugin$7;->val$dialogType:Ljava/lang/String;

    const v4, 0x1030010

    iget-object v5, p0, Lcom/prime31/FacebookPlugin$7;->this$0:Lcom/prime31/FacebookPlugin;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/WebDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILcom/facebook/widget/WebDialog$OnCompleteListener;)V

    .line 488
    .local v0, "dialog":Lcom/facebook/widget/WebDialog;
    invoke-virtual {v0}, Lcom/facebook/widget/WebDialog;->show()V

    goto :goto_0
.end method
