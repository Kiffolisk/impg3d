.class Lcom/prime31/FacebookPlugin$8$1;
.super Ljava/lang/Object;
.source "FacebookPlugin.java"

# interfaces
.implements Lcom/facebook/Request$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/prime31/FacebookPlugin$8;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/prime31/FacebookPlugin$8;


# direct methods
.method constructor <init>(Lcom/prime31/FacebookPlugin$8;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/prime31/FacebookPlugin$8$1;->this$1:Lcom/prime31/FacebookPlugin$8;

    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/facebook/Response;)V
    .locals 3
    .param p1, "response"    # Lcom/facebook/Response;

    .prologue
    .line 509
    invoke-virtual {p1}, Lcom/facebook/Response;->getError()Lcom/facebook/FacebookRequestError;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/prime31/FacebookPlugin$8$1;->this$1:Lcom/prime31/FacebookPlugin$8;

    invoke-static {v0}, Lcom/prime31/FacebookPlugin$8;->access$0(Lcom/prime31/FacebookPlugin$8;)Lcom/prime31/FacebookPlugin;

    move-result-object v0

    const-string v1, "graphRequestFailed"

    invoke-virtual {p1}, Lcom/facebook/Response;->getError()Lcom/facebook/FacebookRequestError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/FacebookRequestError;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :goto_0
    return-void

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/prime31/FacebookPlugin$8$1;->this$1:Lcom/prime31/FacebookPlugin$8;

    invoke-static {v0}, Lcom/prime31/FacebookPlugin$8;->access$0(Lcom/prime31/FacebookPlugin$8;)Lcom/prime31/FacebookPlugin;

    move-result-object v0

    const-string v1, "graphRequestCompleted"

    invoke-virtual {p1}, Lcom/facebook/Response;->getGraphObject()Lcom/facebook/model/GraphObject;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/model/GraphObject;->getInnerJSONObject()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/prime31/FacebookPlugin;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
