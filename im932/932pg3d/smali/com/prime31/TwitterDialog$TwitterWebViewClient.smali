.class Lcom/prime31/TwitterDialog$TwitterWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "TwitterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/prime31/TwitterDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TwitterWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/prime31/TwitterDialog;


# direct methods
.method private constructor <init>(Lcom/prime31/TwitterDialog;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/prime31/TwitterDialog;Lcom/prime31/TwitterDialog$TwitterWebViewClient;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/prime31/TwitterDialog$TwitterWebViewClient;-><init>(Lcom/prime31/TwitterDialog;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 205
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 206
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$3(Lcom/prime31/TwitterDialog;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "title":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$4(Lcom/prime31/TwitterDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$1(Lcom/prime31/TwitterDialog;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 212
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$2(Lcom/prime31/TwitterDialog;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 213
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$2(Lcom/prime31/TwitterDialog;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 214
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$3(Lcom/prime31/TwitterDialog;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 215
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 181
    const-string v0, "Prime31-TD"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Page started loading URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 197
    iget-object v0, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v0}, Lcom/prime31/TwitterDialog;->access$1(Lcom/prime31/TwitterDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 198
    iget-object v0, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v0}, Lcom/prime31/TwitterDialog;->access$2(Lcom/prime31/TwitterDialog;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 199
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 170
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v0, "Prime31-TD"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Page error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v0}, Lcom/prime31/TwitterDialog;->access$0(Lcom/prime31/TwitterDialog;)Lcom/prime31/TwitterDialog$DialogListener;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/prime31/TwitterDialog$DialogListener;->onError(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-virtual {v0}, Lcom/prime31/TwitterDialog;->dismiss()V

    .line 175
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 143
    const-string v1, "Prime31-TD"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Redirect URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    sget-object v1, Lcom/prime31/TwitterPlugin;->callbackUrlPrefix:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 147
    const-string v1, "Prime31-TD"

    const-string v2, "found our redirect url. getting out of here and dismissing web view"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const-string v1, "denied"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$0(Lcom/prime31/TwitterDialog;)Lcom/prime31/TwitterDialog$DialogListener;

    move-result-object v1

    const-string v2, "canceled"

    invoke-interface {v1, v2}, Lcom/prime31/TwitterDialog$DialogListener;->onError(Ljava/lang/String;)V

    .line 154
    :goto_0
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-virtual {v1}, Lcom/prime31/TwitterDialog;->dismiss()V

    .line 163
    :cond_0
    :goto_1
    return v0

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/prime31/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/prime31/TwitterDialog;

    invoke-static {v1}, Lcom/prime31/TwitterDialog;->access$0(Lcom/prime31/TwitterDialog;)Lcom/prime31/TwitterDialog$DialogListener;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/prime31/TwitterDialog$DialogListener;->onComplete(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_2
    const-string v1, "authorize"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    const/4 v0, 0x0

    goto :goto_1
.end method
